﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonUtility
{
    public class TypeCaster
    {
        public static long TryConvertToLong(string value)
        {
            try
            {
                return long.Parse(value);
            }
            catch
            {
                return 0;
            }
        }
        public static float TryConvertToFloat(string value)
        {
            try
            {
                return float.Parse(value);
            }
            catch
            {
                return 0;
            }
        }
    }
}
