using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class ShiftAllocationDetailsRepository : IShiftAllocationDetailsRepository
    {
        GSDBSExtended entity;
        public ShiftAllocationDetailsRepository()
        {
            entity = new GSDBSExtended();
        }

        //public bool CreateShiftAllocationDetails(Models.ShiftAllocation shiftAllocation)
        //{
        //    tbl_ShiftAllocation tblShiftAllocation = new tbl_ShiftAllocation(); //entity.tbl_ShiftAllocation.FirstOrDefault(us => us. == id);
        //    tblShiftAllocation.Date = shiftAllocation.Date;
        //    tblShiftAllocation.EmployeeName = shiftAllocation.EmployeeName;
        //    tblShiftAllocation.Note = shiftAllocation.Note;
        //    tblShiftAllocation.Shift = shiftAllocation.Shift;
        //    foreach (var item in shiftAllocation.lstAllotcation)
        //    {
        //        tbl_ShiftAllocationDetails details = new tbl_ShiftAllocationDetails();
        //        details.Nozzel = item.Nozzel;
        //        details.Opening = item.Opening;
        //        details.Rate = item.Rate;
        //        tblShiftAllocation.tbl_ShiftAllocationDetails.Add(details);
        //    }

        //    entity.tbl_ShiftAllocation.Add(tblShiftAllocation);
        //    int result = entity.SaveChanges();
        //    //return tblShiftAllocation.Id; if want to return ID
        //    return result > 0;
        //}
        //public Models.ShiftAllocationDetails Get(long ID)
        //{
        //    return (from us in entity.tbl_ShiftAllocationDetails
        //            join shiftDetails in entity.tbl_ShiftAllocation
        //            on us.ShiftAllocationMasterID equals shiftDetails.Id
        //            where us.ShiftAllocationMasterID == ID
        //            select new ShiftAllocationDetails()
        //            //return entity.tbl_ShiftAllocationDetails.Select(us => new ShiftAllocationDetails()
        //            {
        //                ID = us.ID,
        //                Shift = shiftDetails.ShiftType.ToString(),
        //                ShiftAllocationMasterID = us.ShiftAllocationMasterID,
        //                Employee_ID = us.Employee_ID,
        //                EmployeeName = shiftDetails.EmployeeName.ToString(),

        //                Note = us.Note,
        //                Tank = us.Tank,
        //                Nozzel = us.Nozzel,
        //                Opening = us.Opening,
        //                Closing = us.Closing,
        //                TotalSale = us.Closing,
        //                Rate = us.Rate,
        //                TotalAmt = us.TotalAmt,
        //                ByCash = us.ByCash,
        //                CreditCardCash = us.CreditCardCash,
        //                DebitCardCash = us.DebitCardCash,
        //                GrandTotalSale = us.GrandTotalSale,
        //                GrandTotalAmount = us.GrandTotalAmount,
        //                PumpTesting = us.PumpTesting,
        //                //ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
        //                //ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,

        //            }).FirstOrDefault();


        //    //var q = (from a in entity.tbl_ShiftAllocation
        //    //         join shiftDetails in entity.tbl_ShiftAllocationDetails
        //    //         on a.Id equals shiftDetails.ShiftAllocationMasterID
        //    //         where a.Id == ID
        //    //         select new
        //    //         {
        //    //             a.Employee_ID,
        //    //             a.Id,
        //    //             a.Note,
        //    //             a.EmployeeName,
        //    //             a.Date,
        //    //             a.ShiftType,
        //    //             a.tbl_ShiftAllocationDetails
        //    //         }
        //    //     ).FirstOrDefault();
        //    //ShiftAllocation allocation = new ShiftAllocation();

        //    //    allocation.ID = q.Id;

        //    ////  allocation.Employee_ID = q.Employee_ID;
        //    ////  allocation.EmployeeName = q.EmployeeName;
        //    //allocation.Date = q.Date;
        //    //allocation.Note = q.Note;
        //    //allocation.ShiftType = q.ShiftType;
        //    //allocation.lstAllotcation = new List<ShiftAllocationDetails>();
        //    //foreach (var us in q.tbl_ShiftAllocationDetails)
        //    //{
        //    //    ShiftAllocationDetails shift = new ShiftAllocationDetails();
        //    //    shift.ID = us.ID;
        //    //    shift.Note = us.Note;
        //    //    shift.Nozzel = us.Nozzel;
        //    //    shift.Opening = us.Opening;
        //    //    shift.Closing = us.Closing;
        //    //    shift.TotalSale = us.TotalSale;
        //    //    shift.Tank = us.Tank;
        //    //    shift.Rate = us.Rate;
        //    //    shift.TotalAmt = us.TotalAmt;
        //    //    shift.ByCash = us.ByCash;
        //    //    shift.CreditCardCash = us.CreditCardCash;
        //    //    shift.DebitCardCash = us.DebitCardCash;
        //    //    shift.GrandTotalSale = us.GrandTotalSale;
        //    //    shift.GrandTotalAmount = us.GrandTotalAmount;
        //    //    shift.PumpTesting = us.PumpTesting;
        //    //    allocation.lstAllotcation.Add(shift);
        //    //}
        //    ////   allocation.Add(shift);
        //    //return allocation;

        //    //DataTable dt = new DataTable();
        //    //SqlDataAdapter da = new SqlDataAdapter();
        //    //string sqlconn = ConfigurationManager.ConnectionStrings["Data Source = PRATIKPC\\SQLEXPRESS; Initial Catalog = GSDBS; Integrated Security = True; MultipleActiveResultSets = True; Application Name = EntityFramework"].ConnectionString;
        //    //SqlConnection con = new SqlConnection(sqlconn);
        //    //con.Open();

        //    //SqlCommand cmd = new SqlCommand("[dbo].[TBL_SP_GSDBS]", con);
        //    //cmd.CommandType = CommandType.StoredProcedure;

        //    //dt = new DataTable("EMP");
        //    //da = new SqlDataAdapter(cmd);
        //    //da.Fill(dt);
        //    //return dt;







        //}


        //List<ShiftAllocationDetails> IShiftAllocationDetailsRepository.Get()
        //  {
        //      throw new NotImplementedException();
        //  }

        public Models.ShiftAllocation Get(long ID)
        {
            var q = (from a in entity.tbl_ShiftAllocation
                     join shiftDetails in entity.tbl_ShiftAllocationDetails
                     on a.Id equals shiftDetails.ShiftAllocationMasterID
                     where a.Id == ID
                     select new
                     {
                         a.Employee_ID,
                         a.Id,
                         a.Note,
                         a.EmployeeName,
                         a.Date,
                         a.ShiftType,
                         a.tbl_ShiftAllocationDetails
                     }
                 ).FirstOrDefault();
            ShiftAllocation allocation = new ShiftAllocation();

            allocation.ID = q.Id;

            //  allocation.Employee_ID = q.Employee_ID;
            //  allocation.EmployeeName = q.EmployeeName;
            allocation.Date = q.Date;
            allocation.Note = q.Note;
            allocation.ShiftType = q.ShiftType;
            allocation.lstAllotcation = new List<ShiftAllocationDetails>();
            foreach (var us in q.tbl_ShiftAllocationDetails)
            {
                ShiftAllocationDetails shift = new ShiftAllocationDetails();
                shift.ID = us.ID;
                shift.Note = us.Note;
                shift.Nozzel = us.Nozzel;
                shift.Opening = us.Opening;
                shift.Closing = us.Closing;
                shift.TotalSale = us.TotalSale;
                shift.Tank = us.Tank;
                shift.Rate = us.Rate;
                shift.TotalAmt = us.TotalAmt;
                shift.ByCash = us.ByCash;
                shift.CreditCardCash = us.CreditCardCash;
                shift.DebitCardCash = us.DebitCardCash;
                shift.GrandTotalSale = us.GrandTotalSale;
                shift.GrandTotalAmount = us.GrandTotalAmount;
                shift.PumpTesting = us.PumpTesting;
                allocation.lstAllotcation.Add(shift);
            }
            //   allocation.Add(shift);
            return allocation;
        }
        public List<Models.ShiftAllocationDetails> Get()
        {
            return (from us in entity.tbl_ShiftAllocationDetails
                    join shiftDetails in entity.tbl_ShiftAllocation
                    on us.ShiftAllocationMasterID equals shiftDetails.Id
                    join noz in entity.tbl_NozzleMaster
                    on us.Nozzel equals noz.Id
                    join tak in entity.tbl_TankMaster
                    on us.Tank equals tak.Id
                    
                   
                    
                    select new ShiftAllocationDetails()
                    //return entity.tbl_ShiftAllocationDetails.Select(us => new ShiftAllocationDetails()
                    {
                ID = us.ID,
                Shift = shiftDetails.ShiftType.ToString(),
                ShiftAllocationMasterID = us.ShiftAllocationMasterID,
                Employee_ID = us.Employee_ID,
                EmployeeName = shiftDetails.EmployeeName.ToString(),
               
               
                Note = us.Note,
                NozzelDes=noz.Type,
                Tank = us.Tank,
                TankDes= tak.Type,
                Nozzel = us.Nozzel,
                Opening = us.Opening,
                Closing = us.Closing,
                TotalSale = us.Closing,
                Rate = us.Rate,
                TotalAmt = us.TotalAmt,
                ByCash = us.ByCash,
                CreditCardCash = us.CreditCardCash,
                DebitCardCash = us.DebitCardCash,
                GrandTotalSale = us.GrandTotalSale,
                GrandTotalAmount = us.GrandTotalAmount,
                PumpTesting = us.PumpTesting,
                //ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                //ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,

            }).ToList();


            //return entity.tbl_ShiftAllocationDetails.Select(us => new ShiftAllocationDetails()
            //{

            //var q = (from a in entity.tbl_ShiftAllocation
            //         join shiftDetails in entity.tbl_ShiftAllocationDetails
            //         on a.Id equals shiftDetails.ShiftAllocationMasterID
            //         where a.Id == shiftDetails.ID
            //         select new
            //         {
            //             a.Employee_ID,
            //             a.Id,
            //             a.Note,
            //             a.EmployeeName,
            //             a.Date,
            //             a.ShiftType,
            //             a.tbl_ShiftAllocationDetails
            //         });

            //List<ShiftAllocationDetails> ls = new List<ShiftAllocationDetails>(q.ToList());
            //return ls;

            //});

            //    ID = us.ID,
            //    Shift = us.Shift,
            //    ShiftAllocationMasterID = us.ShiftAllocationMasterID,
            //    Employee_ID = us.Employee_ID,
            //    //EmployeeNameDes=us.EmployeeName,
            //    Note = us.Note,
            //    Tank = us.Tank,
            //    Nozzel = us.Nozzel,
            //    Opening = us.Opening,
            //    Closing = us.Closing,
            //    TotalSale = us.Closing,
            //    Rate = us.Rate,
            //    TotalAmt = us.TotalAmt,
            //    ByCash = us.ByCash,
            //    CreditCardCash = us.CreditCardCash,
            //    DebitCardCash = us.DebitCardCash,
            //    GrandTotalSale = us.GrandTotalSale,
            //    GrandTotalAmount = us.GrandTotalAmount,
            //    PumpTesting = us.PumpTesting,
            //    //ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
            //    //ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,

            //}).ToList();


            //List<ShiftAllocation> custs = (from c in entity.tbl_ShiftAllocationDetails
            //                               select new ShiftAllocation()
            //                               {
            //                                   ID = c.ID,
            //                                  EmployeeNameDes = c.EmployeeName,
            //                                  ShiftDes =c.Shift,

            //                                   lstAllotcation = (from p in entity.tbl_ShiftAllocationDetails where p.ID == c.ShiftAllocationMasterID select p.Rate).ToList<tbl_ShiftAllocationDetails>()
            //                               }).ToList<ShiftAllocation>();

            //     List<ShiftAllocation> custs = new List<ShiftAllocation>();
            //     var q = (from a in entity.tbl_ShiftAllocation
            //              join shiftDetails in entity.tbl_ShiftAllocationDetails
            //              on a.Id equals shiftDetails.ShiftAllocationMasterID
            //              where a.Id == shiftDetails.ID
            //              select new
            //              {
            //                  a.Employee_ID,
            //                  a.Id,
            //                  a.Note,
            //                  a.EmployeeName,
            //                  a.Date,
            //                  a.ShiftType,
            //                  a.tbl_ShiftAllocationDetails
            //              }
            //).ToList();
            //     return q;

            //   ShiftAllocation allocation = new ShiftAllocation();

            ////   allocation.Employee_ID = q.Employee_ID;
            //   allocation.EmployeeName = q.EmployeeName;
            //   allocation.Date = q.Date;
            //   allocation.Note = q.Note;
            //   allocation.ShiftType = q.ShiftType;
            //   allocation.lstAllotcation = new List<ShiftAllocationDetails>();
            //   foreach (var us in q.tbl_ShiftAllocationDetails)
            //   {
            //       ShiftAllocationDetails shift = new ShiftAllocationDetails();
            //       shift.ID = us.ID;
            //       shift.Note = us.Note;
            //       shift.Nozzel = us.Nozzel;
            //       shift.Opening = us.Opening;
            //       shift.Closing = us.Closing;
            //       shift.TotalSale = us.TotalSale;
            //       shift.Tank = us.Tank;
            //       shift.Rate = us.Rate;
            //       shift.TotalAmt = us.TotalAmt;
            //       shift.ByCash = us.ByCash;
            //       shift.CreditCardCash = us.CreditCardCash;
            //       shift.DebitCardCash = us.DebitCardCash;
            //       shift.GrandTotalSale = us.GrandTotalSale;
            //       shift.GrandTotalAmount = us.GrandTotalAmount;
            //       shift.PumpTesting = us.PumpTesting;
            //       allocation.lstAllotcation.Add(shift);
            //   }
            //   //   allocation.Add(shift);
            //  return allocation;



        }



        public bool CreateShiftAllocationDetails(ShiftAllocation Shiftallocation)
        {
            tbl_ShiftAllocation tblShiftAllocation = new tbl_ShiftAllocation(); //entity.tbl_ShiftAllocation.FirstOrDefault(us => us. == id);
            tblShiftAllocation.Date = Shiftallocation.Date;
            tblShiftAllocation.EmployeeName = Shiftallocation.EmployeeName;
            tblShiftAllocation.Note = Shiftallocation.Note;
            tblShiftAllocation.ShiftType = Shiftallocation.ShiftType;
            //tblShiftAllocation.Employee_ID = Shiftallocation.Employee_ID;
            entity.tbl_ShiftAllocation.Add(tblShiftAllocation);

            foreach (var item in Shiftallocation.lstAllotcation)
            {
                tbl_ShiftAllocationDetails details = new tbl_ShiftAllocationDetails();
                details.Nozzel = item.Nozzel;
                details.Opening = item.Opening;
                details.Rate = item.Rate;
                details.Employee_ID = item.Employee_ID;
               // details.EmployeeName = item.EmployeeName;
                details.Date = item.Date;
                details.Shift = item.Shift;
                details.Note = item.Note;
                details.Tank = item.Tank;
                // details.Nozzel = item.Nozzel;
                // details.Opening = item.Opening;
                details.Closing = item.Closing;
                details.TotalSale = item.TotalSale;
                details.TotalAmt = item.TotalAmt;
                details.ByCash = item.ByCash;
                details.CreditCardCash = item.CreditCardCash;
                details.DebitCardCash = item.DebitCardCash;
                details.GrandTotalSale = item.GrandTotalSale;
                details.GrandTotalAmount = item.GrandTotalAmount;
                details.PumpTesting = item.PumpTesting;
                details.ShiftAllocationMasterID = tblShiftAllocation.Id;
                tblShiftAllocation.tbl_ShiftAllocationDetails.Add(details);
                //entity.tbl_ShiftAllocation.Add(tblShiftAllocation);
                // int result11 = entity.SaveChanges();
            }


            int result1 = entity.SaveChanges();
            //return tblShiftAllocation.Id; //if want to return ID
            return result1 > 0;
            //throw new NotImplementedException();
        }
        public bool UpdateShiftAllocationDetails(Models.ShiftAllocation Shiftallocation)
        {
            tbl_ShiftAllocation tblShiftAllocation = entity.tbl_ShiftAllocation.FirstOrDefault(us => us.Id == Shiftallocation.ID);
            if (tblShiftAllocation != null)
            {
                tblShiftAllocation.Date = Shiftallocation.Date;
                tblShiftAllocation.EmployeeName = Shiftallocation.EmployeeName;
                tblShiftAllocation.Note = Shiftallocation.Note;
                tblShiftAllocation.ShiftType = Shiftallocation.ShiftType;
                tblShiftAllocation.Employee_ID = Shiftallocation.Employee_ID;

                // tblShiftAllocation.Shift = Shiftallocation.Shift;
                foreach (var item in Shiftallocation.lstAllotcation)
                {
                    tbl_ShiftAllocationDetails details = entity.tbl_ShiftAllocationDetails.FirstOrDefault(us => us.ShiftAllocationMasterID == Shiftallocation.ID && us.ID == item.ID);
                    if (item.ID <= 0)
                        details = new tbl_ShiftAllocationDetails();
                    details.Nozzel = item.Nozzel;
                    details.Opening = item.Opening;
                    details.Rate = item.Rate;
                    details.Employee_ID = item.Employee_ID;
                   // details.EmployeeName = item.EmployeeName;
                    details.Date = item.Date;
                    details.Shift = item.Shift;
                    details.Note = item.Note;
                    details.Tank = item.Tank;
                    details.Nozzel = item.Nozzel;
                    details.Opening = item.Opening;
                    details.Closing = item.Closing;
                    details.TotalAmt = item.TotalAmt;
                    details.TotalSale = item.TotalSale;
                    details.Rate = item.Rate;
                    details.ByCash = item.ByCash;
                    details.CreditCardCash = item.CreditCardCash;
                    details.DebitCardCash = item.DebitCardCash;
                    details.GrandTotalSale = item.GrandTotalSale;
                    details.GrandTotalAmount = item.GrandTotalAmount;
                    details.PumpTesting = item.PumpTesting;
                    details.ShiftAllocationMasterID = tblShiftAllocation.Id;
                    if (item.ID <= 0)
                        tblShiftAllocation.tbl_ShiftAllocationDetails.Add(details);
                }


                //entity.tbl_ShiftAllocation.Add(tblShiftAllocation);
                // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

        // public bool RemoveShiftAllocationDetails(long ID)
        //{
        //    tbl_ShiftAllocationDetails smastr = entity.tbl_ShiftAllocationDetails.FirstOrDefault(us => us.ID == ID);
        //    if (smastr != null)
        //    {
        //        entity.tbl_ShiftAllocationDetails.Remove(smastr);
        //        int result = entity.SaveChanges();
        //        return result > 0;
        //    }
        //    return false;
        //}


        public bool AddShiftDetails(ShiftAllocationDetails Shiftallocationdetails)
        {

            tbl_ShiftAllocationDetails details = new tbl_ShiftAllocationDetails();
            details.ShiftAllocationMasterID = Shiftallocationdetails.ShiftAllocationMasterID;
            details.Nozzel = Shiftallocationdetails.Nozzel;
            details.Opening = Shiftallocationdetails.Opening;
            details.Rate = Shiftallocationdetails.Rate;
            details.Employee_ID = Shiftallocationdetails.Employee_ID;
        //    details.EmployeeName = Shiftallocationdetails.EmployeeName;
            details.Date = Shiftallocationdetails.Date;
            details.Shift = Shiftallocationdetails.Shift;
            details.Note = Shiftallocationdetails.Note;
            details.Tank = Shiftallocationdetails.Tank;
            details.Nozzel = Shiftallocationdetails.Nozzel;
            details.Opening = Shiftallocationdetails.Opening;
            details.Closing = Shiftallocationdetails.Closing;
            details.TotalSale = Shiftallocationdetails.TotalSale;
            details.Rate = Shiftallocationdetails.Rate;
            details.ByCash = Shiftallocationdetails.ByCash;
            details.CreditCardCash = Shiftallocationdetails.CreditCardCash;
            details.DebitCardCash = Shiftallocationdetails.DebitCardCash;
            details.GrandTotalSale = Shiftallocationdetails.GrandTotalSale;
            details.GrandTotalAmount = Shiftallocationdetails.GrandTotalAmount;
            //details.ShiftAllocationMasterID = tblShiftAllocation.Id;
            //  tblShiftAllocation.tbl_ShiftAllocationDetails.Add(details);

            entity.tbl_ShiftAllocationDetails.Add(details);
            int result = entity.SaveChanges();
            //   entity.Shiftallocationdetails.Add(details);
            return result > 0;


        }

        //public List<ShiftAllocationDetails> Filter(SearchRecord searchRecord)
        //{
        //    searchRecord.EndDate = searchRecord.EndDate.Date == DateTime.MinValue.Date ? DateTime.Now : searchRecord.EndDate;
        //    var q = (from a in entity.tbl_ShiftAllocation
        //             //join shiftDetails in entity.tbl_ShiftAllocationDetails
        //             //on a.Id equals shiftDetails.ShiftAllocationMasterID
        //             where a.Date >= searchRecord.StartDate.Date && a.Date <= searchRecord.EndDate.Date
        //             && 1 == (string.IsNullOrEmpty(searchRecord.category) ? 1 :
        //           (a.Note.Contains(searchRecord.category)
        //           //|| a.Note.Contains(searchRecord.category)
        //           ) ? 1 : 0)
        //             select new
        //             {
        //                 a.Id,
        //                 a.Note,
        //                 a.tbl_ShiftAllocationDetails
        //             }
        //           ).ToList();
        //    List<ShiftAllocationDetails> allocation = new List<ShiftAllocationDetails>();
        //    foreach (var item in q)
        //    {
        //        foreach (var details in item.tbl_ShiftAllocationDetails)
        //        {
        //            ShiftAllocationDetails shift = new ShiftAllocationDetails();
        //            shift.ID = item.Id;
        //            shift.Note = item.Note;
        //            shift.Nozzel = details.Nozzel;
        //            shift.Opening = details.Opening;
        //            shift.PumpTesting = details.PumpTesting;
        //            allocation.Add(shift);
        //        }
        //        //   allocation.Add(shift);
        //    }
        //    return allocation;
        //}
        public List<ShiftAllocationDetails> Filter(SearchRecord searchRecord)
        {
            searchRecord.EndDate = searchRecord.EndDate.Date == DateTime.MinValue.Date ? DateTime.Now.AddDays(1) : searchRecord.EndDate;
            var result = entity.STPShiftAllocationsearch(searchRecord.category, searchRecord.StartDate, searchRecord.EndDate).ToList();
            List<ShiftAllocationDetails> allocation = new List<ShiftAllocationDetails>();
            foreach (var item in result)
            {
                //foreach (var details in item.tbl_ShiftAllocationDetails)
                {
                    ShiftAllocationDetails shift = new ShiftAllocationDetails();
                    shift.ID = item.AllocationID;
                    shift.Note = item.Note;
                    shift.Nozzel = item.Nozzel;
                    shift.Opening = item.Opening;
                    shift.PumpTesting = item.PumpTesting;
                    allocation.Add(shift);
                } 
                //   allocation.Add(shift);
            }
            return allocation;
        }

        public List<ShiftAllocation> GetShiftMaster()
        {
            return (from sa in entity.tbl_ShiftAllocation
                    join sm in entity.tbl_ShiftMaster
                    on sa.ShiftType equals sm.Id
                    select new ShiftAllocation()
                    {

                        ID=sa.Id,
                        ShiftType=sa.ShiftType,
                        ShiftDes=sm.Type,
                        EmployeeName=sa.EmployeeName,
                        Note=sa.Note
                       

                    } ).ToList();
                  
        }

        public List<ShiftAllocation> GetEmployeeRegMaster()
        {
            return (from sa in entity.tbl_ShiftAllocation
                    join empreg in entity.tbl_EmployeeRegister
                    on sa.EmployeeName equals empreg.Id
                    select new ShiftAllocation()
                    {

                       ID = sa.Id,
                       EmployeeName = sa.EmployeeName,
                       EmployeeNameDes = empreg.EmployeeName,
                        // EmployeeName=sa.EmployeeName,
                        Note = sa.Note


                    }).ToList();

        }

        public List<ShiftAllocationDetails> GetTankMaster()
        {
            return (from sad in entity.tbl_ShiftAllocationDetails
                    join tm in entity.tbl_TankMaster
                    on sad.Tank equals tm.Id
                    select new ShiftAllocationDetails()
                    {

                        ID = sad.ID,
                        Tank = sad.Tank,
                        TankDes = tm.Type,
                        // EmployeeName=sa.EmployeeName,
                        Rate= sad.Rate,


                    }).ToList();
        }

        public List<ShiftAllocationDetails> GetNozzelMaster()
        {
            return (from sad in entity.tbl_ShiftAllocationDetails
                    join nm in entity.tbl_NozzleMaster
                    on sad.Nozzel equals nm.Id
                    select new ShiftAllocationDetails()
                    {

                        ID = sad.ID,
                        Nozzel = sad.Nozzel,
                        NozzelDes = nm.Type,
                       // EmployeeName = sad.EmployeeName,
                       // Note = sad.Note


                    }).ToList();
        }

        List<ShiftAllocationDetails> IShiftAllocationDetailsRepository.Get()
        {
            throw new NotImplementedException();
        }
    }
}