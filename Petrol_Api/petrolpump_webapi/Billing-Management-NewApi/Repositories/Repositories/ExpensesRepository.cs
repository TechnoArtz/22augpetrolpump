﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class ExpensesRepository:IExpensesRepository
    {
        GSDBSExtended entity;
        public ExpensesRepository()
        {
            entity = new GSDBSExtended();

        }
        public Models.Expenses Get(long ID)
        {
            return entity.tbl_Expenses.Where(us => us.Id == ID).Select(us => new Expenses()
            {
                ID = us.Id,
                Date=us.Date,
                Category=us.Category,
                PaymentMode=us.PaymentMode,
                BankName=us.BankName,
                ChequeNo=us.ChequeNo,
                Amount=us.Amount,
                Note=us.Note,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
        public List<Models.Expenses> Get()
        {
            return entity.tbl_Expenses.Select(us => new Expenses()
            {
                ID = us.Id,
                Date=us.Date,
                Category = us.Category,
                PaymentMode = us.PaymentMode,
                BankName = us.BankName,
                ChequeNo = us.ChequeNo,
                Amount = us.Amount,
                Note = us.Note,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).ToList();
        }
        public bool CreateExpenses(Models.Expenses expenses)
        {
            tbl_Expenses exp = new tbl_Expenses();
            exp.Date = expenses.Date.HasValue ? expenses.Date.Value : DateTime.Now;
            exp.Category = expenses.Category;
            exp.PaymentMode = expenses.PaymentMode;
            exp.BankName = expenses.BankName;
            exp.ChequeNo = expenses.ChequeNo;
            exp.Amount = expenses.Amount;
            exp.Note = expenses.Note;
            exp.CreatedBy = expenses.CreatedBy;
            exp.CreatedDate = DateTime.Now;

            entity.tbl_Expenses.Add(exp);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveExpenses(long ID)
        {
            tbl_Expenses exp = entity.tbl_Expenses.FirstOrDefault(us => us.Id == ID);
            if (exp != null)
            {
                entity.tbl_Expenses.Remove(exp);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateExpenses(Models.Expenses expenses)
        {
            tbl_Expenses exp = entity.tbl_Expenses.FirstOrDefault(us => us.Id == expenses.ID);
            if (exp != null)
            {
                exp.Date = expenses.Date;
                exp.Category = expenses.Category;
                exp.PaymentMode = expenses.PaymentMode;
                exp.BankName = expenses.BankName;
                exp.ChequeNo = expenses.ChequeNo;
                exp.Amount = expenses.Amount;
                exp.Note = expenses.Note;
                exp.ModifiedBy = expenses.ModifiedBy;
                exp.ModifiedDate = expenses.ModifiedDate;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}