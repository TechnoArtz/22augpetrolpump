using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class ProductSaleMasterRepository:IProductSaleMasterRespository
    {
        GSDBSExtended entity;
        public ProductSaleMasterRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.ProductSaleMaster Get(long ID)
        {
            var q = (from a in entity.tbl_ProductSaleMaster
                     join details in entity.tbl_ProductSaleDetails
                     on a.Id equals details.ProductSaleMasterID
                     where a.Id == ID
                     select new
                     {
                         a.Id,
                         a.PartyName,
                         a.ShiftType,
                         a.TankType,
                         a.NozzelNo,
                         a.tbl_ProductSaleDetails
                     }
               ).FirstOrDefault();
            ProductSaleMaster crm = new ProductSaleMaster();
            crm.ID = q.Id;
            crm.PartyName = q.PartyName;
            crm.ShiftType = q.ShiftType;
            crm.TankType = q.TankType;
            crm.NozzelNo = q.NozzelNo;
            crm.lstProductsaledetails = new List<ProductSaleDetails>();
            foreach (var us in q.tbl_ProductSaleDetails)
            {
                ProductSaleDetails ced = new ProductSaleDetails();
                ced.ID = us.Id;
                ced.PartyName = us.PartyName;
                ced.ShiftType = us.ShiftType;
                ced.TankType = us.TankType;
                ced.NozzelNo = us.NozzelNo;
                ced.ItemType = us.ItemType;
                ced.Qty = us.Qty;
                ced.Rate = us.Rate;
                ced.Total = us.Total;
                crm.lstProductsaledetails.Add(ced);
            }
            //   allocation.Add(shift);
            return crm;
        }
        public List<Models.ProductSaleDetails> Get()
        {
            return (from us in entity.tbl_ProductSaleDetails
                    join obj in entity.tbl_ProductSaleMaster
                    on us.ProductSaleMasterID equals obj.Id
                    //join shift in entity.tbl_ShiftMaster
                    //on us.PartyName equals shift.Id
                    select new ProductSaleDetails()

                    {
                        ID = us.Id,
                        ProductSaleMasterID = us.ProductSaleMasterID,
                        PartyName = obj.PartyName,

                        //CreditorNameDes= addcrd.CreditorName,
                        ShiftType = obj.ShiftType,
                        // ShiftTypeDes = shift.Type,
                        TankType = obj.TankType,
                        NozzelNo = obj.NozzelNo,
                        ItemType = us.ItemType,
                        Rate = us.Rate,
                        Qty = us.Qty,
                        Total = us.Total,
                        //ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                        //ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
                    }).ToList();
        }
        public bool CreateProductSale(Models.ProductSaleMaster productsalemaster)
        {
            tbl_ProductSaleMaster tblproductsalemaster = new tbl_ProductSaleMaster(); //entity.tbl_ShiftAllocation.FirstOrDefault(us => us. == id);
            tblproductsalemaster.PartyName = productsalemaster.PartyName;
            tblproductsalemaster.ShiftType = productsalemaster.ShiftType;
            tblproductsalemaster.TankType = productsalemaster.TankType;
            tblproductsalemaster.NozzelNo = productsalemaster.NozzelNo;
            // tblCreditorEntryMaster.Employee_ID = creditorentrymaster.Employee_ID;
            entity.tbl_ProductSaleMaster.Add(tblproductsalemaster);
            foreach (var item in productsalemaster.lstProductsaledetails)
            {
                tbl_ProductSaleDetails details = new tbl_ProductSaleDetails();

                details.ItemType = item.ItemType;
                details.Rate = item.Rate;
                details.Qty = item.Qty;
                details.Total = item.Total;

                details.CreatedDate = item.CreatedDate;
                details.ProductSaleMasterID = tblproductsalemaster.Id;
                tblproductsalemaster.tbl_ProductSaleDetails.Add(details);
                //entity.tbl_CreditorEntryMaster.Add(tblCreditorEntryMaster);
                // int result11 = entity.SaveChanges();
            }
            int result1 = entity.SaveChanges();
            //return tblShiftAllocation.Id; //if want to return ID
            return result1 > 0;
            //throw new NotImplementedException();
        }
        public bool UpdateProductSale(Models.ProductSaleMaster productsalemaster)
        {
            tbl_ProductSaleMaster tblproductdalemaster = entity.tbl_ProductSaleMaster.FirstOrDefault(us => us.Id == productsalemaster.ID);
            if (tblproductdalemaster != null)
            {
                tblproductdalemaster.PartyName = productsalemaster.PartyName;
                tblproductdalemaster.ShiftType = productsalemaster.ShiftType;
                tblproductdalemaster.TankType = productsalemaster.TankType;
                tblproductdalemaster.NozzelNo = productsalemaster.NozzelNo;
                // tblShiftAllocation.Shift = Shiftallocation.Shift;
                foreach (var item in productsalemaster.lstProductsaledetails)
                {
                    //tbl_CreaditorEntryDetails details = new tbl_CreaditorEntryDetails();
                    tbl_ProductSaleDetails details = entity.tbl_ProductSaleDetails.FirstOrDefault(us => us.ProductSaleMasterID == productsalemaster.ID && us.Id == item.ID);
                    if (item.ID <= 0)
                        details = new tbl_ProductSaleDetails();
                    details.PartyName = item.PartyName;
                    details.ShiftType = item.ShiftType;
                    details.TankType = item.TankType;
                    details.NozzelNo = item.NozzelNo;
                    details.ItemType = item.ItemType;
                    details.Rate = item.Rate;
                    details.Qty = item.Qty;
                    details.Total = item.Total;
                    details.CreatedDate = item.CreatedDate;
                    details.ProductSaleMasterID = tblproductdalemaster.Id;
                    //       if(item.ID<=0)
                    tblproductdalemaster.tbl_ProductSaleDetails.Add(details);
                }
                //entity.tbl_ShiftAllocation.Add(tblShiftAllocation);
                // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool RemoveProductSale(long ID)
        {
            tbl_ProductSaleDetails product = entity.tbl_ProductSaleDetails.FirstOrDefault(us => us.ProductSaleMasterID == ID);
            if (product != null)
            {
                entity.tbl_ProductSaleDetails.Remove(product);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

    }
}