﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class ShiftEntryRepository :IShiftEntryRepository
    {
        GSDBSExtended entity;
        public ShiftEntryRepository()
        {
            entity = new GSDBSExtended();
        }

        public Models.ShiftEntry Get(long ID)
        {
            return entity.tbl_ShiftEntry.Where(us => us.Id == ID).Select(us => new ShiftEntry()
            {
                ID = us.Id,
                Date=us.Date,
                ShiftType = us.ShiftType,
                EmployeeName = us.EmployeeName,
                CreatedDate=us.CreatedDate,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,

            }).FirstOrDefault();
        }

        public List<Models.ShiftEntry> Get()
        {
            //return entity.tbl_ShiftEntry.Select(us => new ShiftEntry()
            return (from sEntry in entity.tbl_ShiftEntry
                    join sMaster in entity.tbl_ShiftMaster
                    on sEntry.ShiftType equals sMaster.Id
                    join SEmp in entity.tbl_EmployeeRegister
                    on sEntry.EmployeeName equals SEmp.Id
                    select new ShiftEntry()

                    {
                        // EncShiftID = sEntry.ShiftType.ToString(),
                        ID = sEntry.Id,
                        Date = sEntry.Date,
                        ShiftType = sEntry.ShiftType,
                        ShiftTypeDec = sMaster.Type,
                        EmployeeName = sEntry.EmployeeName,
                        EmployeeNameDes = SEmp.EmployeeName,

                        CreatedDate=sEntry.CreatedDate,
                        CreatedBy = sEntry.CreatedBy.HasValue ? sEntry.CreatedBy.Value : 0,

                    }).ToList();
        }

        public bool CreateShiftEntry(Models.ShiftEntry shiftentry)
        {
            tbl_ShiftEntry sentry = new tbl_ShiftEntry();

            sentry.ShiftType = shiftentry.ShiftType;
            sentry.EmployeeName = shiftentry.EmployeeName;
            sentry.Date = shiftentry.Date;
            sentry.CreatedDate = shiftentry.CreatedDate;
            sentry.CreatedBy = shiftentry.CreatedBy;

            entity.tbl_ShiftEntry.Add(sentry);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }

        public bool RemoveShiftEntry(long ID)
        {
            tbl_ShiftEntry sentry = entity.tbl_ShiftEntry.FirstOrDefault(us => us.Id == ID);
            if (sentry != null)
            {
                entity.tbl_ShiftEntry.Remove(sentry);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

        public bool UpdateShiftEntry(Models.ShiftEntry shiftentry)
        {
            tbl_ShiftEntry sentry = entity.tbl_ShiftEntry.FirstOrDefault(us => us.Id == shiftentry.ID);
            if (sentry != null)
            {
                sentry.ShiftType = shiftentry.ShiftType;
                sentry.EmployeeName = shiftentry.EmployeeName;
                sentry.CreatedDate = shiftentry.CreatedDate;
                sentry.CreatedBy = shiftentry.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}