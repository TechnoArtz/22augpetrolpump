﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class ShiftMasterRepository : IShiftMasterRepository
    {
        GSDBSExtended entity;
        public ShiftMasterRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.ShiftMaster Get(long ID)
        {
            return entity.tbl_ShiftMaster.Where(us => us.Id == ID).Select(us => new ShiftMaster()
            {
                ID = us.Id,
                Type = us.Type,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                StartTime = us.StartTime,
                EndTime = us.EndTime,
                // CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
        public List<Models.ShiftMaster> Get()
        {
            return entity.tbl_ShiftMaster.Select(us => new ShiftMaster()
            {
                ID = us.Id,
                Type = us.Type,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                StartTime = us.StartTime,
                EndTime = us.EndTime,
                //CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,

            }).ToList();
        }
        public bool CreateShiftMaster(Models.ShiftMaster shiftmaster)
        {
            tbl_ShiftMaster smastr = new tbl_ShiftMaster();
            smastr.Type = shiftmaster.Type;
            smastr.CreatedBy = shiftmaster.CreatedBy;
            smastr.StartTime = shiftmaster.StartTime;
            smastr.EndTime = shiftmaster.EndTime;

            entity.tbl_ShiftMaster.Add(smastr);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveShiftMaster(long ID)
        {
            tbl_ShiftMaster smastr = entity.tbl_ShiftMaster.FirstOrDefault(us => us.Id == ID);
            if (smastr != null)
            {
                entity.tbl_ShiftMaster.Remove(smastr);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateShiftMaster(Models.ShiftMaster shiftmaster)
        {
            tbl_ShiftMaster smastr = entity.tbl_ShiftMaster.FirstOrDefault(us => us.Id == shiftmaster.ID);
            if (smastr != null)
            {
                smastr.Type = shiftmaster.Type;
                smastr.CreatedBy = shiftmaster.CreatedBy;
                smastr.StartTime = shiftmaster.StartTime;
                smastr.EndTime = shiftmaster.EndTime;
                // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}