using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class TankMasterRepository:ITankMasterRepository
    {
        
         GSDBSExtended entity;
         public TankMasterRepository()
        {
            entity = new GSDBSExtended();
           
        }
         public Models.TankMaster Get(long ID)
        {
            return entity.tbl_TankMaster.Where(us => us.Id == ID).Select(us => new TankMaster()
            {
                ID = us.Id,
                Type = us.Type,
                TankRate=us.TankRate,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                //ModifiedBy=   us.ModifiedBy.HasValue ? us.ModifiedBy.Value :0,
                //ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
         public List<Models.TankMaster> Get()
        {
            return entity.tbl_TankMaster.Select(us => new TankMaster()
            {
                ID = us.Id,
                Type = us.Type,
                TankRate=us.TankRate,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                //ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                //ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,

            }).ToList();
        }
         public bool CreateTankMaster(Models.TankMaster tankmaster)
        {
            tbl_TankMaster tmastr = new tbl_TankMaster();
            tmastr.Type = tankmaster.Type;
            tmastr.TankRate = tankmaster.TankRate;
            tmastr.CreatedBy = tankmaster.CreatedBy;
            //tmastr.CreatedDate = tankmaster.CreatedDate;
            //tmastr.ModifiedBy = tankmaster.ModifiedBy;
            //tmastr.ModifiedDate = tankmaster.ModifiedDate;
            entity.tbl_TankMaster.Add(tmastr);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
         public bool RemoveTankMaster(long ID)
        {
            tbl_TankMaster tmastr = entity.tbl_TankMaster.FirstOrDefault(us => us.Id == ID);
            if (tmastr != null)
            {
                entity.tbl_TankMaster.Remove(tmastr);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
         public bool UpdateTankMaster(Models.TankMaster tankmaster)
        {
            tbl_TankMaster tmastr = entity.tbl_TankMaster.FirstOrDefault(us => us.Id == tankmaster.ID);
            if (tmastr != null)
            {
                tmastr.Type = tankmaster.Type;
                tmastr.TankRate = tankmaster.TankRate;
               // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}