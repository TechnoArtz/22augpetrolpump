﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class DebitorRepository : IDebitorRepository
    {
        GSDBSExtended entity;
        public DebitorRepository()
        {
            entity = new GSDBSExtended();
        }
        public bool CreateDebitor(Debitor debitor)
        {
            tbl_Debitor cust = new tbl_Debitor();
            cust.CustomerName = debitor.CustomerName;
            cust.PreviousAmt = debitor.PreviousAmt;
            cust.PaidAmt = debitor.PaidAmt;
            cust.TotalAmt = debitor.TotalAmt;
            cust.Status = debitor.Status;

          

            entity.tbl_Debitor.Add(cust);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }

        public Debitor Get(long ID)
        {
            return entity.tbl_Debitor.Where(us => us.Id == ID).Select(us => new Debitor()
            {
                ID = us.Id,
                CustomerName=us.CustomerName,
                PreviousAmt=us.PreviousAmt,
                PaidAmt=us.PaidAmt,
                TotalAmt=us.TotalAmt,
                Status=us.Status,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,


            }).FirstOrDefault();
        }

        public List<Debitor> Get()
        {
            //return entity.tbl_Debitor.Select(us => new Debitor()
            return (from us in entity.tbl_Debitor
                    join cust in entity.tbl_CustomerMaster
                    on us.CustomerName equals cust.Id
                select new Debitor()

            {
                ID = us.Id,
                CustomerName = us.CustomerName,
                CustomerNameDes=cust.CustomerName,
                PreviousAmt = us.PreviousAmt,
                PaidAmt = us.PaidAmt,
                TotalAmt = us.TotalAmt,
                Status = us.Status,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,


            }).ToList();
        }
    }
}