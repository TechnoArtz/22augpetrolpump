﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class DipMSRepository : IDipMSRepository
    {
        GSDBSExtended entity;
        public DipMSRepository()
        {
            entity = new GSDBSExtended();
        }
        public bool CreateDipMS(Models.DipMS dipms)
        {
            tbl_DipMS dipm = new tbl_DipMS();
            dipm.Date = dipms.Date.HasValue? dipm.Date.Value : DateTime.Now; ;
            dipm.CurrentDip = dipms.CurrentDip;
            dipm.ActualDip = dipms.ActualDip;
            dipm.Variation = dipms.Variation;
         //   dipm.CreatedBy = dipms.CreatedBy;
            //nmastr.Type = nozzlemaster.Type;
            dipm.CreatedDate = DateTime.Now;

            entity.tbl_DipMS.Add(dipm);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }

        public Models.DipMS Get(long ID)
        {
            return entity.tbl_DipMS.Where(us => us.Id == ID).Select(us => new DipMS()
            {
                ID = us.Id,
                Date=us.Date,
                CurrentDip = us.CurrentDip,
                ActualDip = us.ActualDip,
                Variation = us.Variation,

            }).FirstOrDefault();
        }

        public List<Models.DipMS> Get()
        {
            return entity.tbl_DipMS.Select(us => new DipMS()
            {
                ID = us.Id,
                Date=us.Date,
                CurrentDip = us.CurrentDip,
                ActualDip = us.ActualDip,
                Variation = us.Variation,

            }).ToList();
        }

        public bool RemoveDipMS(long ID)
        {
            tbl_DipMS dipm = entity.tbl_DipMS.FirstOrDefault(us => us.Id == ID);
            if (dipm != null)
            {
                entity.tbl_DipMS.Remove(dipm);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

        public bool UpdateDipMS(Models.DipMS dipms)
        {
            tbl_DipMS dipm = entity.tbl_DipMS.FirstOrDefault(us => us.Id == dipms.ID);
            if (dipm != null)
            {
                dipm.Date = dipms.Date;
                dipm.CurrentDip = dipms.CurrentDip;
                dipm.ActualDip = dipms.ActualDip;
                dipm.Variation = dipms.Variation;
               // dipm.ModifiedBy = dipms.ModifiedBy;
                dipm.ModifiedDate = DateTime.Now;

                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}