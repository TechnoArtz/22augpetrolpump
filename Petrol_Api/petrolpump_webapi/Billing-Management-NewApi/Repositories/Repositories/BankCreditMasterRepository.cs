﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class BankCreditMasterRepository : IBankCreditMasterRepository
    {
        GSDBSExtended entity;
        public BankCreditMasterRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.BankCreditMaster Get(long ID)
        {
            return entity.tbl_BankCreditEntry.Where(us => us.Id == ID).Select(us => new BankCreditMaster()
            {
                ID = us.Id,
                Date = us.Date,
                Shift = us.Shift,
                AccountName = us.AccountName,
                EmployeeName = us.EmployeeName,
                PaymentMode = us.PaymentMode,
                BankName = us.BankName,
                BankAccNo = us.BankAccNo,
                Amount = us.Amount,
                Note = us.Note,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
        public List<Models.BankCreditMaster> Get()
        {
            return (from us in entity.tbl_BankCreditEntry
                    join shift in entity.tbl_ShiftMaster
                    on us.Shift equals shift.Id
                    join emp in entity.tbl_EmployeeRegister
                    on us.EmployeeName equals emp.Id
                    select new BankCreditMaster()
                    {
                        ID = us.Id,
                        Date = us.Date,
                        Shift = us.Shift,
                        ShiftDes = shift.Type,
                        AccountName = us.AccountName,
                        EmployeeName = us.EmployeeName,
                        EmployeeNameDes = emp.EmployeeName,
                        PaymentMode = us.PaymentMode,
                        BankName = us.BankName,
                        BankAccNo = us.BankAccNo,
                        Amount = us.Amount,
                        Note = us.Note,
                        CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                        CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                        ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                        ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
                    }).ToList();
        }
        public bool CreateBankCredit(Models.BankCreditMaster bankCredit)
        {
            tbl_BankCreditEntry bank = new tbl_BankCreditEntry();
            bank.Date = bankCredit.Date;
            bank.Shift = bankCredit.Shift;
            bank.AccountName = bankCredit.AccountName;
            bank.EmployeeName = bankCredit.EmployeeName;
            bank.PaymentMode = bankCredit.PaymentMode;
            bank.BankName = bankCredit.BankName;
            bank.BankAccNo = bankCredit.BankAccNo;
            bank.Amount = bankCredit.Amount;
            bank.Note = bankCredit.Note;
            bank.CreatedBy = bankCredit.CreatedBy;
            bank.CreatedDate = bankCredit.CreatedDate;
            bank.ModifiedBy = bankCredit.ModifiedBy;
            bank.ModifiedDate = bankCredit.ModifiedDate;
            entity.tbl_BankCreditEntry.Add(bank);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveBankCredit(long ID)
        {
            tbl_BankCreditEntry bank = entity.tbl_BankCreditEntry.FirstOrDefault(us => us.Id == ID);
            if (bank != null)
            {
                entity.tbl_BankCreditEntry.Remove(bank);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateBankCredit(Models.BankCreditMaster bankCredit)
        {
            tbl_BankCreditEntry bank = entity.tbl_BankCreditEntry.FirstOrDefault(us => us.Id == bankCredit.ID);
            if (bank != null)
            {
                bank.Date = bankCredit.Date;
                bank.Shift = bankCredit.Shift;
                bank.AccountName = bankCredit.AccountName;
                bank.EmployeeName = bankCredit.EmployeeName;
                bank.PaymentMode = bankCredit.PaymentMode;
                bank.BankName = bankCredit.BankName;
                bank.BankAccNo = bankCredit.BankAccNo;
                bank.Amount = bankCredit.Amount;
                bank.Note = bankCredit.Note;
                bank.CreatedBy = bankCredit.CreatedBy;
                bank.CreatedDate = bankCredit.CreatedDate;
                bank.ModifiedBy = bankCredit.ModifiedBy;
                bank.ModifiedDate = bankCredit.ModifiedDate;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}