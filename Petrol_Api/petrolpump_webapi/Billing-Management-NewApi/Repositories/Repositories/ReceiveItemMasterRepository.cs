using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class ReceiveItemMasterRepository : IReceiveItemMasterRepository
    {
        GSDBSExtended entity;
        public ReceiveItemMasterRepository()
        {
            entity = new GSDBSExtended();
        }
        public bool CreateReceiveItemMaster(ReceiveItemMaster receiveitem)
        {
            tbl_ReceiveItem recitem = new tbl_ReceiveItem();
            recitem.Date = receiveitem.Date;
            recitem.TankType= receiveitem.TankType;
            recitem.Qty = receiveitem.Qty;


            recitem.Createdby = receiveitem.Createdby;
            recitem.Createddate = receiveitem.Createddate;
            entity.tbl_ReceiveItem.Add(recitem);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }

        public ReceiveItemMaster Get(long ID)
        {
            
            return entity.tbl_ReceiveItem.Where(us => us.Id == ID).Select(us => new ReceiveItemMaster()
            {
                ID = us.Id,
                Date=us.Date,
                TankType=us.TankType,
                Qty=us.Qty,

                //CreatedBy=us.CreatedBy
                Createdby = us.Createdby.HasValue ? us.Createdby.Value : 0,
                Createddate = us.Createddate.HasValue ? us.Createddate.Value : DateTime.Now,
                Modifiedby = us.Modifiedby.HasValue ? us.Modifiedby.Value : 0,
                Modifieddate = us.Modifieddate.HasValue ? us.Modifieddate.Value : DateTime.Now

                // CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }

        public List<ReceiveItemMaster> Get()
        {
            return (from us in entity.tbl_ReceiveItem
                    join tank in entity.tbl_TankMaster
                    on us.TankType equals tank.Id
                   
                    select new ReceiveItemMaster()
                    {   
                        ID=us.Id,
                        Date=us.Date,
                        TankType=us.TankType,
                        TankTypeDes=tank.Type,
                        Qty=us.Qty,
                        

                        //CreatedBy=us.CreatedBy
                        Createdby = us.Createdby.HasValue ? us.Createdby.Value : 0,
                        Createddate = us.Createddate.HasValue ? us.Createddate.Value : DateTime.Now,
                        Modifiedby = us.Modifiedby.HasValue ? us.Modifiedby.Value : 0,
                        Modifieddate = us.Modifieddate.HasValue ? us.Modifieddate.Value : DateTime.Now
                        //CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,

                    }).ToList();
        }

        public bool RemoveReceiveItemMaster(long ID)
        {
            tbl_ReceiveItem recitem = entity.tbl_ReceiveItem.FirstOrDefault(us => us.Id == ID);
            if (recitem != null)
            {
                entity.tbl_ReceiveItem.Remove(recitem);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

        public bool UpdateReceiveItemMaster(ReceiveItemMaster receiveItem)
        {
            tbl_ReceiveItem recitem = entity.tbl_ReceiveItem.FirstOrDefault(us => us.Id == receiveItem.ID);
            if (recitem != null)
            {
                recitem.Date = receiveItem.Date;
                recitem.TankType = receiveItem.TankType;
                recitem.Qty = receiveItem.Qty;


                recitem.Modifiedby = receiveItem.Modifiedby;
                recitem.Modifieddate = receiveItem.Modifieddate;

                // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}