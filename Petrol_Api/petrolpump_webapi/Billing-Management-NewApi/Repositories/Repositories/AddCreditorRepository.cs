using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class AddCreditorRepository:IAddCreditorRepository
    {

         GSDBSExtended entity;
        public AddCreditorRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.AddCreditor Get(long id)
        {
            return entity.tbl_AddCreditor.Where(us => us.Id == id).Select(us => new AddCreditor()
            {
                ID= us.Id,
                CreditorName = us.CreditorName,
               // CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                Address = us.Address,
                MobileNo = us.MobileNo,
               
            }).FirstOrDefault();
        }

        public List<Models.AddCreditor> Get()
        {
            return entity.tbl_AddCreditor.Select(us => new AddCreditor()
            {
                ID = us.Id,
                CreditorName = us.CreditorName,
                // CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                Address = us.Address,
                MobileNo = us.MobileNo,
              
                //CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,

            }).ToList();
        }

        public bool CreateAddCreditor(Models.AddCreditor addcreditor)
        {
            tbl_AddCreditor add = new tbl_AddCreditor();
            add.CreditorName = addcreditor.CreditorName;
   
            add.Address = addcreditor.Address;
            add.MobileNo = addcreditor.MobileNo;
            add.CreatedDate = DateTime.Now;
            entity.tbl_AddCreditor.Add(add);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }

        public bool UpdateAddCreditor(Models.AddCreditor addcreditor)
        {
            tbl_AddCreditor add = entity.tbl_AddCreditor.FirstOrDefault(us => us.Id == addcreditor.ID);
            if (add != null)
            {
                add.CreditorName = addcreditor.CreditorName;

                add.Address = addcreditor.Address;
                add.MobileNo = addcreditor.MobileNo;
                add.ModifiedDate = DateTime.Now;
                // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

        public bool RemoveAddCreditor(long ID)
        {
            tbl_AddCreditor add = entity.tbl_AddCreditor.FirstOrDefault(us => us.Id == ID);
            if (add != null)
            {
                entity.tbl_AddCreditor.Remove(add);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}