﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        GSDBSExtended entity;
        public EmployeeRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.Employee Get(long ID)
        {
            return entity.tbl_EmployeeRegister.Where(us => us.Id == ID).Select(us => new Employee()
            {
                ID = us.Id,
                Address = us.Address,
                MobileNo = us.MobileNo,
                EmployeeName = us.EmployeeName,
                Gender = us.Gender,
                UserRole = us.UserRole,
                Salary = us.Salary.HasValue ? us.Salary.Value : 0,
                DOB = us.DOB.HasValue ? us.DOB.Value : DateTime.Now,
                JoiningDate = us.JoiningDate.HasValue ? us.JoiningDate.Value : DateTime.Now,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
            }).FirstOrDefault();
        }
        public List<Models.Employee> Get()
        {
            return (from us in entity.tbl_EmployeeRegister
                    join dep in entity.tbl_DepartmentMaster
                    on us.UserRole equals dep.Id
                 
                    select new Employee()
                    {
                ID = us.Id,
                Address = us.Address,
                MobileNo = us.MobileNo,
                EmployeeName = us.EmployeeName,
                UserRole = us.UserRole,
                UserRoleDes=dep.Type,
                Gender = us.Gender,
                Salary = us.Salary.HasValue ? us.Salary.Value : 0,
                DOB = us.DOB.HasValue ? us.DOB.Value : DateTime.Now,
                JoiningDate = us.JoiningDate.HasValue ? us.JoiningDate.Value : DateTime.Now,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
            }).ToList();
            //return (from us in entity.tbl_EmployeeRegister
            //        join emp in entity.tbl_DepartmentMaster
            //        on us.UserRole equals emp.Id
            //        select new Employee()
            //        {
            //            ID = us.Id,
            //            UserRole = us.UserRole,
            //            UserRoleDes = emp.Type,
            //            Address = us.Address,
            //            MobileNo = us.MobileNo,
            //            EmployeeName = us.EmployeeName,
            //            Gender = us.Gender,
            //            Salary = us.Salary.HasValue ? us.Salary.Value : 0,
            //            DOB = us.DOB.HasValue ? us.DOB.Value : DateTime.Now,
            //            CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
            //            JoiningDate = us.JoiningDate.HasValue ? us.JoiningDate.Value : DateTime.Now,
            //        }).ToList();
        }
        public bool CreateEmployee(Models.Employee employee)
        {
            tbl_EmployeeRegister emp = new tbl_EmployeeRegister();
            emp.EmployeeName = employee.EmployeeName;
            emp.Address = employee.Address;
            emp.MobileNo = employee.MobileNo;
            emp.Salary = employee.Salary;
            emp.Gender = employee.Gender;
            emp.Photo = employee.Photo;
            emp.UserRole = employee.UserRole;
            emp.JoiningDate = employee.JoiningDate;
            emp.DOB = employee.DOB;
            emp.CreatedBy = employee.CreatedBy;
            entity.tbl_EmployeeRegister.Add(emp);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveEmployee(long ID)
        {
            tbl_EmployeeRegister emp = entity.tbl_EmployeeRegister.FirstOrDefault(us => us.Id == ID);
            if (emp != null)
            {
                entity.tbl_EmployeeRegister.Remove(emp);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateEmployee(Models.Employee employee)
        {
            tbl_EmployeeRegister emp = entity.tbl_EmployeeRegister.FirstOrDefault(us => us.Id == employee.ID);
            if (emp != null)
            {
                emp.EmployeeName = employee.EmployeeName;
                emp.Address = employee.Address;
                emp.Photo = employee.Photo;
                emp.MobileNo = employee.MobileNo;
                emp.Salary = employee.Salary;
                emp.UserRole = employee.UserRole;
                emp.Gender = employee.Gender;
                emp.JoiningDate = employee.JoiningDate;
                emp.DOB = employee.DOB;
                emp.CreatedBy = employee.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public List<Employee> GetUserRole()
        {
            return (from emp in entity.tbl_EmployeeRegister
                    join depart in entity.tbl_DepartmentMaster
                    on emp.UserRole equals depart.Id
                    select new Employee()
                    {
                        ID = emp.Id,
                        UserRole = emp.UserRole,
                        UserRoleDes = depart.Type
                    }
                       ).ToList();
        }
    }
}