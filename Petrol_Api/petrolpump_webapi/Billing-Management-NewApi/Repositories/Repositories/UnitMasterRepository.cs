using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class UnitMasterRepository : IUnitMasterRepository
    {
         GSDBSExtended entity;
        public UnitMasterRepository()
        {
            entity = new GSDBSExtended();
           
        }
        public Models.UnitMaster Get(long ID)
        {
            return entity.tbl_UnitMaster.Where(us => us.Id == ID).Select(us => new UnitMaster()
            {
                ID = us.Id,
                Type = us.Type,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
            }).FirstOrDefault();
        }
        public List<Models.UnitMaster> Get()
        {
            return entity.tbl_UnitMaster.Select(us => new UnitMaster()
            {
                ID = us.Id,
                Type = us.Type,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,

            }).ToList();
        }
        public bool CreateUnitMaster(Models.UnitMaster unitmaster)
        {
            tbl_UnitMaster unmastr = new tbl_UnitMaster();
            unmastr.Type = unitmaster.Type;
            unmastr.CreatedBy = unitmaster.CreatedBy;
            
            entity.tbl_UnitMaster.Add(unmastr);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveUnitMaster(long ID)
        {
            tbl_UnitMaster unmastr = entity.tbl_UnitMaster.FirstOrDefault(us => us.Id == ID);
            if (unmastr != null)
            {
                entity.tbl_UnitMaster.Remove(unmastr);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateUnitMaster(Models.UnitMaster unitmaster)
        {
            tbl_UnitMaster unmastr = entity.tbl_UnitMaster.FirstOrDefault(us => us.Id == unitmaster.ID);
            if (unmastr != null)
            {
                unmastr.Type = unitmaster.Type;
               // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}