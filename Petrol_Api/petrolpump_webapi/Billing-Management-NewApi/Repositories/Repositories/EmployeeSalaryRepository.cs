﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class EmployeeSalaryRepository : IEmployeeSalaryRepository
    {
        GSDBSExtended entity;
        public EmployeeSalaryRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.EmployeeSalary Get(long ID)
        {
            return entity.tbl_EmployeeSalary.Where(us => us.ID == ID).Select(us => new EmployeeSalary()
            {
                ID = us.ID,
                EmployeeName = us.EmployeeName,
                Dapartment = us.Dapartment,
                AvailableDays = us.AvailableDays,
                PaidDays = us.PaidDays,
                Rate = us.Rate,
                Salary = us.Salary,
                Bonus = us.Bonus,
                Total = us.Total,
                CreatedBy = us.CreatedBy,
                CreatedDate = us.CreatedDate,
                ModifiedBy = us.ModifiedBy,
                ModifiedDate = us.ModifiedDate,
            }).FirstOrDefault();
        }
        public List<Models.EmployeeSalary> Get()
        {
            return (from us in entity.tbl_EmployeeSalary
                    join emp in entity.tbl_EmployeeRegister
                    on us.EmployeeName equals emp.Id
                    join dept in entity.tbl_DepartmentMaster
                    on us.Dapartment equals dept.Id
                    select new EmployeeSalary()
                    {
                ID = us.ID,
                EmployeeName = us.EmployeeName,
                EmployeeNameDes=emp.EmployeeName,
                Dapartment = us.Dapartment,
                DepartmentDes=dept.Type,
                AvailableDays = us.AvailableDays,
                PaidDays = us.PaidDays,
                Rate = us.Rate,
                Salary = us.Salary,
                Bonus = us.Bonus,
                Total = us.Total,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).ToList();
        }
        public bool CreateEmployeeSalary(Models.EmployeeSalary salary1)
        {
            tbl_EmployeeSalary sal = new tbl_EmployeeSalary();
            sal.EmployeeName = salary1.EmployeeName;
            sal.Dapartment = salary1.Dapartment;
            sal.AvailableDays = salary1.AvailableDays;
            sal.PaidDays = salary1.PaidDays;
            sal.Rate = salary1.Rate;
            sal.Salary = salary1.Salary;
            sal.Bonus = salary1.Bonus;
            sal.Total = salary1.Total;
            sal.CreatedBy = salary1.CreatedBy;
            sal.CreatedDate = salary1.CreatedDate;
            sal.ModifiedBy = salary1.ModifiedBy;
            sal.ModifiedDate = salary1.ModifiedDate;
            entity.tbl_EmployeeSalary.Add(sal);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }


        public bool RemoveEmployeeSalary(long ID)
        {
            tbl_EmployeeSalary sal = entity.tbl_EmployeeSalary.FirstOrDefault(us => us.ID == ID);
            if (sal != null)
            {
                entity.tbl_EmployeeSalary.Remove(sal);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

        public bool UpdateEmployeeSalary(Models.EmployeeSalary salary)
        {
            tbl_EmployeeSalary sal = entity.tbl_EmployeeSalary.FirstOrDefault(us => us.ID == salary.ID);
            if (sal != null)
            {
                sal.EmployeeName = salary.EmployeeName;
                sal.Dapartment = salary.Dapartment;
                sal.AvailableDays = salary.AvailableDays;
                sal.PaidDays = salary.PaidDays;
                sal.Rate = salary.Rate;
                sal.Salary = salary.Salary;
                sal.Bonus = salary.Bonus;
                sal.Total = salary.Total;
                sal.CreatedBy = salary.CreatedBy;
                sal.CreatedDate = salary.CreatedDate;
                sal.ModifiedBy = salary.ModifiedBy;
                sal.ModifiedDate = salary.ModifiedDate;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}