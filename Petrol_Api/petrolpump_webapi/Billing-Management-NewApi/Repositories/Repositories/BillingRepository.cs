﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class BillingRepository: IBillingRepository
    {
        GSDBSExtended entity;
        public BillingRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.Billing Get(long ID)
        {
            return entity.tbl_Billing.Where(us => us.Id == ID).Select(us => new Billing()
            {
                ID = us.Id,
                BillNo=us.BillNo,
                Party=us.Party,
                VehicleNo=us.VehicleNo,
                ItemType=us.ItemType,
                Qty=us.Qty,
                Rate=us.Rate,
                Total=us.Total,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                

            }).FirstOrDefault();
        }
        public List<Models.Billing> Get()
        {
            return (from us in entity.tbl_Billing
                    join tank in entity.tbl_TankMaster
                    on us.ItemType equals tank.Id
                    select new Billing()
                    {
                        ID = us.Id,
                        BillNo=us.BillNo,
                        Party=us.Party,
                        VehicleNo=us.VehicleNo,
                        ItemType=us.ItemType,
                        ItemTypeDes=tank.Type,
                        Rate=us.Rate,
                        Qty=us.Qty,
                        Total=us.Total,
                        //EmployeeName = us.EmployeeName,
                        //EmployeeNameDes = emp.EmployeeName,
                        //StartTime = us.StartTime,
                        //EndTime = us.EndTime,
                        CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,

                    }).ToList();
        }

        public bool CreateBilling(Models.Billing billing)
        {
            tbl_Billing bill = new tbl_Billing();
            bill.BillNo = billing.BillNo;
            bill.Party = billing.Party;
            bill.VehicleNo = billing.VehicleNo;
            bill.ItemType = billing.ItemType;
            bill.Qty = billing.Qty;
            bill.Rate = billing.Rate;
            bill.Total = billing.Total;
            bill.CreatedBy = billing.CreatedBy;
            bill.CreatedDate = billing.CreatedDate;

            entity.tbl_Billing.Add(bill);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveBilling(long ID)
        {
            tbl_Billing bill = entity.tbl_Billing.FirstOrDefault(us => us.Id == ID);
            if (bill != null)
            {
                entity.tbl_Billing.Remove(bill);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateBilling(Models.Billing billing)
        {
            tbl_Billing bill = entity.tbl_Billing.FirstOrDefault(us => us.Id == billing.ID);
            if (bill != null)
            {
                bill.BillNo = billing.BillNo;
                bill.Party = billing.Party;
                bill.VehicleNo = billing.VehicleNo;
                bill.ItemType = billing.ItemType;
                bill.Qty = billing.Qty;
                bill.Rate = billing.Rate;
                bill.Total = billing.Total;
                bill.ModifiedBy = billing.ModifiedBy;
                bill.ModifiedDate = billing.ModifiedDate;

                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}