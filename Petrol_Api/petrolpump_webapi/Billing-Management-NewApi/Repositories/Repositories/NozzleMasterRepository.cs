using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class NozzleMasterRepository:INozzleMasterRepository
    {
        GSDBSExtended entity;
        public NozzleMasterRepository()
        {
            entity = new GSDBSExtended();
        }

        public Models.NozzleMaster GetType(long ID)
        {

            return entity.tbl_NozzleMaster.Where(us => us.Id == ID).Select(us => new NozzleMaster()
            {
                ID = us.Id,
                Type = us.Type,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
        public Models.NozzleMaster Get(long ID)
        {
            return entity.tbl_NozzleMaster.Where(us => us.Id == ID).Select(us => new NozzleMaster()
            {
                ID = us.Id,
                Type = us.Type,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
        public List<Models.NozzleMaster> Get()
        {
            return entity.tbl_NozzleMaster.Select(us => new NozzleMaster()
            {
                ID = us.Id,
                Type = us.Type,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,

            }).ToList();
        }
     
        public bool RemoveNozzleMaster(long ID)
        {
            tbl_NozzleMaster nmastr = entity.tbl_NozzleMaster.FirstOrDefault(us => us.Id == ID);
            if (nmastr != null)
            {
                entity.tbl_NozzleMaster.Remove(nmastr);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool CreateNozzleMaster(Models.NozzleMaster nozzlemaster)
        {
            tbl_NozzleMaster nmastr = new tbl_NozzleMaster();
            nmastr.Type = nozzlemaster.Type;
            nmastr.CreatedDate = DateTime.Now;

            entity.tbl_NozzleMaster.Add(nmastr);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool UpdateNozzleMaster(Models.NozzleMaster nozzlemaster)
        {
            tbl_NozzleMaster nmastr = entity.tbl_NozzleMaster.FirstOrDefault(us => us.Id == nozzlemaster.ID);
            if (nmastr != null)
            {
                nmastr.Type = nozzlemaster.Type;
                nmastr.ModifiedDate = DateTime.Now;
               // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
    }
