﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class BankAccMasterRepository : IBankAccMasterRepository
    {
        GSDBSExtended entity;
        public BankAccMasterRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.BankAccMaster Get(long ID)
        {
            return entity.tbl_BankAccMaster.Where(us => us.Id == ID).Select(us => new BankAccMaster()
            {
                ID = us.Id,
                BankName = us.BankName,
                AccHolderName = us.AccHolderName,
                AccountNo = us.AccountNo,
                Branch = us.Branch,
                IFSCCode = us.IFSCCode,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
        public List<Models.BankAccMaster> Get()
        {
            return entity.tbl_BankAccMaster.Select(us => new BankAccMaster()
            {
                ID = us.Id,
                BankName = us.BankName,
                AccHolderName = us.AccHolderName,
                AccountNo = us.AccountNo,
                Branch = us.Branch,
                IFSCCode = us.IFSCCode,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).ToList();
        }
        public bool CreateBankAccount(Models.BankAccMaster bankAccMaster)
        {
            tbl_BankAccMaster bank = new tbl_BankAccMaster();
            bank.BankName = bankAccMaster.BankName;
            bank.AccHolderName = bankAccMaster.AccHolderName;
            bank.AccountNo = bankAccMaster.AccountNo;
            bank.Branch = bankAccMaster.Branch;
            bank.IFSCCode = bankAccMaster.IFSCCode;
            bank.CreatedBy = bankAccMaster.CreatedBy;
            bank.CreatedDate = bankAccMaster.CreatedDate;
            bank.ModifiedBy = bankAccMaster.ModifiedBy;
            bank.ModifiedDate = bankAccMaster.ModifiedDate;
            entity.tbl_BankAccMaster.Add(bank);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveBankAccount(long ID)
        {
            tbl_BankAccMaster bank = entity.tbl_BankAccMaster.FirstOrDefault(us => us.Id == ID);
            if (bank != null)
            {
                entity.tbl_BankAccMaster.Remove(bank);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateBankAccount(Models.BankAccMaster bank)
        {
            tbl_BankAccMaster amastr = entity.tbl_BankAccMaster.FirstOrDefault(us => us.Id == bank.ID);
            if (amastr != null)
            {
                amastr.BankName = bank.BankName;
                amastr.AccHolderName = bank.AccHolderName;
                amastr.AccountNo = bank.AccountNo;
                amastr.Branch = bank.Branch;
                amastr.IFSCCode = bank.IFSCCode;
                amastr.CreatedBy = bank.CreatedBy;
                amastr.CreatedDate = bank.CreatedDate;
                amastr.ModifiedBy = bank.ModifiedBy;
                amastr.ModifiedDate = bank.ModifiedDate;
                // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}