﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class CustomerMasterRepository: ICustomerMasterRepository
    {
        GSDBSExtended entity;
        public CustomerMasterRepository()
        {
            entity = new GSDBSExtended();

        }
        public Models.CustomerMaster Get(long ID)
        {
            return entity.tbl_CustomerMaster.Where(us => us.Id == ID).Select(us => new CustomerMaster()
            {
                ID = us.Id,
                CustomerName = us.CustomerName,
                Email = us.Email,
                Address = us.Address,
                MobileNumber1 = us.MobileNumber1,
                MobileNumber2 = us.MobileNumber2,
                City = us.City,
                ZipCode = us.ZipCode,
                CreateBy = us.CreateBy.HasValue ? us.CreateBy.Value : 0,
                CreateDate = us.CreateDate.HasValue ? us.CreateDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
        public List<Models.CustomerMaster> Get()
        {
            return entity.tbl_CustomerMaster.Select(us => new CustomerMaster()
            {
                ID = us.Id,
                CustomerName = us.CustomerName,
                Email = us.Email,
                MobileNumber1 = us.MobileNumber1,
                MobileNumber2 = us.MobileNumber2,
                Address = us.Address,
                City = us.City,
                ZipCode = us.ZipCode,
                CreateBy = us.CreateBy.HasValue ? us.CreateBy.Value : 0,
                CreateDate = us.CreateDate.HasValue ? us.CreateDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).ToList();
        }

        public bool CreateCustomerMaster(Models.CustomerMaster customermaster)
        {
            tbl_CustomerMaster cust = new tbl_CustomerMaster();
            cust.CustomerName = customermaster.CustomerName;
            cust.Email = customermaster.Email;
            cust.Address = customermaster.Address;
            cust.MobileNumber1 = customermaster.MobileNumber1;
            cust.MobileNumber2 = customermaster.MobileNumber2;
            cust.City = customermaster.City;
            cust.ZipCode = customermaster.ZipCode;
            cust.CreateBy = customermaster.CreateBy;
            cust.CreateDate = DateTime.Now;

            entity.tbl_CustomerMaster.Add(cust);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveCustomerMaster(long ID)
        {
            tbl_CustomerMaster cust = entity.tbl_CustomerMaster.FirstOrDefault(us => us.Id == ID);
            if (cust != null)
            {
                entity.tbl_CustomerMaster.Remove(cust);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateCustomerMaster(Models.CustomerMaster customermaster)
        {
            tbl_CustomerMaster cust = entity.tbl_CustomerMaster.FirstOrDefault(us => us.Id == customermaster.ID);
            if (cust != null)
            {
                cust.CustomerName = customermaster.CustomerName;
                cust.Email = customermaster.Email;
                cust.Address = customermaster.Address;
                cust.MobileNumber1 = customermaster.MobileNumber1;
                cust.MobileNumber2 = customermaster.MobileNumber2;
                cust.City = customermaster.City;
                cust.ZipCode = customermaster.ZipCode;
                cust.ModifiedBy = customermaster.ModifiedBy;
                cust.ModifiedDate = customermaster.ModifiedDate;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

    }
}