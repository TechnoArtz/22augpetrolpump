﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class DipHSDRepository : IDipHSDRepository
    {
        GSDBSExtended entity;
        public DipHSDRepository()
        {
            entity = new GSDBSExtended();
        }
        public bool CreateDipHSD(Models.DipHSD diphsd)
        {
            tbl_DipHSD diph = new tbl_DipHSD();
            diph.Date = diphsd.Date.HasValue ? diph.Date.Value : DateTime.Now;
            diph.CurrentDip = diphsd.CurrentDip;
            diph.ActualDip = diphsd.ActualDip;
            diph.Variation = diphsd.Variation;
          //  diph.CreatedBy = diphsd.CreatedBy;
            //nmastr.Type = nozzlemaster.Type;
            diph.CreatedDate = DateTime.Now;

            entity.tbl_DipHSD.Add(diph);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public Models.DipHSD Get(long ID)
        {
            return entity.tbl_DipHSD.Where(us => us.Id == ID).Select(us => new DipHSD()
            {
                ID = us.Id,
                Date=us.Date,
                CurrentDip = us.CurrentDip,
                ActualDip=us.ActualDip,
                Variation=us.Variation,
               
            }).FirstOrDefault();
        }
        public List<Models.DipHSD> Get()
        {
            return entity.tbl_DipHSD.Select(us => new DipHSD()
            {
                ID = us.Id,
                Date=us.Date,
                CurrentDip = us.CurrentDip,
                ActualDip = us.ActualDip,
                Variation = us.Variation,

            }).ToList();
        }


        public bool RemoveDipHSD(long ID)
        {
            tbl_DipHSD diph = entity.tbl_DipHSD.FirstOrDefault(us => us.Id == ID);
            if (diph != null)
            {
                entity.tbl_DipHSD.Remove(diph);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateDipHSD(Models.DipHSD diphsd)
        {
            tbl_DipHSD diph = entity.tbl_DipHSD.FirstOrDefault(us => us.Id == diphsd.ID);
            if (diph != null)
            {
                diph.Date = diphsd.Date;
                diph.CurrentDip = diphsd.CurrentDip;
                diph.ActualDip = diphsd.ActualDip;
                diph.Variation = diphsd.Variation;
              //  diph.ModifiedBy = diphsd.ModifiedBy;
                diph.ModifiedDate = DateTime.Now;
                
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

  
    }
}