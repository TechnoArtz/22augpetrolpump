﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class SupplierMasterRepository : ISupplierMasterRepository
    {
        GSDBSExtended entity;
        public SupplierMasterRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.SupplierMaster Get(long ID)
        {
            return entity.tbl_SupplierMaster.Where(us => us.Id == ID).Select(us => new SupplierMaster()
            {
                ID= us.Id,
                Name = us.Name,
                CompanyName = us.CompanyName,
                Email = us.Email,
                MobileNo1 = us.MobileNo1,
                MobileNo2 = us.MobileNo2,
                Address = us.Address,
                City = us.City,
                ZipCode = us.ZipCode,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }

        public List<Models.SupplierMaster> Get()
        {
            return entity.tbl_SupplierMaster.Select(us => new SupplierMaster()
            {
                ID = us.Id,
                Name = us.Name,
                CompanyName = us.CompanyName,
                Email = us.Email,
                MobileNo1 = us.MobileNo1,
                MobileNo2 = us.MobileNo2,
                Address = us.Address,
                City = us.City,
                ZipCode = us.ZipCode,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).ToList();
        }

        public bool CreateSupplier(Models.SupplierMaster supplier)
        {
            tbl_SupplierMaster sup = new tbl_SupplierMaster();
            sup.Name = supplier.Name;
            sup.CompanyName = supplier.CompanyName;
            sup.Email = supplier.Email;
            sup.MobileNo1 = supplier.MobileNo1;
            sup.MobileNo2 = supplier.MobileNo2;
            sup.Address = supplier.Address;
            sup.City = supplier.City;
            sup.ZipCode = supplier.ZipCode;
            sup.CreatedBy = supplier.CreatedBy;
            sup.CreatedDate = DateTime.Now;
            sup.ModifiedBy = supplier.ModifiedBy;
            sup.ModifiedDate = supplier.ModifiedDate;
            entity.tbl_SupplierMaster.Add(sup);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveSupplier(long ID)
        {
            tbl_SupplierMaster sup = entity.tbl_SupplierMaster.FirstOrDefault(us => us.Id == ID);
            if (sup != null)
            {
                entity.tbl_SupplierMaster.Remove(sup);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

        public bool UpdateSupplier(Models.SupplierMaster supplier)
        {
            tbl_SupplierMaster sup = entity.tbl_SupplierMaster.FirstOrDefault(us => us.Id == supplier.ID);
            if (sup != null)
            {
                sup.Name = supplier.Name;
                sup.CompanyName = supplier.CompanyName;
                sup.Email = supplier.Email;
                sup.MobileNo1 = supplier.MobileNo1;
                sup.MobileNo2 = supplier.MobileNo2;
                sup.Address = supplier.Address;
                sup.City = supplier.City;
                sup.ZipCode = supplier.ZipCode;
                sup.CreatedBy = supplier.CreatedBy;
                sup.CreatedDate = supplier.CreatedDate;
                sup.ModifiedBy = supplier.ModifiedBy;
                sup.ModifiedDate = supplier.ModifiedDate;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

    }
}