﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class CreditorMasterRespository : ICreditorMasterRepository
    {
        GSDBSExtended entity;
        public CreditorMasterRespository()
        {
            entity = new GSDBSExtended();
        }
        public Models.CreditorMaster Get(long ID)
        {
            return entity.tbl_CreditorMaster.Where(us => us.Id == ID).Select(us => new CreditorMaster()
            {
                Id = us.Id,
                CreditorName = us.CreditorName,
                EmailId = us.EmailId,
                MobileNo1 = us.MobileNo1,
                MobileNo2 = us.MobileNo2,
                Address = us.Address,
                City = us.City,
                ZipCode = us.ZipCode,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,

            }).FirstOrDefault();
        }
        public List<Models.CreditorMaster> Get()
        {
            return entity.tbl_CreditorMaster.Select(us => new CreditorMaster()
            {
                Id = us.Id,
                CreditorName = us.CreditorName,
                EmailId = us.EmailId,
                MobileNo1 = us.MobileNo1,
                MobileNo2 = us.MobileNo2,
                Address = us.Address,
                City = us.City,
                ZipCode = us.ZipCode,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,


            }).ToList();
        }
        public bool CreateCreditorMaster(Models.CreditorMaster creditor)
        {
            tbl_CreditorMaster objcreditor = new tbl_CreditorMaster();
            objcreditor.CreditorName = creditor.CreditorName;
            objcreditor.EmailId = creditor.EmailId;
            objcreditor.MobileNo1 = creditor.MobileNo1;
            objcreditor.MobileNo2 = creditor.MobileNo2;
            objcreditor.Address = creditor.Address;
            objcreditor.City = creditor.City;
            objcreditor.ZipCode = creditor.ZipCode;
            objcreditor.CreatedBy = creditor.CreatedBy;
            objcreditor.CreatedDate = creditor.CreatedDate;
            objcreditor.ModifiedBy = creditor.ModifiedBy;
            objcreditor.ModifiedDate = creditor.ModifiedDate;


            entity.tbl_CreditorMaster.Add(objcreditor);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveCreditorMaster(long ID)
        {
            tbl_CreditorMaster objcreditor = entity.tbl_CreditorMaster.FirstOrDefault(us => us.Id == ID);
            if (objcreditor != null)
            {
                entity.tbl_CreditorMaster.Remove(objcreditor);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

        public bool UpdateCreditorMaster(Models.CreditorMaster creditor)
        {
            tbl_CreditorMaster objcreditor = entity.tbl_CreditorMaster.FirstOrDefault(us => us.Id == creditor.Id);
            if (objcreditor != null)
            {
                objcreditor.CreditorName = creditor.CreditorName;
                objcreditor.EmailId = creditor.EmailId;
                objcreditor.MobileNo1 = creditor.MobileNo1;
                objcreditor.MobileNo2 = creditor.MobileNo2;
                objcreditor.Address = creditor.Address;
                objcreditor.City = creditor.City;
                objcreditor.ZipCode = creditor.ZipCode;
                objcreditor.CreatedBy = creditor.CreatedBy;
                objcreditor.CreatedDate = creditor.CreatedDate;
                objcreditor.ModifiedBy = creditor.ModifiedBy;
                objcreditor.ModifiedDate = creditor.ModifiedDate;

                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}