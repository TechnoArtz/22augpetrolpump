using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class UserMasterRepository : IUserMasterRepository
    {
        GSDBSExtended entity;
        public UserMasterRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.UserMaster Get(long ID)
        {
            return entity.tbl_UserMaster.Where(us => us.UID == ID).Select(us => new UserMaster()
            {
                UID = us.UID,
                UserName = us.UserName,
                Password = us.Password,
                ConfirmPassword = us.ConfirmPassword,
                UserRole = us.UserRole,
                EmailID = us.EmailID,
                MobileNo = us.MobileNo,
               
          }).FirstOrDefault();
        }
        public List<Models.UserMaster> Get()
        {
            return entity.tbl_UserMaster.Select(us => new UserMaster()
            {
                UID = us.UID,
                UserName = us.UserName,
                Password = us.Password,
                ConfirmPassword = us.ConfirmPassword,
                UserRole = us.UserRole,
                EmailID = us.EmailID,
                MobileNo = us.MobileNo,

            }).ToList();
        }
        public bool CreateUserMaster(Models.UserMaster usermaster)
        {
            tbl_UserMaster objusermaster = new tbl_UserMaster();
            objusermaster.UserName = usermaster.UserName;
            objusermaster.Password = usermaster.Password;
            objusermaster.ConfirmPassword = usermaster.ConfirmPassword;
            objusermaster.UserRole = usermaster.UserRole;
            objusermaster.EmailID = usermaster.EmailID;
            objusermaster.MobileNo = usermaster.MobileNo;

            entity.tbl_UserMaster.Add(objusermaster);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }

        public bool RemoveUserMaster(long ID)
        {
            tbl_UserMaster objusermaster = entity.tbl_UserMaster.FirstOrDefault(us => us.UID == ID);
            if (objusermaster != null)
            {
                entity.tbl_UserMaster.Remove(objusermaster);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateUserMaster(Models.UserMaster usermaster)
        {
            tbl_UserMaster objusermaster = entity.tbl_UserMaster.FirstOrDefault(us => us.UID == usermaster.UID);
            if (objusermaster != null)
            {
                objusermaster.UserName = usermaster.UserName;
                objusermaster.Password = usermaster.Password;
                objusermaster.ConfirmPassword = usermaster.ConfirmPassword;
                objusermaster.UserRole = usermaster.UserRole;
                objusermaster.EmailID = usermaster.EmailID;
                objusermaster.MobileNo = usermaster.MobileNo;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }


    }
}