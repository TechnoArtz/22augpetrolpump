using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class DepartmentMasterRepository:IDepartmentMasterRepository
    {
         GSDBSExtended entity;
         public DepartmentMasterRepository()
        {
            entity = new GSDBSExtended();
           
        }
         public Models.DepartmentMaster Get(long ID)
        {
            return entity.tbl_DepartmentMaster.Where(us => us.Id == ID).Select(us => new DepartmentMaster()
            {
                ID = us.Id,
                Type = us.Type,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy=   us.ModifiedBy.HasValue ? us.ModifiedBy.Value :0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
         public List<Models.DepartmentMaster> Get()
        {
            return entity.tbl_DepartmentMaster.Select(us => new DepartmentMaster()
            {
                ID = us.Id,
                Type = us.Type,
                //CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                //CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                //ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                //ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,

            }).ToList();
        }
         public bool CreateDepartmentMaster(Models.DepartmentMaster departmentmaster)
        {
            tbl_DepartmentMaster depmastr = new tbl_DepartmentMaster();
            depmastr.Type = departmentmaster.Type;
            //depmastr.CreatedBy = departmentmaster.CreatedBy;
            //depmastr.CreatedDate = departmentmaster.CreatedDate;
            //depmastr.ModifiedBy = departmentmaster.ModifiedBy;
            //depmastr.ModifiedDate = departmentmaster.ModifiedDate;
            entity.tbl_DepartmentMaster.Add(depmastr);
            int result = entity.SaveChanges();
            //return emp.Id; if want to return ID
            return result > 0;
        }
        public bool RemoveDepartmentMaster(long ID)
        {
            tbl_DepartmentMaster depmastr = entity.tbl_DepartmentMaster.FirstOrDefault(us => us.Id == ID);
            if (depmastr != null)
            {
                entity.tbl_DepartmentMaster.Remove(depmastr);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateDepartmentMaster(Models.DepartmentMaster departmentmaster)
        {
            tbl_DepartmentMaster depmastr = entity.tbl_DepartmentMaster.FirstOrDefault(us => us.Id == departmentmaster.ID);
            if (depmastr != null)
            {
                depmastr.Type = departmentmaster.Type;
               // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}