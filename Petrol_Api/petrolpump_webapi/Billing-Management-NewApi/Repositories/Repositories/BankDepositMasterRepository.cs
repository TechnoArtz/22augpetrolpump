﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class BankDepositMasterRepository : IBankDepositMasterRepository
    {
        GSDBSExtended entity;
        public BankDepositMasterRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.BankDepositMaster Get(long ID)
        {
            return entity.tbl_BankDepositEntry.Where(us => us.Id == ID).Select(us => new BankDepositMaster()
            {
                ID = us.Id,
                Date = us.Date,
                BankName = us.BankName,
                BankAccNo = us.BankAccNo,
                Amount = us.Amount,
                Note = us.Note,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).FirstOrDefault();
        }
        public List<Models.BankDepositMaster> Get()
        {
            return entity.tbl_BankDepositEntry.Select(us => new BankDepositMaster()
            {
                ID = us.Id,
                Date = us.Date,
                BankName = us.BankName,
                BankAccNo = us.BankAccNo,
                Amount = us.Amount,
                Note = us.Note,
                CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
                CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
                ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            }).ToList();
        }
        public bool CreateBankDeposit(Models.BankDepositMaster bankDeposit)
        {
            tbl_BankDepositEntry bank = new tbl_BankDepositEntry();
            bank.Date = bankDeposit.Date;
            bank.BankName = bankDeposit.BankName;
            bank.BankAccNo = bankDeposit.BankAccNo;
            bank.Amount = bankDeposit.Amount;
            bank.Note = bankDeposit.Note;
            bank.CreatedBy = bankDeposit.CreatedBy;
            bank.CreatedDate = bankDeposit.CreatedDate;
            bank.ModifiedBy = bankDeposit.ModifiedBy;
            bank.ModifiedDate = bankDeposit.ModifiedDate;
            entity.tbl_BankDepositEntry.Add(bank);
            int result = entity.SaveChanges();
            return result > 0;
        }
        public bool RemoveBankDeposit(long ID)
        {
            tbl_BankDepositEntry bank = entity.tbl_BankDepositEntry.FirstOrDefault(us => us.Id == ID);
            if (bank != null)
            {
                entity.tbl_BankDepositEntry.Remove(bank);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
        public bool UpdateBankDeposit(Models.BankDepositMaster bank)
        {
            tbl_BankDepositEntry tblbank = entity.tbl_BankDepositEntry.FirstOrDefault(us => us.Id == bank.ID);
            if (tblbank != null)
            {
                tblbank.Date = bank.Date;
                tblbank.BankName = bank.BankName;
                tblbank.BankAccNo = bank.BankAccNo;
                tblbank.Amount = bank.Amount;
                tblbank.Note = bank.Note;
                tblbank.CreatedBy = bank.CreatedBy;
                tblbank.CreatedDate = bank.CreatedDate;
                tblbank.ModifiedBy = bank.ModifiedBy;
                tblbank.ModifiedDate = bank.ModifiedDate;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}