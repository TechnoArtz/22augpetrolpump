using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Repositories.Repositories
{
    public class CreditorEntryRepository:ICreditorEntryRepository
    {

         GSDBSExtended entity;
        public CreditorEntryRepository()
        {
            entity = new GSDBSExtended();
        }
        public Models.CreditorEntryMaster Get(long ID)
        {
            var q = (from a in entity.tbl_CreditorEntryMaster
                     join details in entity.tbl_CreaditorEntryDetails
                     on a.Id equals details.CreditorEntryMasterId
                     where a.Id == ID
                     select new
                     {
                         a.Id,
                         a.CreditorName,
                         a.Billno,
                         a.BillDate,
                         a.VehicleNo,
          
                         a.tbl_CreaditorEntryDetails
                     }
                ).FirstOrDefault();
            CreditorEntryMaster crm = new CreditorEntryMaster();

            crm.ID = q.Id;
            crm.CreditorName = q.CreditorName;
            crm.Billno = q.Billno;
            crm.BillDate = q.BillDate;
            crm.VehicleNo = q.VehicleNo;
            crm.lstcreditordetails = new List<CreditorEntryDetails>();
            foreach (var us in q.tbl_CreaditorEntryDetails)
            {
                CreditorEntryDetails ced = new CreditorEntryDetails();
                ced.ID = us.Id;
                ced.CreditorName = us.CreditorName;
                ced.Billno = us.Billno;
                ced.BillDate = us.BillDate;
                ced.VehicleNo = us.VehicleNo;
                ced.ItemType = us.ItemType;
                ced.Qty = us.Qty;
                ced.Rate = us.Rate;
                ced.Amount = us.Amount;
                ced.TotalAmount = us.TotalAmount;
                ced.TotalQty = us.TotalQty;
                ced.Unit = us.Unit;

                crm.lstcreditordetails.Add(ced);
                
                    }
            //   allocation.Add(shift);
            return crm;






            //return entity.tbl_CreaditorEntryDetails.Where(us => us.Id == ID).Select(us => new CreditorEntryDetails()
            //{
            //    ID = us.Id,
            //    CreditorEntryMasterId= us.CreditorEntryMasterId,
            //    Billno = us.Billno,
            //    ItemType = us.ItemType,
            //    Qty = us.Qty,
            //    Unit = us.Unit,
            //    Rate = us.Rate,
            //    Amount = us.Amount,
            //    TotalQty = us.TotalQty,
            //    TotalAmount = us.TotalAmount,
            //    //CreatedBy = us.CreatedBy.HasValue ? us.CreatedBy.Value : 0,
            //    //CreatedDate = us.CreatedDate.HasValue ? us.CreatedDate.Value : DateTime.Now,
            //    //ModifiedBy=   us.ModifiedBy.HasValue ? us.ModifiedBy.Value :0,
            //    //ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,
            //}).FirstOrDefault();
        }

        public List<Models.CreditorEntryDetails> Get()
        {
            return (from us in entity.tbl_CreaditorEntryDetails
                    join obj in entity.tbl_CreditorEntryMaster
                    on us.CreditorEntryMasterId equals obj.Id
                    //join addcrd in entity.tbl_AddCreditor
                    //on us.CreditorName equals addcrd.Id
                    select new CreditorEntryDetails()
            //return entity.tbl_CreaditorEntryDetails.Select(us => new CreditorEntryDetails()
            {
                ID = us.Id,
                CreditorEntryMasterId = us.CreditorEntryMasterId,
                CreditorName=us.CreditorName,
                //CreditorNameDes= addcrd.CreditorName,
                Billno = obj.Billno,
                BillDate=obj.BillDate,
                VehicleNo=obj.VehicleNo,
                ItemType = us.ItemType,
                Qty = us.Qty,
                Unit = us.Unit,
                Rate = us.Rate,
                Amount = us.Amount,
                TotalQty = us.TotalQty,
                TotalAmount = us.TotalAmount,

                //ModifiedBy = us.ModifiedBy.HasValue ? us.ModifiedBy.Value : 0,
                //ModifiedDate = us.ModifiedDate.HasValue ? us.ModifiedDate.Value : DateTime.Now,

            }).ToList();
        }

        public bool Createcreditorentry(Models.CreditorEntryMaster creditorentrymaster)
        {
            tbl_CreditorEntryMaster tblCreditorEntryMaster = new tbl_CreditorEntryMaster(); //entity.tbl_ShiftAllocation.FirstOrDefault(us => us. == id);
            tblCreditorEntryMaster.CreditorName = creditorentrymaster.CreditorName;
            tblCreditorEntryMaster.Billno = creditorentrymaster.Billno;
            tblCreditorEntryMaster.BillDate = creditorentrymaster.BillDate.HasValue ? creditorentrymaster.BillDate.Value : DateTime.Now;
            tblCreditorEntryMaster.VehicleNo = creditorentrymaster.VehicleNo;
           // tblCreditorEntryMaster.Employee_ID = creditorentrymaster.Employee_ID;
            entity.tbl_CreditorEntryMaster.Add(tblCreditorEntryMaster);

            foreach (var item in creditorentrymaster.lstcreditordetails)
            {
                tbl_CreaditorEntryDetails details = new tbl_CreaditorEntryDetails();
                //details.CreditorName = item.CreditorName;
                //details.Billno = item.Billno;
                //details.BillDate = item.BillDate;
               // details.VehicleNo = item.VehicleNo;
                details.ItemType= item.ItemType;
                details.Qty = item.Qty;
                details.Unit = item.Unit;
                details.Rate = item.Rate;
                details.Amount = item.Amount;
                details.TotalQty = item.TotalQty;
                details.TotalAmount = item.TotalAmount;
                details.CreatedDate = item.CreatedDate;
                details.CreditorEntryMasterId = tblCreditorEntryMaster.Id;
                tblCreditorEntryMaster.tbl_CreaditorEntryDetails.Add(details);
                //entity.tbl_CreditorEntryMaster.Add(tblCreditorEntryMaster);
                // int result11 = entity.SaveChanges();
            }


            int result1 = entity.SaveChanges();
            //return tblShiftAllocation.Id; //if want to return ID
            return result1 > 0;
            //throw new NotImplementedException();
        }

        public bool UpdateCreditorEntry(Models.CreditorEntryMaster creditorentrymaster)
        {
            tbl_CreditorEntryMaster tblCreditorEntryMaster = entity.tbl_CreditorEntryMaster.FirstOrDefault(us => us.Id == creditorentrymaster.ID);
            if (tblCreditorEntryMaster != null)
            {
                tblCreditorEntryMaster.CreditorName = creditorentrymaster.CreditorName;
                tblCreditorEntryMaster.Billno = creditorentrymaster.Billno;
                tblCreditorEntryMaster.BillDate = creditorentrymaster.BillDate.HasValue ? creditorentrymaster.BillDate.Value : DateTime.Now;
                tblCreditorEntryMaster.VehicleNo = creditorentrymaster.VehicleNo;
                // tblShiftAllocation.Shift = Shiftallocation.Shift;
                foreach (var item in creditorentrymaster.lstcreditordetails)
                {
                    //tbl_CreaditorEntryDetails details = new tbl_CreaditorEntryDetails();
                    tbl_CreaditorEntryDetails details = entity.tbl_CreaditorEntryDetails.FirstOrDefault(us => us.CreditorEntryMasterId == creditorentrymaster.ID && us.Id == item.ID);
                    if (item.ID <= 0)
                        details = new tbl_CreaditorEntryDetails();
                    details.Billno = item.Billno;
                    details.ItemType = item.ItemType;
                    details.Qty = item.Qty;
                    details.Unit = item.Unit;
                    details.Rate = item.Rate;
                    details.Amount = item.Amount;
                    details.TotalQty = item.TotalQty;
                    details.TotalAmount = item.TotalAmount;
                    details.CreatedDate = item.CreatedDate;
                    details.CreditorEntryMasterId = tblCreditorEntryMaster.Id;
                    //       if(item.ID<=0)
                    tblCreditorEntryMaster.tbl_CreaditorEntryDetails.Add(details);
                }


                //entity.tbl_ShiftAllocation.Add(tblShiftAllocation);
                // unmastr.CreatedBy = unitmaster.CreatedBy;
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }

        public bool RemoveCreditorEntry(long ID)
        {

            tbl_CreaditorEntryDetails credit = entity.tbl_CreaditorEntryDetails.FirstOrDefault(us => us.CreditorEntryMasterId == ID);
            if (credit != null)
            {
                entity.tbl_CreaditorEntryDetails.Remove(credit);
                int result = entity.SaveChanges();
                return result > 0;
            }
            return false;
        }
    }
}