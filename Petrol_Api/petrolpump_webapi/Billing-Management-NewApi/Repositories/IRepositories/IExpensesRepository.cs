﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IExpensesRepository
    {
        /// <summary>
        /// get type from Expenses
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        Expenses Get(long ID);
        /// <summary>
        /// Get list of Expense
        /// </summary>
        /// <returns></returns>
        List<Expenses> Get();
        /// <summary>
        /// Add new Expenses in system
        /// </summary>
        /// <param name="expenses"></param>
        /// <returns></returns>
        bool CreateExpenses(Expenses expenses);
        /// <summary>
        /// Remove Expenses in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveExpenses(long ID);
        /// <summary>
        /// Update Expenses information
        /// </summary>
        /// <param name="expenses"></param>
        /// <returns></returns>
        bool UpdateExpenses(Expenses expenses);
    }
}
