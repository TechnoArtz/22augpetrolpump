using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IReceiveItemMasterRepository
    {
        /// <summary>
        /// get type from ReceiveItemMaster
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        ReceiveItemMaster Get(long ID);
        /// <summary>
        /// Get list of ReceiveItemMaster
        /// </summary>
        /// <returns></returns>
        List<ReceiveItemMaster> Get();
        /// <summary>
        /// Add new ReceiveItemMaster in system
        /// </summary>
        /// <param name="ReceiveItemMaster"></param>
        /// <returns></returns>
        bool CreateReceiveItemMaster(ReceiveItemMaster receiveitem);
        /// <summary>
        /// Remove ReceiveItemMaster in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveReceiveItemMaster(long ID);
        /// <summary>
        /// Update ReceiveItemMaster information
        /// </summary>
        /// <param name="ReceiveItemMaster"></param>
        /// <returns></returns>
        bool UpdateReceiveItemMaster(ReceiveItemMaster receiveItem);

    }
}
