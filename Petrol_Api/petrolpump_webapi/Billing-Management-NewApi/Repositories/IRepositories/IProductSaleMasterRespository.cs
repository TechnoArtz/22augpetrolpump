using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IProductSaleMasterRespository
    {
        ///// <summary>
        ///// Get ProductSaleMaster Information 
        ///// </summary>
        ///// <param name="ID"></param>
        ///// <returns></returns>
        //ProductSaleMaster Get(long ID);
        ///// <summary>
        ///// Get list of ProductSaleMaster
        ///// </summary>
        ///// <returns></returns>
        //List<ProductSaleMaster> Get();
        ///// <summary>
        ///// Add new ProductSaleMaster in system
        ///// </summary>
        ///// <param name="productSaleMaster"></param>
        ///// <returns></returns>
        //bool CreateProductSaleMaster(ProductSaleMaster productsalemaster);
        ///// <summary>
        ///// Remove ProductSaleMaster in system
        ///// </summary>
        ///// <param name="ID"></param>
        ///// <returns></returns>
        //bool RemoveProductSaleMaster(long ID);
        ///// <summary>
        ///// Update ProductSaleMaster information
        ///// </summary>
        ///// <param name="productSaleMaster"></param>
        ///// <returns></returns>
        //bool UpdateProductSaleMaster(ProductSaleMaster productsalemaster);
        ProductSaleMaster Get(long ID);
        List<ProductSaleDetails> Get();
        bool CreateProductSale(ProductSaleMaster productsalemaster);
        bool UpdateProductSale(ProductSaleMaster productsalemaster);
        bool RemoveProductSale(long ID);
    }
}
