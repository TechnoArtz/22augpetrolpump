﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
     public interface IDipMSRepository
    {
        /// <summary>
        /// get type from DipMS
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        DipMS Get(long ID);
        /// <summary>
        /// Get list of DipMS
        /// </summary>
        /// <returns></returns>
        List<DipMS> Get();
        /// <summary>
        /// Add new DipMS in system
        /// </summary>
        /// <param name="dipms"></param>
        /// <returns></returns>
        bool CreateDipMS(DipMS dipms);
        /// <summary>
        /// Remove DipMS in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveDipMS(long ID);
        /// <summary>
        /// Update DipMS information
        /// </summary>
        /// <param name="dipms"></param>
        /// <returns></returns>
        bool UpdateDipMS(DipMS dipms);
    }
}
