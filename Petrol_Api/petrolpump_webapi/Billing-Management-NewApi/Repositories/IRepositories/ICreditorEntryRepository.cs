using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
   public interface ICreditorEntryRepository
    {
        /// <summary>
        /// get record by id
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
       CreditorEntryMaster Get(long ID);
        /// <summary>
        /// get list of creditorentry
        /// </summary>
        /// <returns></returns>
       List<CreditorEntryDetails> Get();
        /// <summary>
        /// save creditorentrymaster
        /// </summary>
        /// <param name="creditorentrymaster"></param>
        /// <returns></returns>
       bool Createcreditorentry(CreditorEntryMaster creditorentrymaster);
       /// <summary>
       /// Update   creditorentrymaster information
       /// </summary>
       /// <param name="nozzlemaster"></param>
       /// <returns></returns>
     //  bool AddShiftDetails(ShiftAllocationDetails Shiftallocationdetails);
       bool UpdateCreditorEntry(CreditorEntryMaster creditorentrymaster);
        bool RemoveCreditorEntry(long ID);

    }


    
}
