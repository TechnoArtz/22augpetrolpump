using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    interface IAddCreditorRepository
    {
        /// <summary>
        /// get list of AddCreditor info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        AddCreditor Get(long id);

        /// <summary>
        /// get list AddCreditors
        /// </summary>
        /// <returns></returns>
        List<AddCreditor> Get();
        /// <summary>
        /// Save Creditor 
        /// </summary>
        /// <param name="addcreditor"></param>
        /// <returns></returns>
      bool  CreateAddCreditor(AddCreditor addcreditor);
        /// <summary>
        /// update creditor
        /// </summary>
        /// <param name="addcreditor"></param>
        /// <returns></returns>
      bool UpdateAddCreditor(AddCreditor addcreditor);
        /// <summary>
        /// Delete creditor record by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
      bool RemoveAddCreditor(long id);
    }
}
