﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface ICreditorMasterRepository
    {
        /// <summary>
        /// Get Creditor Information 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        CreditorMaster Get(long ID);
        /// <summary>
        /// Get list of Creditor
        /// </summary>
        /// <returns></returns>
        List<CreditorMaster> Get();
        /// <summary>
        /// Add new Creditor in system
        /// </summary>
        /// <param name="creditor"></param>
        /// <returns></returns>
        bool CreateCreditorMaster(CreditorMaster creditor);
        /// <summary>
        /// Remove Creditor in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveCreditorMaster(long ID);
        /// <summary>
        /// Update creditor information
        /// </summary>
        /// <param name="creditor"></param>
        /// <returns></returns>
        bool UpdateCreditorMaster(CreditorMaster creditor);
    }
}
