﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IShiftMasterRepository
    {
        /// <summary>
        /// Get ShiftMaster Information 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        ShiftMaster Get(long ID);
        /// <summary>
        /// Get list of ShiftMaster
        /// </summary>
        /// <returns></returns>
        List<ShiftMaster> Get();
        /// <summary>
        /// Add new ShiftMaster in system
        /// </summary>
        /// <param name="empoyee"></param>
        /// <returns></returns>
        bool CreateShiftMaster(ShiftMaster shiftmaster);
        /// <summary>
        /// Remove ShiftMaster in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveShiftMaster(long ID);
        /// <summary>
        /// Update ShiftMaster information
        /// </summary>
        /// <param name="nozzlemaster"></param>
        /// <returns></returns>
        bool UpdateShiftMaster(ShiftMaster shiftmaster);
    }
}
