﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IShiftMasterRespository
    {
        /// <summary>
        /// Get Employee Information 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        ShiftMaster Get(long Id);
        /// <summary>
        /// Get list of Shift
        /// </summary>
        /// <returns></returns>
        List<ShiftMaster> Get();
        /// <summary>
        /// Add new employee in system
        /// </summary>
        /// <param name="empoyee"></param>
        /// <returns></returns>
        bool CreateShift(ShiftMaster shift);
        /// <summary>
        /// Remove shift in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveShift(long ID);
        /// <summary>
        /// Update Shift information
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        bool UpdateShift(ShiftMaster shift);
    }
}
