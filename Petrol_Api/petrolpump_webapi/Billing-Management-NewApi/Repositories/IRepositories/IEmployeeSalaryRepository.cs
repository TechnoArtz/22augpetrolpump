﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories
{
    public interface IEmployeeSalaryRepository
    {
        EmployeeSalary Get(long ID);
        List<EmployeeSalary> Get();
        bool CreateEmployeeSalary(EmployeeSalary salary);
        bool RemoveEmployeeSalary(long ID);
        bool UpdateEmployeeSalary(EmployeeSalary salary);
    }
}
