﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IBankCreditMasterRepository
    {
        BankCreditMaster Get(long ID);

        List<BankCreditMaster> Get();

        bool CreateBankCredit(BankCreditMaster bankCredit);

        bool RemoveBankCredit(long ID);

        bool UpdateBankCredit(BankCreditMaster bankCredit);
    }
}
