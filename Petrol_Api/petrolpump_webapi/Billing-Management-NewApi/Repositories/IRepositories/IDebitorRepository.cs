﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IDebitorRepository
    {
        /// <summary>
        /// get type from Billing
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        Debitor Get(long ID);
        /// <summary>
        /// Get list of Billing
        /// </summary>
        /// <returns></returns>
        List<Debitor> Get();
        /// <summary>
        /// Add new Debitor in system
        /// </summary>
        /// <param name="debitor"></param>
        /// <returns></returns>
        bool CreateDebitor(Debitor debitor);

    }
}
