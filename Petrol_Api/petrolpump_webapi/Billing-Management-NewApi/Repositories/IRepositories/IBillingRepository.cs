﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IBillingRepository
    {
        /// <summary>
        /// get type from Billing
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        Billing Get(long ID);
        /// <summary>
        /// Get list of Billing
        /// </summary>
        /// <returns></returns>
        List<Billing> Get();
        /// <summary>
        /// Add new Billing in system
        /// </summary>
        /// <param name="billing"></param>
        /// <returns></returns>
        bool CreateBilling(Billing billing);
        /// <summary>
        /// Remove Billing in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveBilling(long ID);
        /// <summary>
        /// Update Billing information
        /// </summary>
        /// <param name="billing"></param>
        /// <returns></returns>
        bool UpdateBilling(Billing billing);
    }
}
