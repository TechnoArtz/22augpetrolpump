﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IShiftEntryRepository
    {

        /// <summary>
        /// Get shiftentry Information 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        ShiftEntry Get(long ID);
        /// <summary>
        /// Get list of shiftentry
        /// </summary>
        /// <returns></returns>
        List<ShiftEntry> Get();
        /// <summary>
        /// Add new shiftentry in system
        /// </summary>
        /// <param name="shiftentry"></param>
        /// <returns></returns>
        bool CreateShiftEntry(ShiftEntry shiftentry);
        /// <summary>
        /// Remove shiftentry in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveShiftEntry(long ID);
        /// <summary>
        /// Update shiftentry information
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        bool UpdateShiftEntry(ShiftEntry shiftentry);
    }
}
