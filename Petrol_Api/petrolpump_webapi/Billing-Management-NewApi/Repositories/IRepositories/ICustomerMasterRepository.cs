﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface ICustomerMasterRepository
    {
        /// <summary>
        /// get type from CustomerMaster
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        CustomerMaster Get(long ID);
        /// <summary>
        /// Get list of CustomerMaster
        /// </summary>
        /// <returns></returns>
        List<CustomerMaster> Get();
        /// <summary>
        /// Add new CustomerMaster in system
        /// </summary>
        /// <param name="customermaster"></param>
        /// <returns></returns>
        bool CreateCustomerMaster(CustomerMaster customermaster);
        /// <summary>
        /// Remove CustomerMaster in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveCustomerMaster(long ID);
        /// <summary>
        /// Update CustomerMaster information
        /// </summary>
        /// <param name="customermaster"></param>
        /// <returns></returns>
        bool UpdateCustomerMaster(CustomerMaster customermaster);
    }
}
