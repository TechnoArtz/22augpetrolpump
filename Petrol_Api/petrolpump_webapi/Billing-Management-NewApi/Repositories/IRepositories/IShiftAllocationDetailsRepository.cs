using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
   public interface IShiftAllocationDetailsRepository
    {
       ShiftAllocation Get(long ID);

       List<ShiftAllocationDetails> Get();
        /// <summary>
        /// get list of Tank master
        /// </summary>
        /// <returns></returns>
        List<ShiftAllocationDetails> GetTankMaster();
        /// <summary>
        /// Get List of Nozzel Master
        /// </summary>
        /// <returns></returns>
        List<ShiftAllocationDetails> GetNozzelMaster();
        List<ShiftAllocation> GetEmployeeRegMaster();
        /// <summary>
        /// Get list of ShiftMaster
        /// </summary>
        /// <returns></returns>
        List<ShiftAllocation> GetShiftMaster();
        /// Add new   ShiftAllocation in system
        /// </summary>
        /// <param name="empoyee"></param>
        /// <returns></returns>
        bool CreateShiftAllocationDetails(ShiftAllocation Shiftallocation);
       /// <summary>
       /// Update   ShiftAllocation information
       /// </summary>
       /// <param name="nozzlemaster"></param>
       /// <returns></returns>
       bool AddShiftDetails(ShiftAllocationDetails Shiftallocationdetails);
       bool UpdateShiftAllocationDetails(ShiftAllocation Shiftallocation);
        /// <summary>
        /// Remove   ShiftAllocation in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        //  bool RemoveShiftAllocationDetails(long ID);
        List<ShiftAllocationDetails> Filter(SearchRecord searchRecord);
    }
}
