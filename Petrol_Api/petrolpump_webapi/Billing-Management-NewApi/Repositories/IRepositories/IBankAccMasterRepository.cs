﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IBankAccMasterRepository
    {
        BankAccMaster Get(long ID);
        /// <summary>
        /// Get list of Bank Account
        /// </summary>
        /// <returns></returns>
        List<BankAccMaster> Get();
        /// <summary>
        /// Add new Bank Account in system
        /// </summary>
        /// <param name="BankAccMaster"></param>
        /// <returns></returns>
        bool CreateBankAccount(BankAccMaster bankAcc);
        /// <summary>
        /// Remove Bank Account in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveBankAccount(long ID);
        /// <summary>
        /// Update Bank Account information
        /// </summary>
        /// <param name="BankAccMaster"></param>
        /// <returns></returns>
        bool UpdateBankAccount(BankAccMaster bankAcc);
    }
}
