﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IDipHSDRepository
    {
        /// <summary>
        /// get type from DipHSD
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        DipHSD Get(long ID);
        /// <summary>
        /// Get list of DipHSD
        /// </summary>
        /// <returns></returns>
        List<DipHSD>Get();
        /// <summary>
        /// Add new DipHSD in system
        /// </summary>
        /// <param name="diphsd"></param>
        /// <returns></returns>
        bool CreateDipHSD(DipHSD diphsd);
        /// <summary>
        /// Remove DipHSD in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveDipHSD(long ID);
        /// <summary>
        /// Update DipHSD information
        /// </summary>
        /// <param name="diphsd"></param>
        /// <returns></returns>
        bool UpdateDipHSD(DipHSD diphsd);
        
    }
}
