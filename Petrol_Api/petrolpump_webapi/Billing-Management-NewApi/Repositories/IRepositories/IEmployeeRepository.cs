﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IEmployeeRepository
    {
        List<Employee> GetUserRole();
        /// <summary>
        /// Get Employee Information 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        Employee Get(long ID);
        /// <summary>
        /// Get list of employees
        /// </summary>
        /// <returns></returns>
        List<Employee> Get();
        /// <summary>
        /// Add new employee in system
        /// </summary>
        /// <param name="empoyee"></param>
        /// <returns></returns>
        bool CreateEmployee(Employee employee);
        /// <summary>
        /// Remove employee in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveEmployee(long ID);
        /// <summary>
        /// Update employee information
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        bool UpdateEmployee(Employee employee);
    }
}
