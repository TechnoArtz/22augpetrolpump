﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface ISupplierMaster
    {
        Supplier Get(long ID);
        List<Supplier> Get();
        bool CreateSupplier(Supplier supplier);
        bool RemoveSupplier(long ID);
        bool UpdateSupplier(Supplier supplier);
    }
}
