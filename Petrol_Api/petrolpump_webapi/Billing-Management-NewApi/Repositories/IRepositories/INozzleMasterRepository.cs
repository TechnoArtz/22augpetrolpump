using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    interface INozzleMasterRepository
    {
        /// <summary>
        /// get type from nozzellist
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        NozzleMaster GetType(long ID);
        /// <summary>
        /// Get NozzleMaster Information 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        NozzleMaster Get(long ID);
        /// <summary>
        /// Get list of NozzleMaster
        /// </summary>
        /// <returns></returns>
        List<NozzleMaster> Get();
        /// <summary>
        /// Add new NozzleMaster in system
        /// </summary>
        /// <param name="empoyee"></param>
        /// <returns></returns>
        bool CreateNozzleMaster(NozzleMaster nozzlemaster);
        /// <summary>
        /// Remove NozzleMaster in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveNozzleMaster(long ID);
        /// <summary>
        /// Update NozzleMaster information
        /// </summary>
        /// <param name="nozzlemaster"></param>
        /// <returns></returns>
        bool UpdateNozzleMaster(NozzleMaster nozzlemaster);
    }
}
