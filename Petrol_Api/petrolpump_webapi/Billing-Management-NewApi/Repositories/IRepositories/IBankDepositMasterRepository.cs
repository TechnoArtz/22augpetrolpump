﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IBankDepositMasterRepository
    {

        BankDepositMaster Get(long ID);

        List<BankDepositMaster> Get();

        bool CreateBankDeposit(BankDepositMaster bankDeposit);

        bool RemoveBankDeposit(long ID);

        bool UpdateBankDeposit(BankDepositMaster bankDeposit);
    }
}
