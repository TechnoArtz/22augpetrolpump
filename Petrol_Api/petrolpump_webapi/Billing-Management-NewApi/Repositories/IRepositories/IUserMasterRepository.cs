using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
   public interface IUserMasterRepository
    {
        /// <summary>
        /// Get UserMaster Information 
        /// </summary>
        /// <param name="UID"></param>
        /// <returns></returns>
      UserMaster Get(long ID);
      /// <summary>
      /// Get list of UserMaster
      /// </summary>
      /// <returns></returns>
      List<UserMaster> Get();
      /// <summary>
      /// Add new UserMaster in system
      /// </summary>
      /// <param name="empoyee"></param>
      /// <returns></returns>
      bool CreateUserMaster(UserMaster usermaster);
      /// <summary>
      /// Remove  UserMaster in system
      /// </summary>
      /// <param name="ID"></param>
      /// <returns></returns>
      bool RemoveUserMaster(long ID);
      /// <summary>
      /// Update UserMaster information
      /// </summary>
      /// <param name="employee"></param>
      /// <returns></returns>
      bool UpdateUserMaster(UserMaster usermaster);
    }
}
