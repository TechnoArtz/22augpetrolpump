﻿using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface ISupplierMasterRepository
    {
        /// <summary>
        /// Get SupplierMaster Information 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        SupplierMaster Get(long ID);
        /// <summary>
        /// Get list of SupplierMaster
        /// </summary>
        /// <returns></returns>
        List<SupplierMaster> Get();
         /// <summary>
        /// Add new SupplierMaster in system
        /// </summary>
        /// <param name="empoyee"></param>
        /// <returns></returns>
        bool CreateSupplier(SupplierMaster supplier);
        /// <summary>
        /// Remove SupplierMaster in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveSupplier(long ID);
        /// <summary>
        /// Update SupplierMaster information
        /// </summary>
        /// <param name="nozzlemaster"></param>
        /// <returns></returns>
        bool UpdateSupplier(SupplierMaster supplier);
    }
}
