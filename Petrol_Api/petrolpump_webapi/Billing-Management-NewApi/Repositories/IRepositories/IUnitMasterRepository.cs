using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
    public interface IUnitMasterRepository
    {
        /// <summary>
        /// Get UnitMaster Infomation
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        UnitMaster Get(long ID);
        /// <summary>
        /// Get list of UnitMaster
        /// </summary>
        /// <returns></returns>
        List<UnitMaster> Get();
        /// <summary>
        /// Add New Unit In System
        /// </summary>
        /// <param name="unitmaster"></param>
        /// <returns></returns>
        bool CreateUnitMaster(UnitMaster unitmaster);
        /// <summary>
        /// Remove UnitMaster In System
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool RemoveUnitMaster(long ID);
        /// <summary>
        ///  Update UnitMaster information
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool UpdateUnitMaster(UnitMaster unitmaster);
        
    }
}
