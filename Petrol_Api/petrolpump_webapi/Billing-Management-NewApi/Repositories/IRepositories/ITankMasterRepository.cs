using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
   public interface ITankMasterRepository
    {
        /// <summary>
        /// Get TankMaster Information 
        /// </summary>
        /// <param name="UID"></param>
        /// <returns></returns>
       TankMaster Get(long ID);
        /// <summary>
       /// Get list of TankMaster
        /// </summary>
        /// <returns></returns>
       List<TankMaster> Get();
        /// <summary>
       /// Add new TankMaster in system
        /// </summary>
        /// <param name="empoyee"></param>
        /// <returns></returns>
       bool CreateTankMaster(TankMaster tankmaster);
        /// <summary>
       /// Remove  TankMaster in system
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
       bool RemoveTankMaster(long ID);
        /// <summary>
       /// Update TankMaster information
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
       bool UpdateTankMaster(TankMaster tankmaster);
    }
}
