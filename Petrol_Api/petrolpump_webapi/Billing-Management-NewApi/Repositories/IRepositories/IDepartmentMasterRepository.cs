using Billing_Management_NewApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing_Management_NewApi.Repositories.IRepositories
{
  public interface IDepartmentMasterRepository
    {
       /// <summary>
        /// Get DepartmentMaster Information 
        /// </summary>
        /// <param name="UID"></param>
        /// <returns></returns>
      DepartmentMaster Get(long ID);
      /// <summary>
      /// Get list of DepartmentMaster
      /// </summary>
      /// <returns></returns>
      List<DepartmentMaster> Get();
      /// <summary>
      /// Add new DepartmentMaster in system
      /// </summary>
      /// <param name="empoyee"></param>
      /// <returns></returns>
      bool CreateDepartmentMaster(DepartmentMaster departmentmaster);
      /// <summary>
      /// Remove  DepartmentMaster in system
      /// </summary>
      /// <param name="ID"></param>
      /// <returns></returns>
      bool RemoveDepartmentMaster(long ID);
      /// <summary>
      /// Update DepartmentMaster information
      /// </summary>
      /// <param name="employee"></param>
      /// <returns></returns>
      bool UpdateDepartmentMaster(DepartmentMaster departmentmaster);
    
    }
}
