//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Billing_Management_NewApi
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_EmployeeSalary
    {
        public long ID { get; set; }
        public Nullable<long> EmployeeName { get; set; }
        public Nullable<long> Dapartment { get; set; }
        public Nullable<int> AvailableDays { get; set; }
        public Nullable<int> PaidDays { get; set; }
        public Nullable<double> Rate { get; set; }
        public Nullable<double> Salary { get; set; }
        public Nullable<double> Bonus { get; set; }
        public Nullable<double> Total { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual tbl_DepartmentMaster tbl_DepartmentMaster { get; set; }
        public virtual tbl_EmployeeRegister tbl_EmployeeRegister { get; set; }
    }
}
