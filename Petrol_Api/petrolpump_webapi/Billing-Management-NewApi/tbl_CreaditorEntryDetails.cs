//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Billing_Management_NewApi
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_CreaditorEntryDetails
    {
        public long Id { get; set; }
        public Nullable<long> CreditorEntryMasterId { get; set; }
        public Nullable<long> CreditorName { get; set; }
        public Nullable<long> Billno { get; set; }
        public Nullable<System.DateTime> BillDate { get; set; }
        public string VehicleNo { get; set; }
        public string ItemType { get; set; }
        public Nullable<int> Qty { get; set; }
        public string Unit { get; set; }
        public Nullable<double> Rate { get; set; }
        public Nullable<double> Amount { get; set; }
        public Nullable<double> TotalQty { get; set; }
        public Nullable<double> TotalAmount { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual tbl_AddCreditor tbl_AddCreditor { get; set; }
        public virtual tbl_CreditorEntryMaster tbl_CreditorEntryMaster { get; set; }
    }
}
