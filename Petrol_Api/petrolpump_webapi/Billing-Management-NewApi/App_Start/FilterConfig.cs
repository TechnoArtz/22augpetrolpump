﻿using System.Web;
using System.Web.Mvc;

namespace Billing_Management_NewApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
