﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class BankCreditMaster
    {
        public long ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<long> Shift { get; set; }
        public string ShiftDes { get; set; }
        public string AccountName { get; set; }
        public Nullable<long> EmployeeName { get; set; }
        public string EmployeeNameDes { get; set; }
        public string PaymentMode { get; set; }
        public string BankName { get; set; }
        public Nullable<long> BankAccNo { get; set; }
        public Nullable<long> Amount { get; set; }
        public string Note { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual tbl_EmployeeRegister tbl_EmployeeRegister { get; set; }
        public virtual tbl_ShiftMaster tbl_ShiftMaster { get; set; }
    }
}