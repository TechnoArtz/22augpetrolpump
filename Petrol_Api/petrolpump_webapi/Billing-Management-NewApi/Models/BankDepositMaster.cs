﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class BankDepositMaster
    {

        public long ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string BankName { get; set; }
        public Nullable<long> BankAccNo { get; set; }
        public Nullable<long> Amount { get; set; }
        public string Note { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}