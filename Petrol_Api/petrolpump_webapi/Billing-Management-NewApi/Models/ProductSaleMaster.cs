using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class ProductSaleMaster
    {
        public long ID { get; set; }
        public Nullable<long> PartyName { get; set; }
        public string EmployeeNameDes { get; set; }
        public Nullable<long> ShiftType { get; set; }
        public string ShiftTypeDes { get; set; }
        public Nullable<long> TankType { get; set; }
        public string TankTypeDes { get; set; }
        public Nullable<long> NozzelNo { get; set; }
        public string NozzelNoDes { get; set; }
        public string Notes { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public List<ProductSaleDetails> lstProductsaledetails { get; set; }
    }
}