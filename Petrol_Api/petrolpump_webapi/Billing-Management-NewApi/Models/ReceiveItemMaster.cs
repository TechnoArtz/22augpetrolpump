using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class ReceiveItemMaster
    {
        public long ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public long TankType { get; set; }
        public string TankTypeDes { get; set; }
        public Nullable<double> Qty { get; set; }
        public Nullable<long> Createdby { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<long> Modifiedby { get; set; }
        public Nullable<System.DateTime> Modifieddate { get; set; }

        public virtual tbl_TankMaster tbl_TankMaster { get; set; }
    }
}