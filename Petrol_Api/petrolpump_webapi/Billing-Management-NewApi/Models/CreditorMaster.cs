﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class CreditorMaster
    {
        public long Id { get; set; }

        public string CreditorName { get; set; }

        public string EmailId { get; set; }

        public string MobileNo1 { get; set; }

        public string MobileNo2 { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public Nullable<int> ZipCode { get; set; }

        public Nullable<long> CreatedBy { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

        public Nullable<long> ModifiedBy { get; set; }

        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}