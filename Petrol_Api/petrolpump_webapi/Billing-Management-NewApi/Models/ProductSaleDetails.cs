﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class ProductSaleDetails
    {
        public long ID { get; set; }
        public Nullable<long> ProductSaleMasterID { get; set; }
        public Nullable<long> PartyName { get; set; }
        public Nullable<long> ShiftType { get; set; }
        public Nullable<long> TankType { get; set; }
        public Nullable<long> NozzelNo { get; set; }
        public string ItemType { get; set; }
        public Nullable<int> Qty { get; set; }
        public Nullable<double> Rate { get; set; }
        public Nullable<double> Total { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual tbl_NozzleMaster tbl_NozzleMaster { get; set; }
        public virtual tbl_ShiftMaster tbl_ShiftMaster { get; set; }
        public virtual tbl_TankMaster tbl_TankMaster { get; set; }
        public virtual tbl_ProductSaleMaster tbl_ProductSaleMaster { get; set; }
    }
}