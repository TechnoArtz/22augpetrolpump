﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class Expenses
    {
        public long ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Category { get; set; }
        public string PaymentMode { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<double> Amount { get; set; }
        public string Note { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}