using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class UserMaster
    {
        public long UID { get; set; }
        [Required]
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string UserRole { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
    }
}