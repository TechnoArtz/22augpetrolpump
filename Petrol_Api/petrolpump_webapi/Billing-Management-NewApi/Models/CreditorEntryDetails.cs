using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class CreditorEntryDetails
    {
        public long ID { get; set; }
        public Nullable<long> CreditorEntryMasterId { get; set; }
        public Nullable<long> CreditorName { get; set; }
        public string CreditorNameDes { get; set; }
        public Nullable<long> Billno { get; set; }
        public Nullable<System.DateTime> BillDate { get; set; }
        public string VehicleNo { get; set; }
        public string ItemType { get; set; }
        public Nullable<int> Qty { get; set; }
        public string Unit { get; set; }
        public Nullable<double> Rate { get; set; }
        public Nullable<double> Amount { get; set; }
        public Nullable<double> TotalQty { get; set; }
        public Nullable<double> TotalAmount { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual tbl_AddCreditor tbl_AddCreditor { get; set; }
        public virtual tbl_CreditorEntryMaster tbl_CreditorEntryMaster { get; set; }

       

    }
}