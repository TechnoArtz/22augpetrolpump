﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class EmployeeSalary
    {
        public long ID { get; set; }
        public Nullable<long> EmployeeName { get; set; }
        public string EmployeeNameDes { get; set; }
        public Nullable<long> Dapartment { get; set; }
        public string DepartmentDes { get; set; }
        public Nullable<int> AvailableDays { get; set; }
        public Nullable<int> PaidDays { get; set; }
        public Nullable<double> Rate { get; set; }
        public Nullable<double> Salary { get; set; }
        public Nullable<double> Bonus { get; set; }
        public Nullable<double> Total { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual tbl_DepartmentMaster tbl_DepartmentMaster { get; set; }
        public virtual tbl_EmployeeRegister tbl_EmployeeRegister { get; set; }
    }
}