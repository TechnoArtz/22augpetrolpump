﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class Employee
    {
        public long ID { get; set; }
        public string EmployeeName { get; set; }
        public byte[] Photo { get; set; }
        public Nullable<long> UserRole { get; set; }
        public string UserRoleDes { get; set; }
        public string MobileNo { get; set; }
        public string Gender { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Address { get; set; }
        public Nullable<System.DateTime> JoiningDate { get; set; }
        public Nullable<double> Salary { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual tbl_DepartmentMaster tbl_DepartmentMaster { get; set; }
    }
}