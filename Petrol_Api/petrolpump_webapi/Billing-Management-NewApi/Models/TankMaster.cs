using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class TankMaster
    {
        public long ID { get; set; }
        public string Type { get; set; }
        public Nullable<double> TankRate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<tbl_ShiftAllocationDetails> tbl_ShiftAllocationDetails { get; set; }
    }
}