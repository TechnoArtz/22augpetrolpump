using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Billing_Management_NewApi.Models
{
    public class UnitMaster
    {
        public long ID { get; set; }
       [Required]
        public String Type{ get; set; }
        public long CreatedBy { get; set; }
    }
}