﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class BankAccMaster
    {
        public long ID { get; set; }
        public string BankName { get; set; }
        public string AccHolderName { get; set; }
        public Nullable<long> AccountNo { get; set; }
        public string Branch { get; set; }
        public string IFSCCode { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

    }
}