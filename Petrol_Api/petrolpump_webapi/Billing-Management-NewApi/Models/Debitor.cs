﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class Debitor
    {
        public long ID { get; set; }
        public Nullable<long> Billno { get; set; }
        public Nullable<long> CustomerName { get; set; }
        public string CustomerNameDes { get; set; }
        public Nullable<double> PreviousAmt { get; set; }
        public Nullable<double> PaidAmt { get; set; }
        public Nullable<double> TotalAmt { get; set; }
        public string Status { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual tbl_CustomerMaster tbl_CustomerMaster { get; set; }
    }
}