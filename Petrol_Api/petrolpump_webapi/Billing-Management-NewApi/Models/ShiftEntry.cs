﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class ShiftEntry
    {
        public long ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<long> ShiftType { get; set; }
        public string ShiftTypeDec { get; set; }
        public Nullable<long> EmployeeName { get; set; }
        public string EmployeeNameDes { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual tbl_EmployeeRegister tbl_EmployeeRegister { get; set; }
        public virtual tbl_ShiftMaster tbl_ShiftMaster { get; set; }
    }
}