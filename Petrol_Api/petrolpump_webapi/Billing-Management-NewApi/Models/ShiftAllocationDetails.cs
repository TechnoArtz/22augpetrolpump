using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class ShiftAllocationDetails
    {
        public long ID { get; set; }
        public Nullable<long> ShiftAllocationMasterID { get; set; }
        public Nullable<long> Employee_ID { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeNameDes { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Shift { get; set; }
        public string Note { get; set; }
        public string TankDes { get; set; }
        public string NozzelDes { get; set; }
        public Nullable<long> Tank { get; set; }
        public Nullable<long> Nozzel { get; set; }
        public Nullable<double> Opening { get; set; }
        public Nullable<double> Closing { get; set; }
        public Nullable<double> TotalSale { get; set; }
        public Nullable<double> Rate { get; set; }
        public Nullable<double> TotalAmt { get; set; }
        public Nullable<double> ByCash { get; set; }
        public Nullable<double> CreditCardCash { get; set; }
        public Nullable<double> DebitCardCash { get; set; }
        public Nullable<double> GrandTotalSale { get; set; }
        public Nullable<double> GrandTotalAmount { get; set; }
        public Nullable<double> PumpTesting { get; set; }
        //public List<ShiftAllocationDetails> lstAllotcation { get; set; }
       
        public virtual tbl_ShiftAllocation tbl_ShiftAllocation { get; set; }
        public virtual tbl_NozzleMaster tbl_NozzleMaster { get; set; }
        public virtual tbl_TankMaster tbl_TankMaster { get; set; }
    }
}