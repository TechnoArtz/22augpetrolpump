using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class ShiftAllocation
    {

        public long ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<long> ShiftType { get; set; }
        public string ShiftDes { get; set; }
        public long Employee_ID { get; set; }
        public Nullable<long> EmployeeName { get; set; }
        public string EmployeeNameDes { get; set; }
        public string Note { get; set; }
        public virtual tbl_EmployeeRegister tbl_EmployeeRegister { get; set; }
        public virtual tbl_ShiftMaster tbl_ShiftMaster { get; set; }
      //  public virtual ICollection<tbl_ShiftAllocationDetails> tbl_ShiftAllocationDetails { get; set; }
        public List<ShiftAllocationDetails> lstAllotcation { get; set; }
      //  public virtual tbl_ShiftAllocationDetails tbl_ShiftAllocationDetails { get; set; }

    }
}