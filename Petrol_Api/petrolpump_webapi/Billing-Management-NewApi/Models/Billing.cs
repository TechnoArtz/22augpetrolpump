﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class Billing
    {
        public long ID { get; set; }
        public Nullable<long> BillNo { get; set; }
        public string Party { get; set; }
        public string VehicleNo { get; set; }
        public Nullable<long> ItemType { get; set; }
        public string ItemTypeDes { get; set; }
        public Nullable<int> Qty { get; set; }
        public Nullable<long> Rate { get; set; }
        public long RateDes { get; set; }
        public Nullable<double> Total { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual tbl_TankMaster tbl_TankMaster { get; set; }
       
    }
}