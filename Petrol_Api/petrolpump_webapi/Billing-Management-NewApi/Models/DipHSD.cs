﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class DipHSD
    {
        public long ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<double> CurrentDip { get; set; }
        public Nullable<double> ActualDip { get; set; }
        public Nullable<double> Variation { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}