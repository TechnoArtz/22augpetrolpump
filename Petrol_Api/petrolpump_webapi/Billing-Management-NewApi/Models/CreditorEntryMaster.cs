using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Models
{
    public class CreditorEntryMaster
    {

        public long ID { get; set; }
        public string CreditorName { get; set; }
        public Nullable<long> Billno { get; set; }
        public Nullable<System.DateTime> BillDate { get; set; }
        public string VehicleNo { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public List<CreditorEntryDetails> lstcreditordetails { get; set; }
        //public virtual ICollection<tbl_CreaditorEntryDetails> tbl_CreaditorEntryDetails { get; set; }
       // public virtual tbl_CreditorMaster tbl_CreditorMaster { get; set; }
    }
}