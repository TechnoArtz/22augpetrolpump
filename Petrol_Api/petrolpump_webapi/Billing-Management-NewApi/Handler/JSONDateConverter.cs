﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Billing_Management_NewApi.Handler
{
    public class JSONDateConverter : DateTimeConverterBase
    {

        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
        {
            var date = value != null ? (DateTime)value : DateTime.MinValue;
            writer.WriteValue(date.ToString("dd/MM/yyyy"));
        }
        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            DateTime outDate = DateTime.MinValue;
            DateTime.TryParseExact(reader.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outDate);
            return outDate;
        }
    }
}