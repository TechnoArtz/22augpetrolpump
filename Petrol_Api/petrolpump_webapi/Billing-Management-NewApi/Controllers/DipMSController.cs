﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class DipMSController:BaseController
    {
        GSDBSExtended entity;
        IDipMSRepository repository;
        public DipMSController()
        {
            repository = new DipMSRepository();
            entity = new GSDBSExtended();
        }
        /// <summary>
        /// Returns list of DipMS
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Success(repository.Get());
        }
        //[HttpGet]
        //public IHttpActionResult GetType(string Type)
        //{
        //    var result = repository.GetType(Type);
        //    if (result != null)
        //        return Success(result);
        //    else
        //        return Error("No record found");
        //}
        [HttpGet]
        [Route("api/DipMS/GetType/{id}")]
        public IHttpActionResult GetType(string id)
        {
            var type = repository.Get(TypeCaster.TryConvertToLong(id));
            if (type != null)
                return Success(type);
            else
                return Error("No record found");
        }

        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpPost]
        public IHttpActionResult Post(DipMS dipms)
        {
            return Success(repository.CreateDipMS(dipms));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, DipMS dipms)
        {
            dipms.ID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateDipMS(dipms);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveDipMS(TypeCaster.TryConvertToLong(id)));
        }
    }
}