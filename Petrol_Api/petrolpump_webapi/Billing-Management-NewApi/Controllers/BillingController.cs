﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class BillingController:BaseController
    {
        IBillingRepository repository;
        public BillingController()
        {
            repository = new BillingRepository();
        }
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Success(repository.Get());
        }
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpPost]
        public IHttpActionResult Post(Billing billing)
        {
            return Success(repository.CreateBilling(billing));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, Billing billing)
        {
            billing.ID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateBilling(billing);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveBilling(TypeCaster.TryConvertToLong(id)));
        }
    }
}