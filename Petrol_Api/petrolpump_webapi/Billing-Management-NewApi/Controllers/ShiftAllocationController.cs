using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class ShiftAllocationController:BaseController
    {
        ShiftAllocationDetailsRepository repository;
        public ShiftAllocationController()
        {
            repository = new ShiftAllocationDetailsRepository();
        }
        [HttpGet]
        [ActionName("List")]
        public IHttpActionResult GetList()
        {
            return Success(repository.Get());
        }

        //[HttpGet]
        //[ActionName("EmployeeRegMaster")]
        //public IHttpActionResult GetEmployeeRegMaster()
        //{
        //    return Success(repository.GetEmployeeRegMaster());
        //}
        //[HttpGet]
        //[ActionName("ShiftMaster")]
        //public IHttpActionResult GetShiftMaster()
        //{
        //    return Success(repository.GetShiftMaster());
        //}
        //[HttpGet]
        //[ActionName("TankMaster")]
        //public IHttpActionResult GetTankMaster()
        //{
        //    return Success(repository.GetTankMaster());
        //}
        //[HttpGet]
        //[ActionName("NozzelMaster")]
        //public IHttpActionResult GetNozzelMaster()
        //{
        //    return Success(repository.GetNozzelMaster());
        //}
        [HttpGet]
        [ActionName("ShiftID")]
        public IHttpActionResult GetShiftID(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }


        [HttpPost]
        [ActionName("FilterdData")]
        public IHttpActionResult GetFilterdData(SearchRecord searchrecord)
        {
            return Success(repository.Filter(searchrecord));
        }
       
        [HttpPost]
        [ActionName("Create")]
        public IHttpActionResult PostCreate(ShiftAllocation shiftallocation)
        {
            return Success(repository.CreateShiftAllocationDetails(shiftallocation));
        }
        //[HttpPost]
        //public IHttpActionResult Post(ShiftAllocationDetails detail)
        //{
        //    return Success(repository.AddShiftDetails(detail));
        //}

        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, ShiftAllocation shiftallocation)
        {
            shiftallocation.ID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateShiftAllocationDetails(shiftallocation);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
    }
}