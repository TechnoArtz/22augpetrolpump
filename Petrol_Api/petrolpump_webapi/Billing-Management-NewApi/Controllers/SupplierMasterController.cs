﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class SupplierMasterController: BaseController
    {
        ISupplierMasterRepository repository;
        public SupplierMasterController()
        {
            repository = new SupplierMasterRepository();
        }
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Success(repository.Get());
        }
        [HttpPost]
        public IHttpActionResult Post(SupplierMaster supplier)
        {
            return Success(repository.CreateSupplier(supplier));
        }
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveSupplier(TypeCaster.TryConvertToLong(id)));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, SupplierMaster supplier)
        {
            supplier.ID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateSupplier(supplier);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
    }
}