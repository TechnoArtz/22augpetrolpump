using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class ReceiveItemMasterController:BaseController
    {
        GSDBSExtended entity;
        IReceiveItemMasterRepository repository;
        public ReceiveItemMasterController()
        {
            repository = new ReceiveItemMasterRepository();
            entity = new GSDBSExtended();
        }
        
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Success(repository.Get());
        }
       
        //[HttpGet]
       
        //public IHttpActionResult GetType(string id)
        //{
        //    var type = repository.Get(TypeCaster.TryConvertToLong(id));
        //    if (type != null)
        //        return Success(type);
        //    else
        //        return Error("No record found");
        //}

        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpPost]
        public IHttpActionResult Post(ReceiveItemMaster receiveItem)
        {
            return Success(repository.CreateReceiveItemMaster(receiveItem));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, ReceiveItemMaster receiveItem)
        {
            receiveItem.ID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateReceiveItemMaster(receiveItem);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveReceiveItemMaster(TypeCaster.TryConvertToLong(id)));
        }
    }
}