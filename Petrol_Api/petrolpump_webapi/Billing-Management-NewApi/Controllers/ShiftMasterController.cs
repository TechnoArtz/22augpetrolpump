using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Text;
using System.Diagnostics;

namespace Billing_Management_NewApi.Controllers
{
    public class ShiftMasterController:BaseController
    {
        PdfPTable _pdfPTable = new PdfPTable(4);
        PdfPCell _pdfPCell = new PdfPCell();
        //string FileName = "~/test.txt";
        //string fileType = ".txt";
        List<ShiftMaster> _shiftmaster = new List<ShiftMaster>();
      //  ShiftMaster _shiftMaster = new ShiftMaster();
        IShiftMasterRepository repository;
        public ShiftMasterController()
        {
            repository = new ShiftMasterRepository();
        }
        /// <summary>
        /// Pdf file designd by html way
        /// </summary>
        /// <param name="shiftMasters"></param>
        /// <returns></returns>
        //public static string GetHTMLString()
        //{
        //    ShiftMasterRepository a = new ShiftMasterRepository();
        //    var employees = a.Get();

        //    var sb = new StringBuilder();
        //    sb.Append(@"
        //               <html>
        //                   <head>
        //                   </head>
        //                   <body>
        //                       <div class='header'><h1>This is the generated PDF report!!!</h1></div>
        //                       <table align='center'>
        //                           <tr>
        //                               <th>Id</th>
        //                               <th>Type</th>
        //                               <th>Age</th>
        //                               <th>Gender</th>
        //                           </tr>");

        //    foreach (var emp in employees)
        //    {
        //        sb.AppendFormat(@"<tr>
        //                           <td>{0}</td>
        //                           <td>{1}</td>

        //                         </tr>", emp.Type, emp.ID);
        //    }

        //    sb.Append(@"
        //                       </table>
        //                   </body>
        //               </html>");

        //    return sb.ToString();
        //}

        //public Stream CreatePdfById(ShiftMaster _shiftMaster)
        //{

        //    //GSDBSEntities e = new GSDBSEntities();
        //    //var a = e.tbl_ShiftMaster.Where(s => s.Id == _shiftMaster.ID).FirstOrDefault();
        //    {
        //        //  _shiftMaster.ID = shiftMaster.ID;
        //        PdfPTable tableLayout = new PdfPTable(4);
        //        PdfPCell pdfPCell = new PdfPCell();
        //        using (var document = new Document(PageSize.A4, 50, 50, 25, 25))
        //        {
        //            var output = new MemoryStream();

        //            var writer = PdfWriter.GetInstance(document, output);
        //            writer.CloseStream = false;

        //            document.Open();
        //            this.ReportBody();
        //            document.Add(new Paragraph(" Hello World "));
        //            document.Add(_pdfPTable);
        //            // document.Add(Add_Content_To_PDF(tableLayout));



        //            document.Close();

        //            output.Seek(0, SeekOrigin.Begin);

        //            return output;
        //        }
        //    }
        //}


        /// <summary>
        /// Design of Pdf file by Pdfptable using itextsharp
        /// </summary>
        /// <param name="shiftMasters"></param>
        /// <returns></returns>


        public Stream CreatePdf( List<ShiftMaster> shiftMasters)
        {
            #region CreatePdf
            _shiftmaster = shiftMasters;
            // PdfPTable tableLayout = new PdfPTable(4);
            var titleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
            var titleFontBlue = FontFactory.GetFont("Arial", 14, Font.NORMAL, BaseColor.BLUE);
            var boldTableFont = FontFactory.GetFont("Arial", 8, Font.BOLD);
            var bodyFont = FontFactory.GetFont("Arial", 8, Font.NORMAL);
            var EmailFont = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.BLUE);
            BaseColor TabelHeaderBackGroundColor = BaseColor.MAGENTA;

            PdfPCell pdfPCell = new PdfPCell();
            using (var document = new Document(PageSize.A4, 50, 50, 25, 25))
            {
                var output = new MemoryStream();

                var writer = PdfWriter.GetInstance(document, output);
                writer.CloseStream = false;

                document.Open();

                #region ReceiptDemo
                foreach (ShiftMaster ex in _shiftmaster)
                {
                    {

                        PdfPTable headertable = new PdfPTable(1);
                        headertable.HorizontalAlignment = 0;
                        headertable.WidthPercentage = 100;
                       // headertable.SetWidths(new float[] { 100f, 320f, 100f });  // then set the column's __relative__ widths
                        headertable.DefaultCell.Border = Rectangle.NO_BORDER;
                        iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/images/petrolpumplogo1.png"));
                        logo.ScaleToFit(100, 30);
                        iTextSharp.text.Image backimage = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/images/petrolpumplogo1.png"));
                        //backimage.ScaleToFit(500, 200);

                        {
                            PdfPCell pdfCelllogo = new PdfPCell(logo);
                            pdfCelllogo.Border = Rectangle.NO_BORDER;
                            pdfCelllogo.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                            pdfCelllogo.BorderWidthBottom = 1f;

                            headertable.AddCell(pdfCelllogo);
                        }
                        document.Add(headertable);


                        

                        {
                            PdfPCell middlecell = new PdfPCell();
                            middlecell.Border = Rectangle.NO_BORDER;
                            middlecell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                            middlecell.BorderWidthBottom = 1f;
                            headertable.AddCell(middlecell);
                        }

                        PdfPTable recheader = new PdfPTable(1);
                        recheader.DefaultCell.Border = 5;
                        recheader.HorizontalAlignment = 0;
                        recheader.WidthPercentage = 100;
                        // recheader.SetWidths(new float[] { 100f, 320f, 100f });  // then set the column's __relative__ widths
                        recheader.DefaultCell.Border = Rectangle.NO_BORDER;
                        recheader.HorizontalAlignment = Element.ALIGN_CENTER;
                        PdfPTable nested = new PdfPTable(1);
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        PdfPCell nextPostCell1 = new PdfPCell(new Phrase("Company Name", titleFont));
                        nextPostCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                        nextPostCell1.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell1);
                        PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Name: Ramesh Khanna   " , bodyFont));
                        nextPostCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                        nextPostCell2.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell2);
                        PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Department: Marketing   " , bodyFont));
                        nextPostCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                        nextPostCell3.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell3);
                        PdfPCell nextPostCell4 = new PdfPCell(new Phrase("Email: company@example.com", EmailFont));
                        nextPostCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                        nextPostCell4.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell4);
                        nested.AddCell("");
                        PdfPCell nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        nesthousing.BorderWidthBottom = 1f;
                        nesthousing.Rowspan = 5;
                        nesthousing.PaddingBottom = 10f;
                        recheader.AddCell(nesthousing);
                        document.Add(recheader);
                    }


                    #region receiptitem
                    {
                        PdfPTable itemTable1 = new PdfPTable(5);

                        itemTable1.HorizontalAlignment = 0;
                        itemTable1.WidthPercentage = 100;
                        itemTable1.SetWidths(new float[] { 5, 40, 10, 20, 25 });  // then set the column's __relative__ widths
                        itemTable1.SpacingAfter = 40;
                        itemTable1.DefaultCell.Border = Rectangle.BOX;

                        PdfPCell cell11 = new PdfPCell(new Phrase("NO", boldTableFont));
                        cell11.BackgroundColor = TabelHeaderBackGroundColor;
                        cell11.HorizontalAlignment = Element.ALIGN_CENTER;
                        itemTable1.AddCell(cell11);
                        PdfPCell cell22 = new PdfPCell(new Phrase("DESCRIPTION", boldTableFont));
                        cell22.MinimumHeight = 30;
                        cell22.BackgroundColor = TabelHeaderBackGroundColor;
                        cell22.HorizontalAlignment = 1;
                        itemTable1.AddCell(cell22);
                        PdfPCell cell33 = new PdfPCell(new Phrase("QUANTITY", boldTableFont));
                        cell33.MinimumHeight = 30;
                        cell33.BackgroundColor = TabelHeaderBackGroundColor;
                        cell33.HorizontalAlignment = Element.ALIGN_CENTER;
                        itemTable1.AddCell(cell33);
                        PdfPCell cell44 = new PdfPCell(new Phrase("UNIT AMOUNT", boldTableFont));
                        cell44.BackgroundColor = TabelHeaderBackGroundColor;
                        cell44.HorizontalAlignment = Element.ALIGN_CENTER;
                        itemTable1.AddCell(cell44);
                        PdfPCell cell55 = new PdfPCell(new Phrase("TOTAL", boldTableFont));
                        cell55.BackgroundColor = TabelHeaderBackGroundColor;
                        cell55.HorizontalAlignment = Element.ALIGN_CENTER;
                        itemTable1.AddCell(cell55);
                        int srno1 = 1;
                        //foreach (DataRow row in dt.Rows)
                        //  foreach(EmployeeSalary emp in _employeeSalaries)
                        {
                            PdfPCell numberCell = new PdfPCell(new Phrase(srno1++.ToString(), bodyFont));
                            numberCell.HorizontalAlignment = 1;
                            numberCell.PaddingLeft = 10f;
                            numberCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                            itemTable1.AddCell(numberCell);

                            var _phrase = new Phrase();
                            _phrase.Add(new Chunk(ex.Type, EmailFont));
                           // _phrase.Add(new Chunk("Subscription Plan description will add here.", bodyFont));
                            PdfPCell descCell = new PdfPCell(_phrase);
                            descCell.HorizontalAlignment = 0;
                            descCell.PaddingLeft = 10f;
                            descCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                            descCell.MinimumHeight = 40;
                            itemTable1.AddCell(descCell);

                            PdfPCell qtyCell = new PdfPCell(new Phrase("Item1", bodyFont));
                            qtyCell.HorizontalAlignment = 1;
                            qtyCell.PaddingLeft = 10f;
                            qtyCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                            itemTable1.AddCell(qtyCell);

                            PdfPCell amountCell = new PdfPCell(new Phrase("Item2", bodyFont));
                            amountCell.HorizontalAlignment = 1;
                            amountCell.PaddingLeft = 10f;
                            amountCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                            itemTable1.AddCell(amountCell);

                            PdfPCell totalamtCell = new PdfPCell(new Phrase("2000", bodyFont));
                            totalamtCell.HorizontalAlignment = 1;
                            totalamtCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                            itemTable1.AddCell(totalamtCell);

                        }
                        // Table footer
                        // foreach (EmployeeSalary emp in _employeeSalaries)
                        {
                            PdfPCell totalAmtCell1 = new PdfPCell(new Phrase(""));
                            totalAmtCell1.Border = Rectangle.LEFT_BORDER | Rectangle.TOP_BORDER;
                            itemTable1.AddCell(totalAmtCell1);
                            PdfPCell totalAmtCell2 = new PdfPCell(new Phrase(""));
                            totalAmtCell2.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                            itemTable1.AddCell(totalAmtCell2);
                            PdfPCell totalAmtCell3 = new PdfPCell(new Phrase(""));
                            totalAmtCell3.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                            itemTable1.AddCell(totalAmtCell3);
                            PdfPCell totalAmtStrCell = new PdfPCell(new Phrase("5000", boldTableFont));
                            totalAmtStrCell.Border = Rectangle.TOP_BORDER;   //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                            totalAmtStrCell.HorizontalAlignment = 1;
                            itemTable1.AddCell(totalAmtStrCell);
                            PdfPCell totalAmtCell = new PdfPCell(new Phrase("100000", boldTableFont));
                            totalAmtCell.HorizontalAlignment = 1;
                            itemTable1.AddCell(totalAmtCell);
                        }
                        {
                            PdfPCell lcell = new PdfPCell(new Phrase("***NOTICE: A finance charge of 1.5% will be made on unpaid balances after 30 days. ***", bodyFont));
                            lcell.Colspan = 5;
                            lcell.HorizontalAlignment = 1;
                            itemTable1.AddCell(lcell);
                            document.Add(itemTable1);
                        }
                        {
                            PdfPTable footer = new PdfPTable(1);
                            footer.DefaultCell.Border = 5;
                            footer.HorizontalAlignment = 0;
                            footer.WidthPercentage = 100;
                            // recheader.SetWidths(new float[] { 100f, 320f, 100f });  // then set the column's __relative__ widths
                            footer.DefaultCell.Border = Rectangle.NO_BORDER;
                            footer.HorizontalAlignment = Element.ALIGN_RIGHT;
                            PdfPTable nested = new PdfPTable(1);
                            nested.DefaultCell.Border = Rectangle.NO_BORDER;
                            PdfPCell nextPostCell1 = new PdfPCell(new Phrase("Company Name", titleFont));
                            nextPostCell1.HorizontalAlignment = Element.ALIGN_RIGHT;
                            nextPostCell1.Border = Rectangle.NO_BORDER;
                            nested.AddCell(nextPostCell1);
                            PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Name: Ramesh Khanna   ", bodyFont));
                            nextPostCell2.HorizontalAlignment = Element.ALIGN_RIGHT;
                            nextPostCell2.Border = Rectangle.NO_BORDER;
                            nested.AddCell(nextPostCell2);
                            PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Department: Marketing   ", bodyFont));
                            nextPostCell3.HorizontalAlignment = Element.ALIGN_RIGHT;
                            nextPostCell3.Border = Rectangle.NO_BORDER;
                            nested.AddCell(nextPostCell3);
                            PdfPCell nextPostCell4 = new PdfPCell(new Phrase("Email: company@example.com", EmailFont));
                            nextPostCell4.HorizontalAlignment = Element.ALIGN_RIGHT;
                            nextPostCell4.Border = Rectangle.NO_BORDER;
                            nested.AddCell(nextPostCell4);
                            nested.AddCell("");
                            PdfPCell nesthousing = new PdfPCell(nested);
                            nesthousing.Border = Rectangle.NO_BORDER;
                            nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                            nesthousing.BorderWidthBottom = 1f;
                            nesthousing.Rowspan = 5;
                            nesthousing.PaddingBottom = 10f;
                            footer.AddCell(nesthousing);
                            document.Add(footer);
                        }


                    }
                    #endregion
                    #endregion
                }
                // this.ReportBody();
                //document.Add(new Paragraph(" Hello World "));
                // document.Add(_pdfPTable);
                // document.Add(Add_Content_To_PDF(tableLayout));



                document.Close();

                output.Seek(0, SeekOrigin.Begin);

                return output;
            }
            #endregion
        }

        //private void ReportBody()
        //{
        //    #region ReportBody 
        //    _pdfPCell = new PdfPCell(new Phrase("SerialNo"));
        //    _pdfPTable.AddCell(_pdfPCell);
        //    _pdfPCell = new PdfPCell(new Phrase("ID"));
        //    _pdfPTable.AddCell(_pdfPCell);
        //    _pdfPCell = new PdfPCell(new Phrase("Type"));
        //    _pdfPTable.AddCell(_pdfPCell);
        //    _pdfPCell = new PdfPCell(new Phrase("StartTime"));
        //    _pdfPTable.AddCell(_pdfPCell);
        //    //_pdfPCell = new PdfPCell(new Phrase("5"));
        //    //_pdfPTable.AddCell(_pdfPCell);
        //    _pdfPTable.CompleteRow();
        //    #region tablebody
        //    int serialno = 1;
        //    foreach (ShiftMaster ex in _shiftmaster)
        //    {
        //        _pdfPCell = new PdfPCell(new Phrase(serialno++.ToString()));
        //        _pdfPTable.AddCell(_pdfPCell);
        //        _pdfPCell = new PdfPCell(new Phrase(ex.ID));
        //        _pdfPTable.AddCell(_pdfPCell);
        //        _pdfPCell = new PdfPCell(new Phrase(ex.Type.ToString()));
        //        _pdfPTable.AddCell(_pdfPCell);
        //        _pdfPCell = new PdfPCell(new Phrase(ex.StartTime.ToString()));
        //        _pdfPTable.AddCell(_pdfPCell);
        //        //_pdfPCell = new PdfPCell(new Phrase(ex.EndTime.ToString()));
        //        //_pdfPTable.AddCell(_pdfPCell);
        //        _pdfPTable.CompleteRow();
        //    }


        //    #endregion
        //    #endregion


        //}
        /// <summary>
        /// GeneratePdffile and download 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GeneratePdf")]
        public HttpResponseMessage GetGeneratePdf()
        {
            #region

            var stream = CreatePdf(repository.Get());
              // var stream = CreatePdfById(repository.Get(id));

            return new HttpResponseMessage
            {
                Content = new StreamContent(stream)
                {
                    Headers =
            {
                ContentType = new MediaTypeHeaderValue("application/pdf"),
                ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "myfile.pdf"
                }
            }
                },
                StatusCode = HttpStatusCode.OK
            };
            #endregion
        }
     //   public PdfPTable Add_Content_To_PDF(PdfPTable tableLayout)

     //   {

     //       float[] headers = { 20, 20, 30, 30 };  //Header Widths

     //       tableLayout.SetWidths(headers);        //Set the pdf headers

     //       tableLayout.WidthPercentage = 80;       //Set the PDF File witdh percentage



     //       //Add Title to the PDF file at the top

     //       tableLayout.AddCell(new PdfPCell(new Phrase("Creating PDF file using iTextsharp")));



     //       //Add header

     //       AddCellToHeader(tableLayout, "Cricketer Name");

     //       AddCellToHeader(tableLayout, "Height");

     //       AddCellToHeader(tableLayout, "Born On");

     //       AddCellToHeader(tableLayout, "Parents");



     //       //Add body

     //       AddCellToBody(tableLayout, "Sachin Tendulkar");

     //       AddCellToBody(tableLayout, "1.65 m");

     //       AddCellToBody(tableLayout, "April 24, 1973");

     //       AddCellToBody(tableLayout, "Ramesh Tendulkar, Rajni Tendulkar");



     //       AddCellToBody(tableLayout, "Mahendra Singh Dhoni");

     //       AddCellToBody(tableLayout, "1.75 m");

     //       AddCellToBody(tableLayout, "July 7, 1981");

     //       AddCellToBody(tableLayout, "Devki Devi, Pan Singh");



     //       AddCellToBody(tableLayout, "Virender Sehwag");

     //       AddCellToBody(tableLayout, "1.70 m");

     //       AddCellToBody(tableLayout, "October 20, 1978");

     //       AddCellToBody(tableLayout, "Aryavir Sehwag, Vedant Sehwag");



     //       AddCellToBody(tableLayout, "Virat Kohli");

     //       AddCellToBody(tableLayout, "1.75 m");

     //       AddCellToBody(tableLayout, "November 5, 1988");

     //       AddCellToBody(tableLayout, "Saroj Kohli, Prem Kohli");



     //       return tableLayout;

     //   }
     //   // Method to add single cell to the header

     //  public static void AddCellToHeader(PdfPTable tableLayout, string cellText)

     //   {

     //       tableLayout.AddCell(new PdfPCell(new Phrase(cellText)));

     //   }
     //   // Method to add single cell to the body

     //public static void AddCellToBody(PdfPTable tableLayout, string cellText)

     //   {

     //       tableLayout.AddCell(new PdfPCell(new Phrase(cellText)));

     //   }
       
       
        //[HttpGet]
        //public HttpResponseMessage  DownloadPDF()
        //{
            
        //    HttpResponseMessage result = null;
        //   string htmlContent = GetHTMLString();
        //   // var localFilePath = HttpContext.Current.Server.MapPath("~/test.txt");
        //    result = Request.CreateResponse(HttpStatusCode.OK);
        //    result.Content = new StreamContent(new FileStream(htmlContent, FileMode.Open, FileAccess.Read));
        //    result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
        //    result.Content.Headers.ContentDisposition.FileName = "SampleImg";
        //    return result;

        //}
        //[HttpGet]
        //public HttpResponseMessage DownLoadFile()
        //{
        //    HttpResponseMessage result = null;
        //    var localFilePath = HttpContext.Current.Server.MapPath("~/test.txt");

        //    if (!File.Exists(localFilePath))
        //    {
        //      result = Request.CreateResponse(HttpStatusCode.Gone);
        //    }
        //    else
        //    {
        //        // Serve the file to the client
        //        result = Request.CreateResponse(HttpStatusCode.OK);
        //        result.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
        //        result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
        //        result.Content.Headers.ContentDisposition.FileName = "SampleImg";
               
        //    }

        //    return result;
           
        //}

        //public byte[] GetPDF(string pHTML)
        //{
        //    byte[] bPDF = null;

        //    MemoryStream ms = new MemoryStream();
        //    TextReader txtReader = new StringReader(pHTML);

        //    // 1: create object of a itextsharp document class  
        //    Document doc = new Document(PageSize.A4, 25, 25, 25, 25);

        //    // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
        //    PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

        //    // 3: we create a worker parse the document  
        //    HTMLWorker htmlWorker = new HTMLWorker(doc);

        //    // 4: we open document and start the worker on the document  
        //    doc.Open();
        //    htmlWorker.StartDocument();


        //    // 5: parse the html into the document  
        //    htmlWorker.Parse(txtReader);

        //    // 6: close the document and the worker  
        //    htmlWorker.EndDocument();
        //    htmlWorker.Close();
        //    doc.Close();

        //    bPDF = ms.ToArray();

        //    return bPDF;
        //}

        [HttpGet]
        [ActionName("List")]
        public IHttpActionResult GetList()
        {
            return Success(repository.Get());
        }
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpPost]
        [ActionName("CreateShiftMaster")]
        public IHttpActionResult PostCreateShiftMaster(ShiftMaster shiftmaster)
        {
            return Success(repository.CreateShiftMaster(shiftmaster));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, ShiftMaster shiftmaster)
        {
            shiftmaster.ID= TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateShiftMaster(shiftmaster);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveShiftMaster(TypeCaster.TryConvertToLong(id)));
        }
    }
}