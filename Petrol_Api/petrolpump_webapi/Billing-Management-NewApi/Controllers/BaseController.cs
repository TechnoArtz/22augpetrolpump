﻿using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class BaseController : ApiController
    {
        protected IHttpActionResult Success<T>(T content)
        {
            ResponseBase result = new ResponseBase();
            result.Data = content;
            result.IsSuccess = true;
            result.Message = "";
            return base.Ok(result);
        }
        protected IHttpActionResult Error(string Message)
        {
            ResponseBase result = new ResponseBase();
            result.Data = "";
            result.IsSuccess = false;
            result.Message = Message;
            return base.Ok(result);
        }
    }
    public class ResponseBase
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
