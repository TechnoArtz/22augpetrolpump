﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class ShiftEntryController : BaseController
    {
        IShiftEntryRepository repository;
        public ShiftEntryController()
        {
            repository = new ShiftEntryRepository();
        }
        /// <summary>
        /// Returns list of employees
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Success(repository.Get());
        }
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpPost]
        public IHttpActionResult Post(ShiftEntry shiftentry)
        {
            return Success(repository.CreateShiftEntry(shiftentry));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, ShiftEntry shiftentry)
        {
            shiftentry.ID= TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateShiftEntry(shiftentry);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveShiftEntry(TypeCaster.TryConvertToLong(id)));
        }

    }
}