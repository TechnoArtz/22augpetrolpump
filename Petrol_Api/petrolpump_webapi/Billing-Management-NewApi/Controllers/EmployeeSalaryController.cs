using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class EmployeeSalaryController : BaseController
    {
        PdfPTable Maintable = new PdfPTable(2);
        float[] widths1 = new float[] { 50, 50 };
        PdfPTable _pdfPTable = new PdfPTable(2);
        PdfPTable table3 = new PdfPTable(6);
        PdfPCell _pdfPCell;
        Font _font;
        float[] widths = new float[] { 200f, 100f };

        int _totalColumn = 3;
        //string FileName = "~/test.txt";
        //string fileType = ".txt";
        List<EmployeeSalary> _employeeSalaries = new List<EmployeeSalary>();
        IEmployeeSalaryRepository repository;
        public EmployeeSalaryController()
        {
            repository = new EmployeeSalaryRepository();
        }

        public Stream CreatePdf(List<EmployeeSalary> employeeSalaries)
        {
            #region CreatePdf
            _employeeSalaries = employeeSalaries;
            // PdfPTable tableLayout = new PdfPTable(4);

            using (var document = new Document(PageSize.A4, 50, 50, 25, 25))
            {
                var output = new MemoryStream();

                var writer = PdfWriter.GetInstance(document, output);
                writer.CloseStream = false;
                _pdfPTable.WidthPercentage = 100;
                document.Open();
                Rectangle one = new Rectangle(500, 600);
                //document.SetPageSize(one);
                var titleFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
                var titleFontBlue = FontFactory.GetFont("Arial", 14, Font.NORMAL, BaseColor.BLUE);
                var boldTableFont = FontFactory.GetFont("Arial", 8, Font.BOLD);
                var bodyFont = FontFactory.GetFont("Arial", 8, Font.NORMAL);
                var EmailFont = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.BLUE);
                BaseColor TabelHeaderBackGroundColor = BaseColor.CYAN;

                Rectangle pageSize = writer.PageSize;

                // Open the Document for writing
                document.Open();
                //Add elements to the document here

                #region Top table
                // Create the header table 
                foreach (EmployeeSalary emp in _employeeSalaries)
                {
                    PdfPTable headertable = new PdfPTable(3);
                    headertable.HorizontalAlignment = 0;
                    headertable.WidthPercentage = 100;
                    headertable.SetWidths(new float[] { 100f, 320f, 100f });  // then set the column's __relative__ widths
                    headertable.DefaultCell.Border = Rectangle.NO_BORDER;
                    //headertable.DefaultCell.Border = Rectangle.BOX; //for testing           

                    iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/images/petrolpumplogo1.png"));
                    logo.ScaleToFit(100, 30);
                    iTextSharp.text.Image backimage = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/images/petrolpumplogo1.png"));
                    //backimage.ScaleToFit(500, 200);

                    {
                        PdfPCell pdfCelllogo = new PdfPCell(logo);
                        pdfCelllogo.Border = Rectangle.NO_BORDER;
                        pdfCelllogo.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        pdfCelllogo.BorderWidthBottom = 1f;

                        headertable.AddCell(pdfCelllogo);
                    }



                    //If you want to choose image as background then,

                    //  backimage.Alignment = iTextSharp.text.Image.UNDERLYING;

                    //If you want to give absolute/specified fix position to image.
                    //  backimage.SetAbsolutePosition(0,0);
                    //  backimage.SetAbsolutePosition((PageSize.A4.Width - backimage.ScaledWidth) / 2, (PageSize.A4.Height - backimage.ScaledHeight) / 2);
                    //backimage.SetAbsolutePosition(0, 0); // set the position to bottom left corner of pdf
                    //backimage.ScaleAbsolute(iTextSharp.text.PageSize.A4.Width, iTextSharp.text.PageSize.A4.Height); // set the height and width of image to PDF page size

                    //document.Add(backimage);

                    {
                        PdfPCell middlecell = new PdfPCell();
                        middlecell.Border = Rectangle.NO_BORDER;
                        middlecell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        middlecell.BorderWidthBottom = 1f;
                        headertable.AddCell(middlecell);
                    }
                    //  foreach (EmployeeSalary emp in _employeeSalaries)
                    {
                        PdfPTable nested = new PdfPTable(1);
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        PdfPCell nextPostCell1 = new PdfPCell(new Phrase("GSPetrolPump", titleFont));
                        nextPostCell1.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell1);
                        PdfPCell nextPostCell2 = new PdfPCell(new Phrase("xxx City Heights, AZ 8xxx4, US,", bodyFont));
                        nextPostCell2.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell2);
                        PdfPCell nextPostCell3 = new PdfPCell(new Phrase("(+91) 986588-888", bodyFont));
                        nextPostCell3.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell3);
                        PdfPCell nextPostCell4 = new PdfPCell(new Phrase("company@example.com", EmailFont));
                        nextPostCell4.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell4);
                        nested.AddCell("");
                        PdfPCell nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        nesthousing.BorderWidthBottom = 1f;
                        nesthousing.Rowspan = 5;
                        nesthousing.PaddingBottom = 10f;
                        headertable.AddCell(nesthousing);
                    }


                    PdfPTable Invoicetable = new PdfPTable(3);
                    Invoicetable.HorizontalAlignment = 0;
                    Invoicetable.WidthPercentage = 100;
                    Invoicetable.SetWidths(new float[] { 100f, 320f, 100f });  // then set the column's __relative__ widths
                    Invoicetable.DefaultCell.Border = Rectangle.NO_BORDER;

                    // foreach (EmployeeSalary emp in _employeeSalaries)
                    {
                        PdfPTable nested = new PdfPTable(1);
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        PdfPCell nextPostCell1 = new PdfPCell(new Phrase("INVOICE TO:", bodyFont));
                        nextPostCell1.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell1);
                        PdfPCell nextPostCell2 = new PdfPCell(new Phrase(emp.EmployeeName.ToString(), titleFont));
                        nextPostCell2.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell2);
                        PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Deparment", bodyFont));
                        nextPostCell3.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell3);
                        PdfPCell nextPostCell4 = new PdfPCell(new Phrase("shivam@example.com", EmailFont));
                        nextPostCell4.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell4);
                        nested.AddCell("");
                        PdfPCell nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        //nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        //nesthousing.BorderWidthBottom = 1f;
                        nesthousing.Rowspan = 5;
                        nesthousing.PaddingBottom = 10f;
                        Invoicetable.AddCell(nesthousing);
                    }

                    {
                        PdfPCell middlecell = new PdfPCell();
                        middlecell.Border = Rectangle.NO_BORDER;
                        //middlecell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        //middlecell.BorderWidthBottom = 1f;
                        Invoicetable.AddCell(middlecell);
                    }


                    {
                        PdfPTable nested = new PdfPTable(1);
                        nested.DefaultCell.Border = Rectangle.NO_BORDER;
                        PdfPCell nextPostCell1 = new PdfPCell(new Phrase("INVOICE 3-2-1", titleFontBlue));
                        nextPostCell1.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell1);
                        PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Date of Invoice: " + DateTime.Now.ToShortDateString(), bodyFont));
                        nextPostCell2.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell2);
                        PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Due Date: " + DateTime.Now.AddDays(30).ToShortDateString(), bodyFont));
                        nextPostCell3.Border = Rectangle.NO_BORDER;
                        nested.AddCell(nextPostCell3);
                        nested.AddCell("");
                        PdfPCell nesthousing = new PdfPCell(nested);
                        nesthousing.Border = Rectangle.NO_BORDER;
                        //nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                        //nesthousing.BorderWidthBottom = 1f;
                        nesthousing.Rowspan = 5;
                        nesthousing.PaddingBottom = 10f;
                        Invoicetable.AddCell(nesthousing);
                    }


                    document.Add(headertable);
                    Invoicetable.PaddingTop = 10f;

                    document.Add(Invoicetable);

                #endregion

                    #region Items Table
                    //Create body table
                    PdfPTable itemTable = new PdfPTable(5);

                    itemTable.HorizontalAlignment = 0;
                    itemTable.WidthPercentage = 100;
                    itemTable.SetWidths(new float[] { 5, 40, 10, 20, 25 });  // then set the column's __relative__ widths
                    itemTable.SpacingAfter = 40;
                    itemTable.DefaultCell.Border = Rectangle.BOX;

                    PdfPCell cell1 = new PdfPCell(new Phrase("ID", boldTableFont));
                    cell1.BackgroundColor = TabelHeaderBackGroundColor;
                    cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                    itemTable.AddCell(cell1);
                    PdfPCell cell2 = new PdfPCell(new Phrase("DESCRIPTION", boldTableFont));
                    cell2.BackgroundColor = TabelHeaderBackGroundColor;
                    cell2.HorizontalAlignment = 1;
                    itemTable.AddCell(cell2);
                    PdfPCell cell3 = new PdfPCell(new Phrase("QUANTITY", boldTableFont));
                    cell3.BackgroundColor = TabelHeaderBackGroundColor;
                    cell3.HorizontalAlignment = Element.ALIGN_CENTER;
                    itemTable.AddCell(cell3);
                    PdfPCell cell4 = new PdfPCell(new Phrase("UNIT AMOUNT", boldTableFont));
                    cell4.BackgroundColor = TabelHeaderBackGroundColor;
                    cell4.HorizontalAlignment = Element.ALIGN_CENTER;
                    itemTable.AddCell(cell4);
                    PdfPCell cell5 = new PdfPCell(new Phrase("TOTAL", boldTableFont));
                    cell5.BackgroundColor = TabelHeaderBackGroundColor;
                    cell5.HorizontalAlignment = Element.ALIGN_CENTER;
                    itemTable.AddCell(cell5);
                    //  int srno = 1;
                    //foreach (DataRow row in dt.Rows)
                    //  foreach(EmployeeSalary emp in _employeeSalaries)
                    {
                        PdfPCell numberCell = new PdfPCell(new Phrase(emp.ID.ToString()));
                        numberCell.HorizontalAlignment = 1;
                        numberCell.PaddingLeft = 10f;
                        numberCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                        itemTable.AddCell(numberCell);

                        var _phrase = new Phrase();
                        _phrase.Add(new Chunk(emp.EmployeeNameDes, EmailFont));
                        _phrase.Add(new Chunk("Subscription Plan description will add here.", bodyFont));
                        PdfPCell descCell = new PdfPCell(_phrase);
                        descCell.HorizontalAlignment = 0;
                        descCell.PaddingLeft = 10f;
                        descCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                        itemTable.AddCell(descCell);

                        PdfPCell qtyCell = new PdfPCell(new Phrase(emp.Dapartment.ToString(), bodyFont));
                        qtyCell.HorizontalAlignment = 1;
                        qtyCell.PaddingLeft = 10f;
                        qtyCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                        itemTable.AddCell(qtyCell);

                        PdfPCell amountCell = new PdfPCell(new Phrase(emp.Salary.ToString(), bodyFont));
                        amountCell.HorizontalAlignment = 1;
                        amountCell.PaddingLeft = 10f;
                        amountCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                        itemTable.AddCell(amountCell);

                        PdfPCell totalamtCell = new PdfPCell(new Phrase(emp.Bonus.ToString(), bodyFont));
                        totalamtCell.HorizontalAlignment = 1;
                        totalamtCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                        itemTable.AddCell(totalamtCell);

                    }
                    // Table footer
                    // foreach (EmployeeSalary emp in _employeeSalaries)
                    {
                        PdfPCell totalAmtCell1 = new PdfPCell(new Phrase(""));
                        totalAmtCell1.Border = Rectangle.LEFT_BORDER | Rectangle.TOP_BORDER;
                        itemTable.AddCell(totalAmtCell1);
                        PdfPCell totalAmtCell2 = new PdfPCell(new Phrase(""));
                        totalAmtCell2.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                        itemTable.AddCell(totalAmtCell2);
                        PdfPCell totalAmtCell3 = new PdfPCell(new Phrase(""));
                        totalAmtCell3.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                        itemTable.AddCell(totalAmtCell3);
                        PdfPCell totalAmtStrCell = new PdfPCell(new Phrase(emp.AvailableDays.ToString(), boldTableFont));
                        totalAmtStrCell.Border = Rectangle.TOP_BORDER;   //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                        totalAmtStrCell.HorizontalAlignment = 1;
                        itemTable.AddCell(totalAmtStrCell);
                        PdfPCell totalAmtCell = new PdfPCell(new Phrase(emp.Total.ToString(), boldTableFont));
                        totalAmtCell.HorizontalAlignment = 1;
                        itemTable.AddCell(totalAmtCell);
                    }
                    PdfPCell cell = new PdfPCell(new Phrase("***NOTICE: A finance charge of 1.5% will be made on unpaid balances after 30 days. ***", bodyFont));
                    cell.Colspan = 5;
                    cell.HorizontalAlignment = 1;
                    itemTable.AddCell(cell);
                    document.Add(itemTable);
                    #endregion

                    //#region ReceiptDemo
                    //{
                    //    PdfPTable recheader = new PdfPTable(1);
                    //    recheader.DefaultCell.Border = 5;
                    //    recheader.HorizontalAlignment = 0;
                    //    recheader.WidthPercentage = 100;
                    //    // recheader.SetWidths(new float[] { 100f, 320f, 100f });  // then set the column's __relative__ widths
                    //    recheader.DefaultCell.Border = Rectangle.NO_BORDER;
                    //    recheader.HorizontalAlignment = Element.ALIGN_CENTER;
                    //    PdfPTable nested = new PdfPTable(1);
                    //    nested.DefaultCell.Border = Rectangle.NO_BORDER;
                    //    PdfPCell nextPostCell1 = new PdfPCell(new Phrase("Company Name", titleFont));
                    //    nextPostCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                    //    nextPostCell1.Border = Rectangle.NO_BORDER;
                    //    nested.AddCell(nextPostCell1);
                    //    PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Name:   " + emp.EmployeeName, bodyFont));
                    //    nextPostCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                    //    nextPostCell2.Border = Rectangle.NO_BORDER;
                    //    nested.AddCell(nextPostCell2);
                    //    PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Department:   " + emp.Dapartment.ToString(), bodyFont));
                    //    nextPostCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                    //    nextPostCell3.Border = Rectangle.NO_BORDER;
                    //    nested.AddCell(nextPostCell3);
                    //    PdfPCell nextPostCell4 = new PdfPCell(new Phrase("Email: company@example.com", EmailFont));
                    //    nextPostCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                    //    nextPostCell4.Border = Rectangle.NO_BORDER;
                    //    nested.AddCell(nextPostCell4);
                    //    nested.AddCell("");
                    //    PdfPCell nesthousing = new PdfPCell(nested);
                    //    nesthousing.Border = Rectangle.NO_BORDER;
                    //    nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    //    nesthousing.BorderWidthBottom = 1f;
                    //    nesthousing.Rowspan = 5;
                    //    nesthousing.PaddingBottom = 10f;
                    //    recheader.AddCell(nesthousing);
                    //    document.Add(recheader);
                    //}


                    //#region receiptitem
                    //{
                    //    PdfPTable itemTable1 = new PdfPTable(5);

                    //    itemTable1.HorizontalAlignment = 0;
                    //    itemTable1.WidthPercentage = 100;
                    //    itemTable1.SetWidths(new float[] { 5, 40, 10, 20, 25 });  // then set the column's __relative__ widths
                    //    itemTable1.SpacingAfter = 40;
                    //    itemTable1.DefaultCell.Border = Rectangle.BOX;

                    //    PdfPCell cell11 = new PdfPCell(new Phrase("NO", boldTableFont));
                    //    cell11.BackgroundColor = TabelHeaderBackGroundColor;
                    //    cell11.HorizontalAlignment = Element.ALIGN_CENTER;
                    //    itemTable1.AddCell(cell11);
                    //    PdfPCell cell22 = new PdfPCell(new Phrase("DESCRIPTION", boldTableFont));
                    //    cell22.MinimumHeight = 30;
                    //    cell22.BackgroundColor = TabelHeaderBackGroundColor;
                    //    cell22.HorizontalAlignment = 1;
                    //    itemTable1.AddCell(cell22);
                    //    PdfPCell cell33 = new PdfPCell(new Phrase("QUANTITY", boldTableFont));
                    //    cell33.MinimumHeight = 30;
                    //    cell33.BackgroundColor = TabelHeaderBackGroundColor;
                    //    cell33.HorizontalAlignment = Element.ALIGN_CENTER;
                    //    itemTable1.AddCell(cell33);
                    //    PdfPCell cell44 = new PdfPCell(new Phrase("UNIT AMOUNT", boldTableFont));
                    //    cell44.BackgroundColor = TabelHeaderBackGroundColor;
                    //    cell44.HorizontalAlignment = Element.ALIGN_CENTER;
                    //    itemTable1.AddCell(cell44);
                    //    PdfPCell cell55 = new PdfPCell(new Phrase("TOTAL", boldTableFont));
                    //    cell55.BackgroundColor = TabelHeaderBackGroundColor;
                    //    cell55.HorizontalAlignment = Element.ALIGN_CENTER;
                    //    itemTable1.AddCell(cell55);
                    //    int srno1 = 1;
                    //    //foreach (DataRow row in dt.Rows)
                    //    //  foreach(EmployeeSalary emp in _employeeSalaries)
                    //    {
                    //        PdfPCell numberCell = new PdfPCell(new Phrase(srno1++.ToString(), bodyFont));
                    //        numberCell.HorizontalAlignment = 1;
                    //        numberCell.PaddingLeft = 10f;
                    //        numberCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    //        itemTable1.AddCell(numberCell);

                    //        var _phrase = new Phrase();
                    //        _phrase.Add(new Chunk(emp.EmployeeName, EmailFont));
                    //        _phrase.Add(new Chunk("Subscription Plan description will add here.", bodyFont));
                    //        PdfPCell descCell = new PdfPCell(_phrase);
                    //        descCell.HorizontalAlignment = 0;
                    //        descCell.PaddingLeft = 10f;
                    //        descCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    //        descCell.MinimumHeight = 40;
                    //        itemTable1.AddCell(descCell);

                    //        PdfPCell qtyCell = new PdfPCell(new Phrase(emp.Dapartment.ToString(), bodyFont));
                    //        qtyCell.HorizontalAlignment = 1;
                    //        qtyCell.PaddingLeft = 10f;
                    //        qtyCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    //        itemTable1.AddCell(qtyCell);

                    //        PdfPCell amountCell = new PdfPCell(new Phrase(emp.Salary.ToString(), bodyFont));
                    //        amountCell.HorizontalAlignment = 1;
                    //        amountCell.PaddingLeft = 10f;
                    //        amountCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    //        itemTable1.AddCell(amountCell);

                    //        PdfPCell totalamtCell = new PdfPCell(new Phrase(emp.Bonus.ToString(), bodyFont));
                    //        totalamtCell.HorizontalAlignment = 1;
                    //        totalamtCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    //        itemTable1.AddCell(totalamtCell);

                    //    }
                    //    // Table footer
                    //    // foreach (EmployeeSalary emp in _employeeSalaries)
                    //    {
                    //        PdfPCell totalAmtCell1 = new PdfPCell(new Phrase(""));
                    //        totalAmtCell1.Border = Rectangle.LEFT_BORDER | Rectangle.TOP_BORDER;
                    //        itemTable1.AddCell(totalAmtCell1);
                    //        PdfPCell totalAmtCell2 = new PdfPCell(new Phrase(""));
                    //        totalAmtCell2.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                    //        itemTable1.AddCell(totalAmtCell2);
                    //        PdfPCell totalAmtCell3 = new PdfPCell(new Phrase(""));
                    //        totalAmtCell3.Border = Rectangle.TOP_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                    //        itemTable1.AddCell(totalAmtCell3);
                    //        PdfPCell totalAmtStrCell = new PdfPCell(new Phrase(emp.AvailableDays.ToString(), boldTableFont));
                    //        totalAmtStrCell.Border = Rectangle.TOP_BORDER;   //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
                    //        totalAmtStrCell.HorizontalAlignment = 1;
                    //        itemTable1.AddCell(totalAmtStrCell);
                    //        PdfPCell totalAmtCell = new PdfPCell(new Phrase(emp.Total.ToString(), boldTableFont));
                    //        totalAmtCell.HorizontalAlignment = 1;
                    //        itemTable1.AddCell(totalAmtCell);
                    //    }
                    //    {
                    //        PdfPCell lcell = new PdfPCell(new Phrase("***NOTICE: A finance charge of 1.5% will be made on unpaid balances after 30 days. ***", bodyFont));
                    //        lcell.Colspan = 5;
                    //        lcell.HorizontalAlignment = 1;
                    //        itemTable1.AddCell(lcell);
                    //        document.Add(itemTable1);
                    //    }
                    //}
                    //#endregion
                    //#endregion

                    PdfContentByte cb = new PdfContentByte(writer);


                    BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
                    cb = new PdfContentByte(writer);
                    cb = writer.DirectContent;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, 8);
                    cb.SetTextMatrix(pageSize.GetLeft(120), 20);
                    cb.ShowText("Invoice was created on a computer and is valid without the signature and seal. ");
                    cb.EndText();

                    //Move the pointer and draw line to separate footer section from rest of page
                    cb.MoveTo(40, document.PageSize.GetBottom(50));
                    cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
                    cb.Stroke();
                }
                document.Close();
                output.Seek(0, SeekOrigin.Begin);
                return output;
                #region old

                //this.ReportHeader();
                ////  this.ReportBody();

                //document.Add(new Paragraph(" Hello World "));

                //document.Add(_pdfPTable);
                //document.Add(_pdfPCell);
                //document.Add(Maintable);

                //// document.Add(Add_Content_To_PDF(tableLayout));

                #endregion

            }
            #endregion

        }

        private void invoicebill()
        {
            //Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);
            //MemoryStream PDFData = new MemoryStream();
            //PdfWriter writer = PdfWriter.GetInstance(pdfDoc, PDFData);



        }
        private void ReportHeader()
        {
            EmployeeSalary ees = new EmployeeSalary();
            _font = FontFactory.GetFont("Tahoma", 11f, 1);

            _pdfPCell = new PdfPCell(new Phrase("Receipt", _font));
            //_pdfPCell = new PdfPCell(new Phrase(ees.EmployeeName));
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.Border = 0;
            _pdfPCell.BackgroundColor = BaseColor.BLUE;
            _pdfPCell.ExtraParagraphSpace = 0;
            _pdfPTable.AddCell(_pdfPCell);
            _font = FontFactory.GetFont("Tahoma", 11f, 1);
            _pdfPCell = new PdfPCell(new Phrase("EmployeeSalaryList", _font));
            _pdfPCell.Colspan = _totalColumn;
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPCell.Border = 0;
            _pdfPCell.BackgroundColor = BaseColor.BLUE;
            _pdfPCell.ExtraParagraphSpace = 0;
            _pdfPTable.AddCell(_pdfPCell);
            //table.setTableEvent(new BorderEvent());

            foreach (EmployeeSalary ee in _employeeSalaries)
            {
                Maintable.SetWidths(widths1);
                //  table.setWidths(new int[] { 1, 12, 8, 1 });
                Maintable.DefaultCell.Border = (Rectangle.NO_BORDER);

                // first row
                PdfPCell cell = new PdfPCell(new Phrase("Main table"));
                cell.Border = Rectangle.NO_BORDER;
                cell = new PdfPCell();
                cell.Colspan = 3;
                cell.BorderWidth = 0;
                cell.MinimumHeight = 16;
                // cell.Colspan = 4;
                Maintable.AddCell(cell);
                // second row
                PdfPTable table1 = new PdfPTable(2);
                float[] widths7 = new float[] { 6, 6 };
                table1.DefaultCell.Border = Rectangle.NO_BORDER;
                table1.SetWidths(widths7);
                table1.AddCell("Name");
                table1.AddCell(new Phrase(new Chunk(ee.EmployeeName.ToString())));

                table1.AddCell("Department");
                table1.AddCell(new Phrase(new Chunk(ee.Dapartment.ToString())));

                Maintable.AddCell(new PdfPCell(table1));

                PdfPTable table2 = new PdfPTable(2);

                float[] widths8 = new float[] { 6, 6 };
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                table2.SetWidths(widths8);
                table2.AddCell("Date");
                table2.AddCell(new Phrase(new Chunk(ee.Salary.ToString())));
                table2.AddCell("Place");
                table2.AddCell(new Phrase(new Chunk(ee.Rate.ToString())));


                table2.AddCell("Date");
                table2.AddCell(new Phrase(new Chunk((ee.PaidDays.ToString()))));
                //  table2.Colspan=(2);
                // table2.AddCell(cell1);
                // table.addCell(new PdfPCell(table2));


                //PdfPTable table2 = new PdfPTable(2);
                //table2.DefaultCell.Border = Rectangle.NO_BORDER;
                //float[] widths6 = new float[] { 3, 3 };
                //table2.SetWidths(widths6);
                //table2.AddCell("Salary");
                //table2.AddCell( new Phrase(new Chunk( ee.Salary.ToString())));
                //table2.AddCell("Rate");
                //table2.AddCell(new Phrase(new Chunk(ee.Rate_Days.ToString())));
                //table2.AddCell("PaidDays");
                //table2.AddCell(new Phrase(new Chunk((ee.PaidDays.ToString()))));
                Maintable.AddCell(new PdfPCell(table2));
                // third row cell 3

                float[] widths9 = new float[] { 6, 6, 6, 6, 6, 6 };
                table3.SetWidths(widths9);
                table3.DefaultCell.MinimumHeight = (50);
                table3.DefaultCell.BackgroundColor = BaseColor.BLUE;

                table3.DefaultCell.Border = Rectangle.NO_BORDER;
                table3.AddCell("Total");
                table3.AddCell(new Phrase(new Chunk((ee.Total.ToString()))));

                table3.AddCell("Bonus");
                table3.AddCell(new Phrase(new Chunk((ee.Bonus.ToString()))));
                table3.AddCell("Total");
                table3.AddCell(new Phrase(new Chunk((ee.Total.ToString()))));

                //table3.AddCell("Bonus");
                //table3.AddCell(new Phrase(new Chunk((ee.Bonus.ToString()))));

                //table3.AddCell("Total");
                //table3.AddCell(new Phrase(new Chunk((ee.Total.ToString()))));

                //table3.AddCell("Bonus");
                //table3.AddCell(new Phrase(new Chunk((ee.Bonus.ToString()))));
                //PdfPTable table3 = new PdfPTable(2);
                //   table3.DefaultCell.MinimumHeight=(50);

                //    table3.DefaultCell.Border = Rectangle.NO_BORDER;
                //    table3.AddCell("Total");
                //    table3.AddCell(new Phrase(new Chunk((ee.Total.ToString()))));

                //    table3.AddCell("Bonus");
                //    table3.AddCell(new Phrase(new Chunk((ee.Bonus.ToString()))));
                //    cell = new PdfPCell(new Phrase(ee.Bonus.ToString()));
                // cell.Colspan = 2;
                //table3.AddCell("");
                //table3.AddCell("");
                Maintable.AddCell(new PdfPCell(table3));
                // third row cell 4
                Maintable.AddCell("");

                // fourth row


                // Maintable.AddCell(cell);


            }
        }
        private void ReportBody()
        {
            EmployeeSalary es = new EmployeeSalary();
            #region ReportBody
            _pdfPCell = new PdfPCell(new Phrase("SerialNo"));
            _pdfPTable.AddCell(_pdfPCell);
            _pdfPCell = new PdfPCell(new Phrase("ID"));
            _pdfPCell = new PdfPCell(new Phrase(es.ID));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_LEFT;
            _pdfPTable.AddCell(_pdfPCell);
            _pdfPCell = new PdfPCell(new Phrase("EmployeeName"));
            _pdfPCell = new PdfPCell(new Phrase(es.EmployeeNameDes));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
            _pdfPTable.AddCell(_pdfPCell);
            _pdfPCell = new PdfPCell(new Phrase("Department"));
            _pdfPCell = new PdfPCell(new Phrase(es.Dapartment.ToString()));
            _pdfPCell.HorizontalAlignment = Element.ALIGN_BOTTOM;
            _pdfPTable.AddCell(_pdfPCell);
            //_pdfPCell = new PdfPCell(new Phrase("5"));
            //_pdfPTable.AddCell(_pdfPCell);
            _pdfPTable.CompleteRow();
            #region tablebody
            int serialno = 1;
            foreach (EmployeeSalary emp in _employeeSalaries)
            {
                _pdfPCell = new PdfPCell(new Phrase(serialno++.ToString()));
                _pdfPTable.AddCell(_pdfPCell);
                _pdfPCell = new PdfPCell(new Phrase(emp.ID));

                _pdfPTable.AddCell(_pdfPCell);
                _pdfPCell = new PdfPCell(new Phrase(emp.EmployeeNameDes));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_LEFT;
                _pdfPTable.AddCell(_pdfPCell);
                _pdfPCell = new PdfPCell(new Phrase(emp.Dapartment.ToString()));
                _pdfPCell.HorizontalAlignment = Element.ALIGN_LEFT;
                _pdfPTable.AddCell(_pdfPCell);
                //_pdfPCell = new PdfPCell(new Phrase(ex.EndTime.ToString()));
                //_pdfPTable.AddCell(_pdfPCell);
                _pdfPTable.CompleteRow();
            }


            #endregion
            #endregion


        }
        [HttpGet]
        [ActionName("GeneratePdf")]
        public HttpResponseMessage GetGeneratePdf()
        {
            #region

            var stream = CreatePdf(repository.Get());
            // var stream = CreatePdfById(repository.Get(id));

            return new HttpResponseMessage
            {
                Content = new StreamContent(stream)
                {
                    Headers =
            {
                ContentType = new MediaTypeHeaderValue("application/pdf"),
                ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "myfile.pdf"

                }
            }
                },
                StatusCode = HttpStatusCode.OK
            };
            #endregion
        }

        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpGet]
        [ActionName("List")]
        public IHttpActionResult GetList()
        {
            return Success(repository.Get());
        }

        [HttpPost]
        [ActionName("CreateSalary")]
        public IHttpActionResult PostCreateSalary(EmployeeSalary salary)
        {
            return Success(repository.CreateEmployeeSalary(salary));
        }

        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveEmployeeSalary(TypeCaster.TryConvertToLong(id)));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, EmployeeSalary salary)
        {
            salary.ID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateEmployeeSalary(salary);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
    }
}