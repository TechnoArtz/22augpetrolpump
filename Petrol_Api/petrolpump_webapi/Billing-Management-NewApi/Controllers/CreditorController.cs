using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class CreditorController:BaseController
    {
        ICreditorEntryRepository repository;
        public CreditorController()
        {
            repository = new CreditorEntryRepository();
        }
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Success(repository.Get());
        }
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpPost]
        public IHttpActionResult Post(CreditorEntryMaster creditorentrymaster)
        {
            return Success(repository.Createcreditorentry(creditorentrymaster));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, CreditorEntryMaster creditorentrymaster)
        {
            creditorentrymaster.ID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateCreditorEntry(creditorentrymaster);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
    }
}