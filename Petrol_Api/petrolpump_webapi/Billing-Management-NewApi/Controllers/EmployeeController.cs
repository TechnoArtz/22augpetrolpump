﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
namespace Billing_Management_NewApi.Controllers
{
    public class EmployeeController : BaseController
    {
        IEmployeeRepository repository;
        public EmployeeController()
        {
            repository = new EmployeeRepository();
        }
        /// <summary>
        /// Returns list of employees
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("List")]
        public IHttpActionResult GetList()
        {
            return Success(repository.Get());
        }
        [HttpGet]
        [ActionName("UserRole")]
        public IHttpActionResult GetUserRole()
        {
            return Success(repository.GetUserRole());
        }
        [HttpGet]
        [ActionName("ListByID")]
        public IHttpActionResult GetListByID(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpPost]
        public IHttpActionResult Post(Employee employee)
        {
            return Success(repository.CreateEmployee(employee));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, Employee employee)
        {
            employee.ID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateEmployee(employee);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveEmployee(TypeCaster.TryConvertToLong(id)));
        }


        /// <summary>
        /// Image File Upload 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage UploadJsonFile()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    var filePath = HttpContext.Current.Server.MapPath("~/UploadFile/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);
                }
            }
            return response;
        }
    }
}