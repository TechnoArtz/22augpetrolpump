using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class UserMasterController : BaseController
    {
        IUserMasterRepository repository;
        public UserMasterController()
        {
            repository = new UserMasterRepository();
        }
        /// <summary>
        /// Returns list of UserMaster
        /// </summary>
        /// <returns></returns>
         [HttpGet]
        public IHttpActionResult Get()
        {
            return Success(repository.Get());
        }
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpPost]
        public IHttpActionResult Post(UserMaster usermaster)
        {
            return Success(repository.CreateUserMaster(usermaster));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, UserMaster usermaster)
        {
            usermaster.UID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateUserMaster(usermaster);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveUserMaster (TypeCaster.TryConvertToLong(id)));
        }
    }
}