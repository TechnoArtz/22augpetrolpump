using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class AddCreditorController:BaseController
    {
         IAddCreditorRepository repository;
         public AddCreditorController()
        {
            repository = new AddCreditorRepository();
        }
        /// <summary>
        /// Return list of Addcreditor
        /// </summary>
        /// <returns></returns>
         [HttpGet]
         public IHttpActionResult Get()
         {
             return Success(repository.Get());
         }
        /// <summary>
        /// Return List of Creditors by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
         [HttpGet]
         public IHttpActionResult Get(string id)
         {
             var result = repository.Get(TypeCaster.TryConvertToLong(id));
             if (result != null)
                 return Success(result);
             else
                 return Error("No record found");
         }
         [HttpPost]
         public IHttpActionResult Post(AddCreditor addcreditor)
         {
             return Success(repository.CreateAddCreditor(addcreditor));
         }
         [HttpPut]
         public IHttpActionResult Put([FromUri]string id, AddCreditor addcreditor)
         {
             addcreditor.ID = TypeCaster.TryConvertToLong(id);
             var result = repository.UpdateAddCreditor(addcreditor);
             if (result)
                 return Success(result);
             else
                 return Error("Failed to update");
         }
         [HttpDelete]
         public IHttpActionResult Delete([FromUri]string id)
         {
             return Success(repository.RemoveAddCreditor(TypeCaster.TryConvertToLong(id)));
         }
    }
}