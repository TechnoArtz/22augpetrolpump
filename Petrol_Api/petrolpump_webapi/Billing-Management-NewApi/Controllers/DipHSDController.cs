﻿using Billing_Management_NewApi.Models;
using Billing_Management_NewApi.Repositories;
using Billing_Management_NewApi.Repositories.IRepositories;
using Billing_Management_NewApi.Repositories.Repositories;
using CommonUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billing_Management_NewApi.Controllers
{
    public class DipHSDController: BaseController
    {

        GSDBSExtended entity;
        IDipHSDRepository repository;
        public DipHSDController()
        {
            repository = new DipHSDRepository();
            entity = new GSDBSExtended();
        }
        /// <summary>
        /// Returns list of NozzleMaster
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Success(repository.Get());
        }
        //[HttpGet]
        //public IHttpActionResult GetType(string Type)
        //{
        //    var result = repository.GetType(Type);
        //    if (result != null)
        //        return Success(result);
        //    else
        //        return Error("No record found");
        //}
        [HttpGet]
        [Route("api/DipHSD/GetType/{id}")]
        public IHttpActionResult GetType(string id)
        {
            var type = repository.Get(TypeCaster.TryConvertToLong(id));
            if (type != null)
                return Success(type);
            else
                return Error("No record found");
        }

        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            var result = repository.Get(TypeCaster.TryConvertToLong(id));
            if (result != null)
                return Success(result);
            else
                return Error("No record found");
        }
        [HttpPost]
        public IHttpActionResult Post(DipHSD diphsd)
        {
            return Success(repository.CreateDipHSD(diphsd));
        }
        [HttpPut]
        public IHttpActionResult Put([FromUri]string id, DipHSD diphsd)
        {
            diphsd.ID = TypeCaster.TryConvertToLong(id);
            var result = repository.UpdateDipHSD(diphsd);
            if (result)
                return Success(result);
            else
                return Error("Failed to update");
        }
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string id)
        {
            return Success(repository.RemoveDipHSD(TypeCaster.TryConvertToLong(id)));
        }

    }
}