USE [GSDBS]
GO
/****** Object:  StoredProcedure [dbo].[STPShiftAllocationsearch]    Script Date: 8/22/2018 11:15:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[STPShiftAllocationsearch] 
	-- Add the parameters for the stored procedure here
@Category varchar(1000),
@fromDate Date,
@toDate Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sa.Note,
	sa.ID AS ShiftID,
	sd.ID AS AllocationID,
	sd.Note AS AllocationNote,
	sd.Nozzel,
	sd.Opening,
	sd.PumpTesting,
	NM.Type AS NozzelText
	FROM tbl_ShiftAllocationDetails sd  
	INNER JOIN	tbl_ShiftAllocation sa
	ON sd.ShiftAllocationMasterID=sa.ID
	INNER JOIN tbl_NozzleMaster NM
	ON NM.Id=sd.Nozzel
	WHERE sa.Date BETWEEN @fromDate and @toDate AND
	1=CASE WHEN @Category='' THEN 1 
		   WHEN (sa.Note LIKE @Category OR sa.Note LIKE @Category or sd.Nozzel like @Category  OR sd.Tank LIKE @Category )  THEN 1
		   ELSE 0 
		   END --AND
	--1=CASE WHEN @Category='' THEN 1 
	--	   WHEN sa.EmployeeName LIKE @Category THEN 1
	--	   ELSE 0 
	--	   END
END

GO
/****** Object:  Table [dbo].[tbl_AddCreditor]    Script Date: 8/22/2018 11:15:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AddCreditor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreditorName] [varchar](50) NULL,
	[MobileNo] [varchar](12) NULL,
	[Address] [varchar](100) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_AddCreditor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_BankAccMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BankAccMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BankName] [varchar](50) NULL,
	[AccHolderName] [varchar](50) NULL,
	[AccountNo] [bigint] NULL,
	[Branch] [varchar](50) NULL,
	[IFSCCode] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_BankAccMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_BankCreditEntry]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BankCreditEntry](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[Shift] [bigint] NULL,
	[AccountName] [varchar](50) NULL,
	[EmployeeName] [bigint] NULL,
	[PaymentMode] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[BankAccNo] [bigint] NULL,
	[Amount] [bigint] NULL,
	[Note] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_BankCreditEntry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_BankDepositEntry]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BankDepositEntry](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[BankName] [varchar](50) NULL,
	[BankAccNo] [bigint] NULL,
	[Amount] [bigint] NULL,
	[Note] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_BankDepositEntry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Billing]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Billing](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BillNo] [bigint] NULL,
	[Party] [varchar](50) NULL,
	[VehicleNo] [varchar](50) NULL,
	[ItemType] [bigint] NULL,
	[Qty] [int] NULL,
	[Rate] [bigint] NULL,
	[Total] [float] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Billing] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_CreaditorEntryDetails]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CreaditorEntryDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreditorEntryMasterId] [bigint] NULL,
	[CreditorName] [bigint] NULL,
	[Billno] [bigint] NULL,
	[BillDate] [date] NULL,
	[VehicleNo] [varchar](50) NULL,
	[ItemType] [varchar](50) NOT NULL,
	[Qty] [int] NULL,
	[Unit] [varchar](50) NULL,
	[Rate] [float] NULL,
	[Amount] [float] NULL,
	[TotalQty] [float] NULL,
	[TotalAmount] [float] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CreaditorEntryDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_CreditorEntryMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CreditorEntryMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreditorMasterId] [bigint] NULL,
	[CreditorName] [varchar](50) NULL,
	[Billno] [bigint] NULL,
	[BillDate] [date] NULL,
	[VehicleNo] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_tbl_CreditorEntryMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_CreditorMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CreditorMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreditorName] [varchar](50) NULL,
	[EmailId] [varchar](100) NULL,
	[MobileNo1] [varchar](12) NULL,
	[MobileNo2] [varchar](12) NULL,
	[Address] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[ZipCode] [int] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CreditorMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_CustomerMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CustomerMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerName] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Address] [varchar](100) NULL,
	[MobileNumber1] [varchar](12) NULL,
	[MobileNumber2] [varchar](12) NULL,
	[City] [varchar](50) NULL,
	[ZipCode] [int] NULL,
	[CreateBy] [bigint] NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CustomerMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Debitor]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Debitor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Billno] [bigint] NULL,
	[CustomerName] [bigint] NULL,
	[PreviousAmt] [float] NULL,
	[PaidAmt] [float] NULL,
	[TotalAmt] [float] NULL,
	[Status] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Debitor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_DepartmentMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DepartmentMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Department] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_DipHSD]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_DipHSD](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[CurrentDip] [float] NULL,
	[ActualDip] [float] NULL,
	[Variation] [float] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_DipHSD] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_DipMS]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_DipMS](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[CurrentDip] [float] NULL,
	[ActualDip] [float] NULL,
	[Variation] [float] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_DipMS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_EmployeeRegister]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EmployeeRegister](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [varchar](50) NULL,
	[Photo] [image] NULL,
	[UserRole] [bigint] NULL,
	[MobileNo] [varchar](12) NULL,
	[Gender] [varchar](10) NULL,
	[DOB] [date] NULL,
	[Address] [varchar](200) NULL,
	[JoiningDate] [date] NULL,
	[Salary] [float] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_EmployeeRegister] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_EmployeeSalary]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_EmployeeSalary](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [bigint] NULL,
	[Dapartment] [bigint] NULL,
	[AvailableDays] [int] NULL,
	[PaidDays] [int] NULL,
	[Rate] [float] NULL,
	[Salary] [float] NULL,
	[Bonus] [float] NULL,
	[Total] [float] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_EmployeeSalary1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Expenses]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Expenses](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[Category] [varchar](50) NULL,
	[PaymentMode] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[ChequeNo] [varchar](100) NULL,
	[Amount] [float] NULL,
	[Note] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Expenses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_NozzleMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NozzleMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_NozzelMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_ProductSaleDetails]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProductSaleDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductSaleMasterID] [bigint] NULL,
	[PartyName] [bigint] NULL,
	[ShiftType] [bigint] NULL,
	[TankType] [bigint] NULL,
	[NozzelNo] [bigint] NULL,
	[ItemType] [varchar](50) NULL,
	[Qty] [int] NULL,
	[Rate] [float] NULL,
	[Total] [float] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_ProductSaleDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_ProductSaleMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProductSaleMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PartyName] [bigint] NULL,
	[ShiftType] [bigint] NULL,
	[TankType] [bigint] NULL,
	[NozzelNo] [bigint] NULL,
	[Notes] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_ProductSaleMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_ReceiveItem]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ReceiveItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[TankType] [bigint] NOT NULL,
	[Qty] [float] NULL,
	[Createdby] [bigint] NULL,
	[Createddate] [datetime] NULL,
	[Modifiedby] [bigint] NULL,
	[Modifieddate] [datetime] NULL,
 CONSTRAINT [PK_tbl_ReceiveItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ShiftAllocation]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ShiftAllocation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[ShiftType] [bigint] NULL,
	[Employee_ID] [bigint] NULL,
	[EmployeeName] [bigint] NULL,
	[Note] [nchar](10) NULL,
 CONSTRAINT [PK_tbl_ShiftAllocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ShiftAllocationDetails]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ShiftAllocationDetails](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ShiftAllocationMasterID] [bigint] NULL,
	[Employee_ID] [bigint] NULL,
	[EmployeeName] [varchar](50) NULL,
	[Date] [datetime] NULL,
	[Shift] [varchar](50) NULL,
	[Note] [varchar](50) NULL,
	[Tank] [bigint] NULL,
	[Nozzel] [bigint] NULL,
	[Opening] [float] NULL,
	[Closing] [float] NULL,
	[TotalSale] [float] NULL,
	[Rate] [float] NULL,
	[TotalAmt] [float] NULL,
	[ByCash] [float] NULL,
	[CreditCardCash] [float] NULL,
	[DebitCardCash] [float] NULL,
	[GrandTotalSale] [float] NULL,
	[GrandTotalAmount] [float] NULL,
	[PumpTesting] [float] NULL,
 CONSTRAINT [PK_tbl_ShiftAllocationDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_ShiftEntry]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ShiftEntry](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[ShiftType] [bigint] NULL,
	[EmployeeName] [bigint] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_ShiftEntry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_ShiftMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ShiftMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[StartTime] [time](7) NULL,
	[EndTime] [time](7) NULL,
 CONSTRAINT [PK_ShiftMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_SupplierMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_SupplierMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[CompanyName] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[MobileNo1] [varchar](12) NULL,
	[MobileNo2] [varchar](12) NULL,
	[Address] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[ZipCode] [int] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_SupplierMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_TankMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TankMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
	[TankRate] [float] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_TankMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_TempShiftAllocationDetails]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_TempShiftAllocationDetails](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Tank] [nchar](10) NULL,
	[Nozzel] [nchar](10) NULL,
	[Opening] [float] NULL,
	[Closing] [float] NULL,
	[TotalSale] [float] NULL,
	[Rate] [float] NULL,
	[TotalAmt] [float] NULL,
	[ByCash] [float] NULL,
	[CreditCardCash] [float] NULL,
	[DebitCardCash] [float] NULL,
	[GrandTotalSale] [float] NULL,
	[GrandTotalAmt] [float] NULL,
 CONSTRAINT [PK_tbl_TempShiftAllocationDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_UnitMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_UnitMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_UnitMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_UserMaster]    Script Date: 8/22/2018 11:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_UserMaster](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[ConfirmPassword] [varchar](50) NULL,
	[UserRole] [varchar](50) NULL,
	[EmailID] [varchar](50) NULL,
	[MobileNo] [varchar](50) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_UserMaster] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbl_BankCreditEntry]  WITH CHECK ADD  CONSTRAINT [FK_tbl_BankCreditEntry_tbl_EmployeeRegister] FOREIGN KEY([EmployeeName])
REFERENCES [dbo].[tbl_EmployeeRegister] ([Id])
GO
ALTER TABLE [dbo].[tbl_BankCreditEntry] CHECK CONSTRAINT [FK_tbl_BankCreditEntry_tbl_EmployeeRegister]
GO
ALTER TABLE [dbo].[tbl_BankCreditEntry]  WITH CHECK ADD  CONSTRAINT [FK_tbl_BankCreditEntry_tbl_ShiftMaster] FOREIGN KEY([Shift])
REFERENCES [dbo].[tbl_ShiftMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_BankCreditEntry] CHECK CONSTRAINT [FK_tbl_BankCreditEntry_tbl_ShiftMaster]
GO
ALTER TABLE [dbo].[tbl_Billing]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Billing_tbl_TankMaster] FOREIGN KEY([ItemType])
REFERENCES [dbo].[tbl_TankMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_Billing] CHECK CONSTRAINT [FK_tbl_Billing_tbl_TankMaster]
GO
ALTER TABLE [dbo].[tbl_CreaditorEntryDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CreaditorEntryDetails_tbl_AddCreditor] FOREIGN KEY([CreditorName])
REFERENCES [dbo].[tbl_AddCreditor] ([Id])
GO
ALTER TABLE [dbo].[tbl_CreaditorEntryDetails] CHECK CONSTRAINT [FK_tbl_CreaditorEntryDetails_tbl_AddCreditor]
GO
ALTER TABLE [dbo].[tbl_CreaditorEntryDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CreaditorEntryDetails_tbl_CreditorEntryMaster] FOREIGN KEY([CreditorEntryMasterId])
REFERENCES [dbo].[tbl_CreditorEntryMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_CreaditorEntryDetails] CHECK CONSTRAINT [FK_tbl_CreaditorEntryDetails_tbl_CreditorEntryMaster]
GO
ALTER TABLE [dbo].[tbl_CreditorEntryMaster]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CreditorEntryMaster_tbl_CreditorEntryMaster] FOREIGN KEY([CreditorMasterId])
REFERENCES [dbo].[tbl_CreditorMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_CreditorEntryMaster] CHECK CONSTRAINT [FK_tbl_CreditorEntryMaster_tbl_CreditorEntryMaster]
GO
ALTER TABLE [dbo].[tbl_Debitor]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Debitor_tbl_CustomerMaster] FOREIGN KEY([CustomerName])
REFERENCES [dbo].[tbl_CustomerMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_Debitor] CHECK CONSTRAINT [FK_tbl_Debitor_tbl_CustomerMaster]
GO
ALTER TABLE [dbo].[tbl_EmployeeRegister]  WITH CHECK ADD  CONSTRAINT [FK_tbl_EmployeeRegister_tbl_DepartmentMaster] FOREIGN KEY([UserRole])
REFERENCES [dbo].[tbl_DepartmentMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_EmployeeRegister] CHECK CONSTRAINT [FK_tbl_EmployeeRegister_tbl_DepartmentMaster]
GO
ALTER TABLE [dbo].[tbl_EmployeeSalary]  WITH CHECK ADD  CONSTRAINT [FK_tbl_EmployeeSalary_tbl_DepartmentMaster] FOREIGN KEY([Dapartment])
REFERENCES [dbo].[tbl_DepartmentMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_EmployeeSalary] CHECK CONSTRAINT [FK_tbl_EmployeeSalary_tbl_DepartmentMaster]
GO
ALTER TABLE [dbo].[tbl_EmployeeSalary]  WITH CHECK ADD  CONSTRAINT [FK_tbl_EmployeeSalary_tbl_EmployeeRegister] FOREIGN KEY([EmployeeName])
REFERENCES [dbo].[tbl_EmployeeRegister] ([Id])
GO
ALTER TABLE [dbo].[tbl_EmployeeSalary] CHECK CONSTRAINT [FK_tbl_EmployeeSalary_tbl_EmployeeRegister]
GO
ALTER TABLE [dbo].[tbl_ProductSaleDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProductSaleDetails_tbl_NozzleMaster] FOREIGN KEY([NozzelNo])
REFERENCES [dbo].[tbl_NozzleMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ProductSaleDetails] CHECK CONSTRAINT [FK_tbl_ProductSaleDetails_tbl_NozzleMaster]
GO
ALTER TABLE [dbo].[tbl_ProductSaleDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProductSaleDetails_tbl_ProductSaleMaster] FOREIGN KEY([ProductSaleMasterID])
REFERENCES [dbo].[tbl_ProductSaleMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ProductSaleDetails] CHECK CONSTRAINT [FK_tbl_ProductSaleDetails_tbl_ProductSaleMaster]
GO
ALTER TABLE [dbo].[tbl_ProductSaleDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProductSaleDetails_tbl_ShiftMaster] FOREIGN KEY([ShiftType])
REFERENCES [dbo].[tbl_ShiftMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ProductSaleDetails] CHECK CONSTRAINT [FK_tbl_ProductSaleDetails_tbl_ShiftMaster]
GO
ALTER TABLE [dbo].[tbl_ProductSaleDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProductSaleDetails_tbl_TankMaster] FOREIGN KEY([TankType])
REFERENCES [dbo].[tbl_TankMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ProductSaleDetails] CHECK CONSTRAINT [FK_tbl_ProductSaleDetails_tbl_TankMaster]
GO
ALTER TABLE [dbo].[tbl_ProductSaleMaster]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProductSaleMaster_tbl_EmployeeRegister] FOREIGN KEY([PartyName])
REFERENCES [dbo].[tbl_EmployeeRegister] ([Id])
GO
ALTER TABLE [dbo].[tbl_ProductSaleMaster] CHECK CONSTRAINT [FK_tbl_ProductSaleMaster_tbl_EmployeeRegister]
GO
ALTER TABLE [dbo].[tbl_ProductSaleMaster]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProductSaleMaster_tbl_NozzleMaster] FOREIGN KEY([NozzelNo])
REFERENCES [dbo].[tbl_NozzleMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ProductSaleMaster] CHECK CONSTRAINT [FK_tbl_ProductSaleMaster_tbl_NozzleMaster]
GO
ALTER TABLE [dbo].[tbl_ProductSaleMaster]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProductSaleMaster_tbl_ShiftMaster] FOREIGN KEY([ShiftType])
REFERENCES [dbo].[tbl_ShiftMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ProductSaleMaster] CHECK CONSTRAINT [FK_tbl_ProductSaleMaster_tbl_ShiftMaster]
GO
ALTER TABLE [dbo].[tbl_ProductSaleMaster]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProductSaleMaster_tbl_TankMaster] FOREIGN KEY([TankType])
REFERENCES [dbo].[tbl_TankMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ProductSaleMaster] CHECK CONSTRAINT [FK_tbl_ProductSaleMaster_tbl_TankMaster]
GO
ALTER TABLE [dbo].[tbl_ReceiveItem]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ReceiveItem_tbl_TankMaster] FOREIGN KEY([TankType])
REFERENCES [dbo].[tbl_TankMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ReceiveItem] CHECK CONSTRAINT [FK_tbl_ReceiveItem_tbl_TankMaster]
GO
ALTER TABLE [dbo].[tbl_ShiftAllocation]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ShiftAllocation_tbl_EmployeeRegister] FOREIGN KEY([EmployeeName])
REFERENCES [dbo].[tbl_EmployeeRegister] ([Id])
GO
ALTER TABLE [dbo].[tbl_ShiftAllocation] CHECK CONSTRAINT [FK_tbl_ShiftAllocation_tbl_EmployeeRegister]
GO
ALTER TABLE [dbo].[tbl_ShiftAllocation]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ShiftAllocation_tbl_ShiftMaster] FOREIGN KEY([ShiftType])
REFERENCES [dbo].[tbl_ShiftMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ShiftAllocation] CHECK CONSTRAINT [FK_tbl_ShiftAllocation_tbl_ShiftMaster]
GO
ALTER TABLE [dbo].[tbl_ShiftAllocationDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ShiftAllocationDetails_tbl_NozzleMaster] FOREIGN KEY([Nozzel])
REFERENCES [dbo].[tbl_NozzleMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ShiftAllocationDetails] CHECK CONSTRAINT [FK_tbl_ShiftAllocationDetails_tbl_NozzleMaster]
GO
ALTER TABLE [dbo].[tbl_ShiftAllocationDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ShiftAllocationDetails_tbl_ShiftAllocation] FOREIGN KEY([ShiftAllocationMasterID])
REFERENCES [dbo].[tbl_ShiftAllocation] ([Id])
GO
ALTER TABLE [dbo].[tbl_ShiftAllocationDetails] CHECK CONSTRAINT [FK_tbl_ShiftAllocationDetails_tbl_ShiftAllocation]
GO
ALTER TABLE [dbo].[tbl_ShiftAllocationDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ShiftAllocationDetails_tbl_TankMaster] FOREIGN KEY([Tank])
REFERENCES [dbo].[tbl_TankMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ShiftAllocationDetails] CHECK CONSTRAINT [FK_tbl_ShiftAllocationDetails_tbl_TankMaster]
GO
ALTER TABLE [dbo].[tbl_ShiftEntry]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ShiftEntry_tbl_EmployeeRegister] FOREIGN KEY([EmployeeName])
REFERENCES [dbo].[tbl_EmployeeRegister] ([Id])
GO
ALTER TABLE [dbo].[tbl_ShiftEntry] CHECK CONSTRAINT [FK_tbl_ShiftEntry_tbl_EmployeeRegister]
GO
ALTER TABLE [dbo].[tbl_ShiftEntry]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ShiftEntry_tbl_ShiftMaster] FOREIGN KEY([ShiftType])
REFERENCES [dbo].[tbl_ShiftMaster] ([Id])
GO
ALTER TABLE [dbo].[tbl_ShiftEntry] CHECK CONSTRAINT [FK_tbl_ShiftEntry_tbl_ShiftMaster]
GO
