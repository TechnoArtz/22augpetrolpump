import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { EmployeesalaryComponent } from './employeesalary.component';
import { EmployeesalaryRoutingModule } from './employeesalary.routing';


@NgModule({
    declarations: [
        EmployeesalaryComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        EmployeesalaryRoutingModule
    ],
  
})
export class EmployeesalaryModule { }

