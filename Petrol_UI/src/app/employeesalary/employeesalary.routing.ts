
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { EmployeesalaryComponent } from './employeesalary.component';



const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'layout' },
    {
        path: '', component: EmployeesalaryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmployeesalaryRoutingModule { }
