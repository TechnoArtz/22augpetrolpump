import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftmanagementComponent } from './shiftmanagement.component';

describe('ShiftmanagementComponent', () => {
  let component: ShiftmanagementComponent;
  let fixture: ComponentFixture<ShiftmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
