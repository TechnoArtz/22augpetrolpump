import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
@Component({
  selector: 'app-shiftmanagement',
  templateUrl: './shiftmanagement.component.html',
  styleUrls: ['./shiftmanagement.component.css']
})
export class ShiftmanagementComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  moveemployee(){
    this.router.navigateByUrl("/layout/employee-list");

  }
}
