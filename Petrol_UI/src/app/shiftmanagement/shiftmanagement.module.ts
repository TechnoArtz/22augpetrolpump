import { NgModule } from '@angular/core';


import { ShiftmanagementComponent } from './shiftmanagement.component';
import { ShiftmanagementRoutingModule } from './shiftmanagement.routing';


@NgModule({
    declarations: [
        ShiftmanagementComponent
    ],
    imports: [
        ShiftmanagementRoutingModule
    ],
})
export class ShiftmanagementModule { }
