import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShiftmanagementComponent } from './shiftmanagement.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'layout' },
    {
        path: '', component: ShiftmanagementComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShiftmanagementRoutingModule { }
