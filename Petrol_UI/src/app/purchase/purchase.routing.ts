import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchaseComponent } from './purchase.component';
// import { ShiftmanagementComponent } from './shiftmanagement.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'layout' },
    {
        path: '', component: PurchaseComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PurchaseRoutingModule { }
