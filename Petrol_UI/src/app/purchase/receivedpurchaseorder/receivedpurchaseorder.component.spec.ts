import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivedpurchaseorderComponent } from './receivedpurchaseorder.component';

describe('ReceivedpurchaseorderComponent', () => {
  let component: ReceivedpurchaseorderComponent;
  let fixture: ComponentFixture<ReceivedpurchaseorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceivedpurchaseorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivedpurchaseorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
