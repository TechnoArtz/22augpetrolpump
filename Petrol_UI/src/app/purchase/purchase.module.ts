import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchaseRoutingModule } from './purchase-routing.module';
import { PurchaseorderComponent } from './purchaseorder/purchaseorder.component';
import { ReceivedpurchaseorderComponent } from './receivedpurchaseorder/receivedpurchaseorder.component';

@NgModule({
  imports: [
    CommonModule,
    PurchaseRoutingModule
  ],
  declarations: [PurchaseorderComponent, ReceivedpurchaseorderComponent]
})
export class PurchaseModule { }
