import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchaseorderComponent } from './purchaseorder/purchaseorder.component';
import { ReceivedpurchaseorderComponent } from './receivedpurchaseorder/receivedpurchaseorder.component';

const routes: Routes = [
  {
    path: '', children: [
      {
        path: 'purchase-order',
        component: PurchaseorderComponent
      },
      {
        path: 'received-purchase',
        component: ReceivedpurchaseorderComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseRoutingModule { }
