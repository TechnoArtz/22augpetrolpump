import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './user.service';
import { User } from './User';
import { ROWSIZE, PAGESTOSHOW } from '../Common/Constant';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user.component.css']
})
export class UserListComponent {
  userList: User[];
  errMessage: string = "";
  showError: boolean = false;
  private page: number = 1;
  private rowSize: number;
  private PagesToShow: number;
  private pageCount: number = 10;
  private loading: boolean = false;
  constructor(private userService: UserService, private router: Router) {
    this.loadUser(this.page);
    this.rowSize = ROWSIZE;
    this.PagesToShow = PAGESTOSHOW;
  };
  loadUser(page: number) {
    this.userService.GetUsers().subscribe(res => {
      this.userList = res;
      this.pageCount = 10;
    }, err => {
      this.showError = false;
      this.errMessage = err.error.Message;
    });
  }
  CreateUser() {
    this.userService.user = {};
    this.userService.UserID = "-1";
    this.router.navigateByUrl("/layout/userlist/user");
  }
  UpdateUser(user: User) {
    this.userService.user = user;
    this.userService.UserID = user.EncUserID;
    this.router.navigateByUrl("/layout/userlist/user");
  }
  RemoveUser(UserID: string) {
    this.userService.RemoveUser(UserID).subscribe(res => {
      this.loadUser(this.page);
    }, error => {
      this.showError = true;
      this.errMessage = error.error.Message;
    })
  }
  goToPage(n: any): void {
    this.page = n;
    this.loadUser(this.page);
  }

  nextPage(): void {
    this.page++;
    this.loadUser(this.page);
  }

  prevPage(): void {
    this.page--;
    this.loadUser(this.page);
  }
}
