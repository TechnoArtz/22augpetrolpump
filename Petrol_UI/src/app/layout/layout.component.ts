import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  public logo = "./assets/img/Final Petrol Logo.png";
  public user1 = "./assets/img/icons8-windows-client-402.png";
  public employee = "./assets/img/Employee.png";
  public expenses = "./assets/img/Expenses.png";
  public employeesalary = "./assets/img/Employee Salary.png";
  public report = "./assets/img/Report.png";
  public purchase = "./assets/img/icons8-purchase-order-40.png";
  public billing = "./assets/img/icons8-billing-40.png";
  public accounting = "./assets/img/Accounting.png";
  public creditor = "./assets/img/Creditor.png";
  public inventory = "./assets/img/Inventory.png";
  // shift: string ="./assets/img/Shift.png";
  public shiftimg = "./assets/img/Shift.png";

  public disabled = false;
  public status: { isopen: boolean } = { isopen: false };

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }
  constructor() { }

  ngOnInit() {
  }

}
