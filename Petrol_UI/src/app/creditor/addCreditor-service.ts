import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { APIConstant } from '../Common/AppConstant';
import { AddCreditor } from './addCreditor';
@Injectable()
export class AddCreditorService {
    constructor(private httpclient: HttpClient) {
    }
    CreateCreditor(addcreditor: AddCreditor): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "AddCreditor", addcreditor);
    }
    UpdateCreditor(addcreditor: AddCreditor): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "AddCreditor?id=" + addcreditor.ID, addcreditor);
    }
    DeleteCreditor(ID: string): Observable<AddCreditor> {
        return this.httpclient.delete<AddCreditor>(APIConstant + "AddCreditor?id=" + ID);
    }
    GetList(): Observable<Array<AddCreditor>> {
        return this.httpclient.get<Array<AddCreditor>>(APIConstant + "AddCreditor");
    }
    Get(ID: string): Observable<AddCreditor> {
        return this.httpclient.get<AddCreditor>(APIConstant + "AddCreditor?id=" + ID);
    }
}