import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreditorRoutingModule } from './creditor-routing.module';
import { EntrycreditorComponent } from './entrycreditor/entrycreditor.component';
import { AddcreditorComponent } from './addcreditor/addcreditor.component';
import { DebitorComponent } from './debitor/debitor.component';
import { AddCreditorService } from './addCreditor-service';
import { FormsModule } from '../../../node_modules/@angular/forms';
import { BsDatepickerModule } from '../../../node_modules/ngx-bootstrap/datepicker';
import { CustomerService } from '../crm/addCustomer-service';
import { AddDebitorrService } from './debitorservice';
import { CreditorEntryService } from './entrycreditor-service';
import { PagerService } from '../page/page.service';
import { UnitService } from '../shift/ShiftMaster-Service';

@NgModule({
  imports: [
    CommonModule,
    CreditorRoutingModule,
    FormsModule,
    BsDatepickerModule
  ],
  declarations: [EntrycreditorComponent, AddcreditorComponent, DebitorComponent],
  providers:[AddCreditorService,UnitService,CustomerService,AddDebitorrService,CreditorEntryService,PagerService]
})
export class CreditorModule { }
