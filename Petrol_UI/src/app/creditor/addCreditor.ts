export interface AddCreditor
{
   ID?:string;
   CreditorName?:string;
   MobileNo?:string;
   Address?:string;
}