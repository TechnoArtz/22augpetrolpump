import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';
import { Debitor } from './Debitor';



@Injectable()
export class AddDebitorrService {
   constructor(private httpclient: HttpClient) {
   }
   CreateDebitor(debitor: Debitor): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "Debitor", debitor);
   }
   UpdateDebitor(debitor: Debitor): Observable<boolean> {
       return this.httpclient.put<boolean>(APIConstant + "Debitor?id=" + debitor.ID, debitor);
   }
   DeleteDebitor(ID: string): Observable<Debitor> {
       return this.httpclient.delete<Debitor>(APIConstant + "Debitor?id=" + ID);
   }
   GetList(): Observable<Array<Debitor>> {
       return this.httpclient.get<Array<Debitor>>(APIConstant + "Debitor");
   }
   Get(ID: string): Observable<Debitor> {
       return this.httpclient.get<Debitor>(APIConstant + "Debitor?id=" + ID);
   }
}