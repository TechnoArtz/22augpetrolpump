export interface CreditorEntryMaster{
    ID?:string;
    CreditorName?:string;
    Billno?:string;
    BillDate?:any;
    VehicleNo?:string;
    lstcreditordetails?:Array<CreditorEntryDetails>;
 
 }
 export interface CreditorEntryDetails{
    ID?:string;
    CreditorEntryMasterId?:string;
    CreditorName?:string;
    CreditorNameDes?:string;
    Billno?:string;
    BillDate?:any;
    VehicleNo?:string;
    ItemType?:string;
    Qty?:number;
    Unit?:string;
    Rate?:number;
    Amount?:number;
    TotalQty?:number;
    TotalAmount?:number;
 }