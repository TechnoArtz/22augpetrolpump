// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-debitor',
//   templateUrl: './debitor.component.html',
//   styleUrls: ['./debitor.component.css']
// })
// export class DebitorComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AddDebitorrService } from '../debitorservice';
import { DateTimeFormatter } from '../../Common/DateFormatter';
import { Debitor } from '../Debitor';
import { CustomerService } from '../../crm/addCustomer-service';
import { Customer } from '../../crm/addCustomer';
import { PagerService } from '../../page/page.service';
@Component({
  selector: 'app-debitor',
  templateUrl: './debitor.component.html',
  styleUrls: ['./debitor.component.css']
})
export class DebitorComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  pager: any = {};
  pagedItems: any[];
  page:number=1;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  debitorOBJ: Debitor = new Object();
  debitorList: Array<Debitor>;
  errorMessage: string = "";
  showError: boolean = false;
  customer:Customer[];
  constructor(private pagerService: PagerService,private debitorservice:AddDebitorrService,private customerservice:CustomerService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter) { 
    this.customerservice.GetList().subscribe( result=>{
      this.customer=result;
    },error=>{
      this.customer=null;
    });
  }
  ngOnInit() {
  }
  
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id == undefined) {
      this.isUpdate = false;
      this.debitorOBJ = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg',keyboard:false,
      backdrop: false,
      ignoreBackdropClick: true });
    }
    else {
    debugger;
      this.isUpdate = true;
      this.ShowLoader = true;
      this.debitorservice.Get(Id).subscribe(result => {
        this.debitorOBJ = result;
        this.debitorOBJ.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg',keyboard:false,
        backdrop: false,
        ignoreBackdropClick: true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
     this.ShowLoader = true;
     debugger;
     this.debitorservice.GetList().subscribe(result => {
       this.debitorList = result;
       this.setPage(this.page);
       this.showError = false;
       this.errorMessage = "";
       this.ShowLoader = false;
     }, error => {
       this.ShowLoader = false;
       this.showError = true;
       this.errorMessage = error.Data.Message;
     });
   }
   SaveDebitor() {
    this.ShowLoader = true;
    debugger;
    this.debitorservice.CreateDebitor(this.debitorOBJ).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateDebitor() {
    this.ShowLoader = true;
    debugger;
    this.debitorservice.UpdateDebitor(this.debitorOBJ).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteDebitor(ID: string) {
    this.ShowLoader = true;
    debugger;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.debitorservice.DeleteDebitor(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
  subtract(){
    debugger;
    this.debitorOBJ.TotalAmt=this.debitorOBJ.PreviousAmt-this.debitorOBJ.PaidAmt;
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.debitorList.length, page);
 
    // get current page of items
    this.pagedItems = this.debitorList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
}
