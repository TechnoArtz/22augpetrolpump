import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { AddCreditor } from '../addCreditor';
import { AddCreditorService } from '../addCreditor-service';
import { DateTimeFormatter } from '../../Common/DateFormatter';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PagerService } from '../../page/page.service';


@Component({
  selector: 'app-addcreditor',
  templateUrl: './addcreditor.component.html',
  styleUrls: ['./addcreditor.component.css']
})
export class AddcreditorComponent implements OnInit {
  modalRef: BsModalRef;
  pager: any = {};
  pagedItems: any[];
  page:number=1;
  config = {
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  creditorOBJ: AddCreditor = new Object();
  creditorList: Array<AddCreditor>;
  errorMessage: string = "";
  showError: boolean = false;
  constructor(private pagerService: PagerService,private creditorservice: AddCreditorService, private modalservice: BsModalService, private dateformatter: DateTimeFormatter) { }
  ngOnInit() {
  }

  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id == undefined) {
      this.isUpdate = false;
      this.creditorOBJ = {};
      this.modalRef = this.modalservice.show(template, {
        class: 'modal-lg', keyboard: false,
        backdrop: false,
        ignoreBackdropClick: true
      });
    }
    else {
      debugger;
      this.isUpdate = true;
      this.ShowLoader = true;
      this.creditorservice.Get(Id).subscribe(result => {
        this.creditorOBJ = result;
        this.creditorOBJ.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, {
          class: 'modal-lg', keyboard: false,
          backdrop: false,
          ignoreBackdropClick: true
        });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    this.ShowLoader = true;
    debugger;
    this.creditorservice.GetList().subscribe(result => {
      this.creditorList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  SaveCreditor() {
    this.ShowLoader = true;
    debugger;
    this.creditorservice.CreateCreditor(this.creditorOBJ).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateCreditor() {
    this.ShowLoader = true;
    debugger;
    this.creditorservice.UpdateCreditor(this.creditorOBJ).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteCust(ID: string) {
    this.ShowLoader = true;
    debugger;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.creditorservice.DeleteCreditor(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.creditorList.length, page);
 
    // get current page of items
    this.pagedItems = this.creditorList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
}
