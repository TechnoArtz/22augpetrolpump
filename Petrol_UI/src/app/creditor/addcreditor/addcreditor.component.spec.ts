import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcreditorComponent } from './addcreditor.component';

describe('AddcreditorComponent', () => {
  let component: AddcreditorComponent;
  let fixture: ComponentFixture<AddcreditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcreditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcreditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
