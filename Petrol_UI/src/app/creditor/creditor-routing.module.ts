import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddcreditorComponent } from './addcreditor/addcreditor.component';
import { EntrycreditorComponent } from './entrycreditor/entrycreditor.component';
import { DebitorComponent } from './debitor/debitor.component';

const routes: Routes = [
  {
    path: '', children: [
      {
        path: 'addcreditor',
        component: AddcreditorComponent
      },
      {
        path: 'entrycreditor',
        component: EntrycreditorComponent
      },
      {
        path: 'debitor',
        component: DebitorComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreditorRoutingModule { }
