export interface Debitor
{
   ID?:string;
   Billno?:string;
   CustomerName?:string;
   CustomerNameDes?:string;
   PreviousAmt?:number;
   PaidAmt?:number;
   TotalAmt?:number;
   Status?:string
}