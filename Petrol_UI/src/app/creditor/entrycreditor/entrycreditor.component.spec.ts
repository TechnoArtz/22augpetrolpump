import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntrycreditorComponent } from './entrycreditor.component';

describe('EntrycreditorComponent', () => {
  let component: EntrycreditorComponent;
  let fixture: ComponentFixture<EntrycreditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntrycreditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntrycreditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
