import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { DateTimeFormatter } from '../../Common/DateFormatter';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CreditorEntryService } from '../entrycreditor-service';
import { CreditorEntryMaster, CreditorEntryDetails } from '../entrycreditor';
import { AddCreditorService } from '../addCreditor-service';
import { AddCreditor } from '../addCreditor';
import { PagerService } from '../../page/page.service';
import { UnitService } from '../../shift/ShiftMaster-Service';
import { Unit } from '../../shift/ShiftMaster';
@Component({
  selector: 'app-entrycreditor',
  templateUrl: './entrycreditor.component.html',
  styleUrls: ['./entrycreditor.component.css']
})
export class EntrycreditorComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  pager: any = {};
  pagedItems: any[];
  page:number=1;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  entrydetails: CreditorEntryDetails = new Object();
  entrymaster: CreditorEntryMaster = new Object();
  entrydetailsList: Array<CreditorEntryDetails> = [{}];
  entrymasterList: Array<CreditorEntryMaster> = [{}];
  errorMessage: string = "";
  showError: boolean = false;
  Qty: any = 0;
  Rate: any = 0;
  Amount: number;
  TotalAmount: number;
  TotalAmt: number;
  addCreditor:AddCreditor[];
  unitType:Unit[];
  valuedate=new Date();
  constructor(private modalservice: BsModalService,private unitmasterservice:UnitService,private pagerService: PagerService, private dateformatter: DateTimeFormatter, private creditorentryservice: CreditorEntryService,private addcreditorservice:AddCreditorService) {
    if (this.creditorentryservice.creditordetailsID != null && this.creditorentryservice.creditordetailsID != undefined && this.creditorentryservice.creditordetailsID != "0") {
      this.isUpdate = true;
      this.creditorentryservice.Get(this.creditorentryservice.creditordetailsID).subscribe(result => {
        this.entrydetailsList = result.lstcreditordetails;
        this.entrymaster = result;
      })
    }
    this.addcreditorservice.GetList().subscribe( result=>{
      this.addCreditor=result;
    },error=>{
      this.addCreditor=null;
    });
    this.unitmasterservice.GetList().subscribe( result=>{
      this.unitType=result;
    },error=>{
      this.unitType=null;
    });
  }
  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id == undefined) {
      this.isUpdate = false;
      this.entrymaster = {};
      //this.entrydetails={};
     this.entrydetailsList=[{}];
      this.modalRef = this.modalservice.show(template, {class: 'modal-lg', keyboard: false,backdrop: false, ignoreBackdropClick: true
      });
    }
    else {
      debugger;
      this.isUpdate = true;
      this.ShowLoader = true;
      this.creditorentryservice.Get(Id).subscribe(result => {
        this.entrymaster = result;
        this.entrydetailsList=this.entrymaster.lstcreditordetails;
        this.entrydetails.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, {
          class: 'modal-lg', keyboard: false,
          backdrop: false,
          ignoreBackdropClick: true
        });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    this.ShowLoader = true;
    debugger;
    this.creditorentryservice.GetList().subscribe(result => {
      this.entrydetailsList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  SaveCreditor() {
    this.ShowLoader = true;
    debugger;
    
    this.entrymaster.lstcreditordetails=this.entrydetailsList;
   // this.creditorentryservice.creditordetailsID="0";
    this.creditorentryservice.CreateCreditor(this.entrymaster).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateCreditor() {
    this.ShowLoader = true;
    debugger;
    this.entrymaster.lstcreditordetails=this.entrydetailsList;
    this.creditorentryservice.UpdateCreditor(this.entrymaster, this.entrymaster.ID).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteCreditor(ID: string) {
    this.ShowLoader = true;
    debugger;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.creditorentryservice.DeleteCreditor(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
  MultiplicationAmt(m, i) {
    debugger;
    this.entrydetailsList[i].Amount = m.Qty * m.Rate;
    // this.resultrate = this.entrydetailsList[i].Amount;
    this.getTotalAmount();
  }
  addrow() {
    this.entrydetailsList.push({});
    
  }
  getTotalAmount() {
    debugger;
    let total = 0;
    for (let i = 0; i < this.entrydetailsList.length; i++) {
      if (this.entrydetailsList[i].Amount) {
        total += this.entrydetailsList[i].Amount;
        this.TotalAmt = total;
      }
    }
    return this.TotalAmt;
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.entrydetailsList.length, page);
 
    // get current page of items
    this.pagedItems = this.entrydetailsList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
}