import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { APIConstant } from '../Common/AppConstant';
import { CreditorEntryMaster, CreditorEntryDetails } from './entrycreditor';
@Injectable()
export class CreditorEntryService {
   creditordetailsID:string;
   constructor(private httpclient: HttpClient) {
   }
   CreateCreditor(creditorentrymaster: CreditorEntryMaster): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "Creditor", creditorentrymaster);
   }
   UpdateCreditor(creditorentrydetails: CreditorEntryDetails,creditordetailsID): Observable<boolean> {
       debugger;
       return this.httpclient.put<boolean>(APIConstant + "Creditor?id=" + creditordetailsID, creditorentrydetails);
   }
   DeleteCreditor(ID: string): Observable<CreditorEntryDetails> {
       debugger;
       return this.httpclient.delete<CreditorEntryDetails>(APIConstant + "Creditor?id=" + ID);
   }
   GetList(): Observable<Array<CreditorEntryDetails>> {
       return this.httpclient.get<Array<CreditorEntryDetails>>(APIConstant + "Creditor");
   }
   Get(ID: string): Observable<CreditorEntryMaster> {
       debugger;
       return this.httpclient.get<CreditorEntryMaster>(APIConstant + "Creditor?id=" + ID);
   }
}