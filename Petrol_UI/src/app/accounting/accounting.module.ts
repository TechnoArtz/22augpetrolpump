import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AccountingRoutingModule } from './accounting-routing.module';
import { DebitnoteComponent } from './debitnote/debitnote.component';
import { CreditnoteComponent } from './creditnote/creditnote.component';
import { CreditsalebillComponent } from './creditsalebill/creditsalebill.component';
import { BankadvanceComponent } from './bankadvance/bankadvance.component';
import { BankdepositComponent } from './bankdeposit/bankdeposit.component';
import { BankwithdrawComponent } from './bankwithdraw/bankwithdraw.component';
import { AccountComponent } from './account/account.component';
import { BanktransferComponent } from './banktransfer/banktransfer.component';
import { BankAccMasterComponent } from './bank-acc-master/bank-acc-master.component';
import { BankAccMasterService } from './bank-acc-master/bank-acc-master-service';
import { BankDepositMasterComponent } from './bank-deposit-master/bank-deposit-master.component';
import { BankDepositMasterService } from './bank-deposit-master/bank-deposit-master-service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BankCreditMasterComponent } from './bank-credit-master/bank-credit-master.component';

import { ShiftMaster1Service } from '../shift/ShiftMaster1-Service';
import { EmployeeService } from '../employee/employee-service';
import { BankCreditService } from './bank-credit-master/BankCredit-service';
import { PagerService } from '../page/page.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AccountingRoutingModule,
    BsDatepickerModule
  ],
  // tslint:disable-next-line:max-line-length
  declarations: [
    DebitnoteComponent,
    CreditnoteComponent,
    CreditsalebillComponent,
    BankadvanceComponent,
    BankdepositComponent,
    BankwithdrawComponent,
    AccountComponent,
    BanktransferComponent,
    BankAccMasterComponent,
    BankDepositMasterComponent,
    BankCreditMasterComponent],
    providers:[BankAccMasterService,
      BankDepositMasterService,
      ShiftMaster1Service,
      EmployeeService,
    BankCreditService,
    PagerService]
})
export class AccountingModule { }