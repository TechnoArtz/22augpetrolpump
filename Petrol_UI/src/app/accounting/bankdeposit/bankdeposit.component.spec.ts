import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankdepositComponent } from './bankdeposit.component';

describe('BankdepositComponent', () => {
  let component: BankdepositComponent;
  let fixture: ComponentFixture<BankdepositComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankdepositComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankdepositComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
