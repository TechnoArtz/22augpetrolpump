import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { BankAccMaster } from './bank-acc-master';
import { APIConstant } from '../../Common/AppConstant';

@Injectable()
export class BankAccMasterService {
   constructor(private httpclient: HttpClient) {
   }
   CreateBankAccMaster(bankaccmaster: BankAccMaster): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "BankAccMaster", bankaccmaster);
   }
   UpdateBankAccMaster(bankaccmaster: BankAccMaster): Observable<boolean> {
       return this.httpclient.put<boolean>(APIConstant + "BankAccMaster?id=" + bankaccmaster.ID, bankaccmaster);
   }
   DeleteBankAccMaster(ID: string): Observable<BankAccMaster> {
       return this.httpclient.delete<BankAccMaster>(APIConstant + "BankAccMaster?id=" + ID);
   }
   GetList(): Observable<Array<BankAccMaster>> {
       return this.httpclient.get<Array<BankAccMaster>>(APIConstant + "BankAccMaster");
   }
   Get(ID: string): Observable<BankAccMaster> {
       return this.httpclient.get<BankAccMaster>(APIConstant + "BankAccMaster?id=" + ID);
   }
}