export interface BankAccMaster
{
    ID?:string,
    BankName?:string,
    AccHolderName?:string,
    AccountNo?:string,
    Branch?:string,
    IFSCCode?:string
}