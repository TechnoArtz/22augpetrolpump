import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BankAccMaster } from './bank-acc-master';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { BankAccMasterService } from './bank-acc-master-service';
import { DateTimeFormatter } from '../../Common/DateFormatter';
import { PagerService } from '../../page/page.service';

@Component({
  selector: 'app-bank-acc-master',
  templateUrl: './bank-acc-master.component.html',
  styleUrls: ['./bank-acc-master.component.css']
})
export class BankAccMasterComponent implements OnInit {
  modalRef: BsModalRef;
  pager: any = {};
 pagedItems: any[];
 page:number=1;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  errorMessage: string = "";
  showError: boolean = false;
  bankaccmaster: BankAccMaster=new Object();
  bankaccmasterList:Array<BankAccMaster>;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  constructor(private pagerService: PagerService,private bankaccmasterservice: BankAccMasterService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter) 
  { }

  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id === undefined) {
      this.isUpdate = false;
      this.bankaccmaster= {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    } else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.bankaccmasterservice.Get(Id).subscribe(result => {
        this.bankaccmaster = result;
        this.bankaccmaster.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      });
    }
  }
  GetList() {
    debugger;
    this.ShowLoader = true;
    this.bankaccmasterservice.GetList().subscribe(result => {
      this.bankaccmasterList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  SaveBankAccMaster() {
    this.ShowLoader = true;
  
    this.bankaccmasterservice.CreateBankAccMaster(this.bankaccmaster).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateBankAccMaster() {
    this.ShowLoader = true;
   
    this.bankaccmasterservice.UpdateBankAccMaster(this.bankaccmaster).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteBankAccMaster(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = 'Are you sure?';
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.bankaccmasterservice.DeleteBankAccMaster(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = '';
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      } else {
        this.ShowLoader = false;
      }
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.bankaccmasterList.length, page);
 
    // get current page of items
    this.pagedItems = this.bankaccmasterList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
}
