import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankAccMasterComponent } from './bank-acc-master.component';

describe('BankAccMasterComponent', () => {
  let component: BankAccMasterComponent;
  let fixture: ComponentFixture<BankAccMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankAccMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankAccMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
