import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankDepositMasterComponent } from './bank-deposit-master.component';

describe('BankDepositMasterComponent', () => {
  let component: BankDepositMasterComponent;
  let fixture: ComponentFixture<BankDepositMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankDepositMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankDepositMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
