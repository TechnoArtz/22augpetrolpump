import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { CommonModalComponent } from '../../common-modal/common-modal.component';

import { DateTimeFormatter } from '../../Common/DateFormatter';
import { BankDepositMasterService } from './bank-deposit-master-service';
import { BankDepositMaster } from './bank-deposit-master';


@Component({
  selector: 'app-bank-deposit-master',
  templateUrl: './bank-deposit-master.component.html',
  styleUrls: ['./bank-deposit-master.component.css'],
  providers:[DateTimeFormatter]
  
})
export class BankDepositMasterComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  errorMessage: string = "";
  showError: boolean = false;
  bankdeposit: BankDepositMaster=new Object();
  bankdepositList:Array<BankDepositMaster>;
  constructor(private bankdepositmasterservice: BankDepositMasterService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter) 
  { }

  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id === undefined) {
      this.isUpdate = false;
      this.bankdeposit= {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    } else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.bankdepositmasterservice.Get(Id).subscribe(result => {
        this.bankdeposit = result;
        this.bankdeposit.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      });
    }
  }
  GetList() {
    debugger;
    this.ShowLoader = true;
    this.bankdepositmasterservice.GetList().subscribe(result => {
      this.bankdepositList = result;
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  SaveBankAccMaster() {
    this.ShowLoader = true;
debugger;
    this.bankdepositmasterservice.CreateBankDepositMaster(this.bankdeposit).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateBankAccMaster() {
    this.ShowLoader = true;
   
    this.bankdepositmasterservice.UpdateBankDepositMaster(this.bankdeposit).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteBankAccMaster(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = 'Are you sure?';
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.bankdepositmasterservice.DeleteBankDepositMaster(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = '';
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      } else {
        this.ShowLoader = false;
      }
    });
  }

}
