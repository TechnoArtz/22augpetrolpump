import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { APIConstant } from '../../Common/AppConstant';
import { BankDepositMaster } from './bank-deposit-master';

@Injectable()
export class BankDepositMasterService {
   constructor(private httpclient: HttpClient) {
   }
   CreateBankDepositMaster(bankdepositmaster: BankDepositMaster): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "BankDepositMaster", bankdepositmaster);
   }
   UpdateBankDepositMaster(bankdepositmaster: BankDepositMaster): Observable<boolean> {
       return this.httpclient.put<boolean>(APIConstant + "BankDepositMaster?id=" + bankdepositmaster.ID, bankdepositmaster);
   }
   DeleteBankDepositMaster(ID: string): Observable<BankDepositMaster> {
       return this.httpclient.delete<BankDepositMaster>(APIConstant + "BankDepositMaster?id=" + ID);
   }
   GetList(): Observable<Array<BankDepositMaster>> {
       return this.httpclient.get<Array<BankDepositMaster>>(APIConstant + "BankDepositMaster");
   }
   Get(ID: string): Observable<BankDepositMaster> {
       return this.httpclient.get<BankDepositMaster>(APIConstant + "BankDepositMaster?id=" + ID);
   }
}