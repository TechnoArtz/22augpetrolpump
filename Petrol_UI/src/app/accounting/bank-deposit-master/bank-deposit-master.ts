export interface BankDepositMaster
{
    ID?:string,
    Date?:any,
    BankName?:string,
    BankAccNo?:string,
    Amount?:string,
    Note?:string
}