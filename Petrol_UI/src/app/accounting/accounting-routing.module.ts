import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DebitnoteComponent } from './debitnote/debitnote.component';
import { CreditnoteComponent } from './creditnote/creditnote.component';
import { CreditsalebillComponent } from './creditsalebill/creditsalebill.component';
import { BankadvanceComponent } from './bankadvance/bankadvance.component';
import { BankdepositComponent } from './bankdeposit/bankdeposit.component';
import { BankwithdrawComponent } from './bankwithdraw/bankwithdraw.component';
import { AccountComponent } from './account/account.component';
import { BanktransferComponent } from './banktransfer/banktransfer.component';
import { BankAccMasterComponent } from './bank-acc-master/bank-acc-master.component';
import { BankDepositMasterComponent } from './bank-deposit-master/bank-deposit-master.component';
import { BankCreditMasterComponent } from './bank-credit-master/bank-credit-master.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'debit-note', component: DebitnoteComponent
      },
      {
        path: 'credit-note', component: CreditnoteComponent
      },
      {
        path: 'credit-sale-bill', component: CreditsalebillComponent
      },
      {
        path: 'bank-advance', component: BankadvanceComponent
      },
      {
        path: 'bank-deposit', component: BankdepositComponent
      },
      {
        path: 'bank-withdraw', component: BankwithdrawComponent
      },
      {
        path: 'account', component: AccountComponent
      },
      {
        path: 'bank-transfer', component: BanktransferComponent
      },
      {
        path:'bank-acc-master', component:BankAccMasterComponent
      },
      {
        path:'bank-deposit-master', component:BankDepositMasterComponent
      },
      {
        path:'bank-credit-master', component:BankCreditMasterComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingRoutingModule { }
