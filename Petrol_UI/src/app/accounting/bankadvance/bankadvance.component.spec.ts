import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankadvanceComponent } from './bankadvance.component';

describe('BankadvanceComponent', () => {
  let component: BankadvanceComponent;
  let fixture: ComponentFixture<BankadvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankadvanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankadvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
