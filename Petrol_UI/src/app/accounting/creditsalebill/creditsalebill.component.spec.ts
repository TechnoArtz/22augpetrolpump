import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditsalebillComponent } from './creditsalebill.component';

describe('CreditsalebillComponent', () => {
  let component: CreditsalebillComponent;
  let fixture: ComponentFixture<CreditsalebillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditsalebillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditsalebillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
