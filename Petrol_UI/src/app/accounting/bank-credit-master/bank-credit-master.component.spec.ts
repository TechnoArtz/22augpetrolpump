import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCreditMasterComponent } from './bank-credit-master.component';

describe('BankCreditMasterComponent', () => {
  let component: BankCreditMasterComponent;
  let fixture: ComponentFixture<BankCreditMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankCreditMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankCreditMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
