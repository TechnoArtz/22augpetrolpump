export interface BankCreditMaster{
    ID? : string,
    Date?:any,
    Shift?:string,
    ShiftDes?:string,
    AccountName?:string,
    EmployeeName?:string,
    EmployeeNameDes?:string,
    PaymentMode?:string,
    BankName?: string,
    BankAccNo?:string,
    Amount?: number,
    Note?: string,
    
 }