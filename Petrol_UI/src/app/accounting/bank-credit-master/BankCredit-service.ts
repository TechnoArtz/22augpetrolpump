import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import {BankCreditMaster} from './BankCredit';
import { APIConstant } from '../../Common/AppConstant';

@Injectable()
export class BankCreditService {
   constructor(private httpclient: HttpClient) {
   }
   CreateBankCredit(bankcredit: BankCreditMaster): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "bankCreditMaster", bankcredit);
   }
   UpdateBankCredit(bankcredit: BankCreditMaster): Observable<boolean> {
       debugger;
       return this.httpclient.put<boolean>(APIConstant + "bankCreditMaster?id=" + bankcredit.ID, bankcredit);
   }
   DeleteBankCredit(ID: string): Observable<BankCreditMaster> {
       return this.httpclient.delete<BankCreditMaster>(APIConstant + "bankCreditMaster?id=" + ID);
   }
   GetList(): Observable<Array<BankCreditMaster>> {
       return this.httpclient.get<Array<BankCreditMaster>>(APIConstant + "bankCreditMaster");
   }
   Get(ID: string): Observable<BankCreditMaster> {
       debugger;
       return this.httpclient.get<BankCreditMaster>(APIConstant + "bankCreditMaster?id=" + ID);
   }
}