import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonModalComponent } from './../../common-modal/common-modal.component';
import { BsModalRef, BsModalService } from '../../../../node_modules/ngx-bootstrap/modal';
import { BankCreditMaster } from './BankCredit';

import { ShiftMaster1Service } from '../../shift/ShiftMaster1-Service';
import { ShiftMaster1 } from '../../shift/ShiftMaster1';
import { EmployeeService } from '../../employee/employee-service';
import { Employee } from '../../employee/Employee';
import { BankCreditService } from './BankCredit-service';

@Component({
  selector: 'app-bank-credit-master',
  templateUrl: './bank-credit-master.component.html',
  styleUrls: ['./bank-credit-master.component.css']
})
export class BankCreditMasterComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
    errorMessage: string = "";
  showError: boolean = false;
  bankcredit: BankCreditMaster = new Object();
  bankCreditList: Array<BankCreditMaster>;
  shiftmaster:ShiftMaster1[];
  emplist:Employee[];
  constructor(private modalservice:BsModalService,private empservice:EmployeeService,private creditservice:BankCreditService,private shiftservice:ShiftMaster1Service) {
    this.shiftservice.GetList().subscribe( result=>{
      debugger;
      this.shiftmaster=result;
    },error=>{
      this.shiftmaster=null;
    });
    this.empservice.GetList().subscribe(result=>{
      this.emplist=result;
    },error=>{
      this.emplist=null;
    });
   }
  
  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id == undefined) {
      this.isUpdate = false;
      this.bankcredit = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    }
    else {
    debugger;
      this.isUpdate = true;
      this.ShowLoader = true;
      this.creditservice.Get(Id).subscribe(result => {
        this.bankcredit = result;
        this.bankcredit.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    this.ShowLoader = true;
    debugger;
    this.creditservice.GetList().subscribe(result => {
      this.bankCreditList = result;
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  SaveCreditEntry() {
   this.ShowLoader = true;
   debugger;
   this.creditservice.CreateBankCredit(this.bankcredit).subscribe(result => {
     this.modalRef.hide();
     this.showError = false;
     this.errorMessage = "";
     this.ShowLoader = false;
     this.GetList();
   }, error => {
     this.showError = true;
     this.errorMessage = error.Data.Message;
     this.ShowLoader = false;
   });
 }
 UpdateCreditEntry() {
   this.ShowLoader = true;
   debugger;
   this.creditservice.UpdateBankCredit(this.bankcredit).subscribe(result => {
     this.modalRef.hide();
     this.showError = false;
     this.errorMessage = "";
     this.ShowLoader = false;
     this.GetList();
   }, error => {
     this.ShowLoader = false;
     this.showError = true;
     this.errorMessage = error.Data.Message;
   });
 }
 DeleteCreditEntry(ID: string) {
   this.ShowLoader = true;
   debugger;
   this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
   this.modalRef.content.message = "Are you sure?";
   this.modalRef.content.onClose.subscribe(result => {
     if (result) {
       this.creditservice.DeleteBankCredit(ID).subscribe(result => {
         this.modalRef.hide();
         this.showError = false;
         this.errorMessage = "";
         this.ShowLoader = false;
         this.GetList();
       }, error => {
         this.showError = true;
         this.errorMessage = error.Data.Message;
         this.ShowLoader = false;
       });
     }
     else
       this.ShowLoader = false;
   })
 }
}