import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductSaleMasterService } from './productsale-service';
import { NozzelMasterService } from '../shift/NozzelMaster-service';
import { ShiftTypeMasterService } from '../shift/shift-type-master/shift-type-master-service';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
//import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { EmployeeService } from '../employee/employee-service';
import { ShiftType } from '../shift/shift-type-master/shift-type-master';
import { Employee } from '../employee/Employee';
import { Nozzel } from '../shift/NozzelMaster';
import { ProductSaleMaster } from './productsale';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Tank } from '../shift/TankMaster';
import { TankMasterService } from '../shift/TankMaster-service';
import { CommonModalComponent } from '../common-modal/common-modal.component';

@Component({
  selector: 'app-productsale',
  templateUrl: './productsale.component.html',
  styleUrls: ['./productsale.component.css']
})
export class ProductsaleComponent implements OnInit {
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  productsale: ProductSaleMaster = new Object();
  productsalelist: Array<ProductSaleMaster>;
  errorMessage: string = "";
  showError: boolean = false;
  modalRef: BsModalRef;
  shiftmaster: ShiftType[];
  employee: Employee[];
  nozzelMaster:Nozzel[];
  tankmaster:Tank[];
  constructor(private productsaleservice: ProductSaleMasterService,private nozzelService:NozzelMasterService,private shiftservice: ShiftTypeMasterService,private modalService: BsModalService,private tankservice:TankMasterService,private empservice: EmployeeService)
  {
    this.empservice.GetList().subscribe(result=>{
      this.employee=result;
    },error =>{
      this.employee=null;
    });
 
 
    this.shiftservice.GetList().subscribe(result=>{
      this.shiftmaster=result;
    },error=>{
      this.shiftmaster=null;
    });
    this.tankservice.GetList().subscribe(result=>{
      this.tankmaster=result;
    },error=>{
      this.tankmaster=null;
    });
 
 this.nozzelService.GetList().subscribe(result=>{
  this.nozzelMaster=result;
 },error=>{
  this.nozzelMaster=null;
 });
 
 
   }
 
  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id === undefined) {
      this.isUpdate = false;
      this.productsale = {};
      this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
    } else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.productsaleservice.Get(Id).subscribe(result => {
        this.productsale = result;
        this.productsale.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      });
    }
  }
  
  GetList() {
 debugger;
    this.ShowLoader = true;
    this.productsaleservice.GetList().subscribe(result => {
      
      this.productsalelist = result;
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  SaveProductSaleMaster() {
    this.ShowLoader = true;
    debugger;
    this.productsaleservice.CreateProductSaleMaster(this.productsale).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }

  UpdateProductSaleMaster() {
    this.ShowLoader = true;
    
    this.productsaleservice.UpdateProductSaleMaster(this.productsale).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteProductSaleMaster(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalService.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.productsaleservice.DeleteProductSaleMaster(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
}
