import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { ProductsaleComponent } from './productsale.component';
// import { ShiftmanagementComponent } from './shiftmanagement.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'layout' },
    {
        path: '', component: ProductsaleComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductsaleRoutingModule { }
