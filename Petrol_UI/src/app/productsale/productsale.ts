export interface ProductSaleMaster{
    ID?:string;
    EmployeeName?:string;
    EmployeeNameDes?:string;
    ShiftType?:string;
    ShiftTypeDes?:string;
    TankType?:string;
    TankTypeDes?:string;
    NozzelNo?:string;
    NozzelNoDes?:string
    Notes?:string;
 
 }