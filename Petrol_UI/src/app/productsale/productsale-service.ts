import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';
import { ProductSaleMaster } from './productsale';

@Injectable()
export class ProductSaleMasterService {
   constructor(private httpclient: HttpClient) {
   }
   CreateProductSaleMaster(productsalemaster: ProductSaleMaster): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "ProductSaleMaster", productsalemaster);
   }
   UpdateProductSaleMaster(productsalemaster: ProductSaleMaster): Observable<boolean> {
       return this.httpclient.put<boolean>(APIConstant + "ProductSaleMaster?id=" + productsalemaster.ID, productsalemaster);
   }
   DeleteProductSaleMaster(ID: string): Observable<ProductSaleMaster> {
       return this.httpclient.delete<ProductSaleMaster>(APIConstant + "ProductSaleMaster?id=" + ID);
   }
   GetList(): Observable<Array<ProductSaleMaster>> {
       return this.httpclient.get<Array<ProductSaleMaster>>(APIConstant + "ProductSaleMaster");
   }
   Get(ID: string): Observable<ProductSaleMaster> {
       return this.httpclient.get<ProductSaleMaster>(APIConstant + "ProductSaleMaster?id=" + ID);
    }
}