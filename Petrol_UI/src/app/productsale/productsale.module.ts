import { NgModule } from '@angular/core';

import { ProductsaleComponent } from './productsale.component';
import {ProductsaleRoutingModule} from './productsale.routing';
import { ProductSaleMasterService } from './productsale-service';
import { EmployeeService } from '../employee/employee-service';
import { ShiftTypeMasterService } from '../shift/shift-type-master/shift-type-master-service';
import { NozzelMasterService } from '../shift/NozzelMaster-service';
import { TankMasterService } from '../shift/TankMaster-service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PagerService } from '../page/page.service';

// import { ShiftmanagementComponent } from './shiftmanagement.component';
// import { ShiftmanagementRoutingModule } from './shiftmanagement.routing';


@NgModule({
    declarations: [
        ProductsaleComponent
    ],
    imports: [
        ProductsaleRoutingModule,CommonModule,FormsModule
    ],
    providers:[ProductSaleMasterService,EmployeeService,ShiftTypeMasterService,PagerService,NozzelMasterService,TankMasterService]
})
export class ProductsaleModule { }
