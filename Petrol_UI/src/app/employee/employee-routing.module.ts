import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { EmployeesalaryComponent } from './employeesalary/employeesalary.component';
import { DepartmentMasterComponent } from './department-master/department-master.component';

const routes: Routes = [
  {
    path: '', children: [
      {
        path: 'registration', component: RegistrationComponent
      },
      {
        path: 'employee-salary', component: EmployeesalaryComponent
      },
      {
        path: 'department-master', component: DepartmentMasterComponent
      }
    ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
