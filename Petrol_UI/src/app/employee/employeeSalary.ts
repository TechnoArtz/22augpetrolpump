export interface EmployeeSalary {
    ID? : string,
    EmployeeName?: string,
    EmployeeNameDes?:string,
    Dapartment?:string,
    DepartmentDes?:string;
    AvailableDays?: number,
    PaidDays?: number,
    Rate?:number,
    Salary?:number,
    Bonus?:number,
    Total?:number,
 }