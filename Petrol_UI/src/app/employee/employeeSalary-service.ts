import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';
import { EmployeeSalary } from './employeeSalary';
@Injectable()
export class EmployeeSalaryService {
    constructor(private httpclient: HttpClient) {
    }
    CreateEmployeeSalary(employee: EmployeeSalary): Observable<boolean> {
        
        return this.httpclient.post<boolean>(APIConstant + "EmployeeSalary/CreateSalary", employee);
    }
    UpdateEmployeeSalary(employee: EmployeeSalary): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "EmployeeSalary?id=" + employee.ID, employee);
    }
    DeleteEmployeeSalary(ID: string): Observable<EmployeeSalary> {
        return this.httpclient.delete<EmployeeSalary>(APIConstant + "EmployeeSalary?id=" + ID);
    }
    GetList(): Observable<Array<EmployeeSalary>> {
        return this.httpclient.get<Array<EmployeeSalary>>(APIConstant + "EmployeeSalary/List");
    }
    Get(ID: string): Observable<EmployeeSalary> {
        return this.httpclient.get<EmployeeSalary>(APIConstant + "EmployeeSalary?id=" + ID);
    }
    GetPDF(): Observable<Array<EmployeeSalary>> {
        
        return this.httpclient.get<Array<EmployeeSalary>>(APIConstant + "EmployeeSalary/GeneratePdf");
    }
    // GetUserRoleList(): Observable<Array<Employee>> {
    //     return this.httpclient.get<Array<Employee>>(APIConstant + "Employee/UserRole");
    // }
    
}