import { Component, OnInit, TemplateRef } from '@angular/core';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { DateTimeFormatter } from '../../Common/DateFormatter';

import { DepartmentMasterService } from './department-master-service';
import { Department } from './department-master';
import { PagerService } from '../../page/page.service';
import { NgForm } from '../../../../node_modules/@angular/forms';
@Component({
  selector: 'app-department-master',
  templateUrl: './department-master.component.html',
  styleUrls: ['./department-master.component.css']
})
export class DepartmentMasterComponent implements OnInit {
  page:number=1;
  pager: any = {};
 pagedItems: any[];
  modalRef: BsModalRef;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  department: Department = new Object();
  departmentlist: Array<Department>;
  errorMessage: string = "";
  showError: boolean = false;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  Departmentname: string='';
  
  constructor(private departmentservice:DepartmentMasterService,private pagerService: PagerService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter) { }
  ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){
    
    form.reset();
    this.Departmentname='';
    
  }
  openModal(template: TemplateRef<any>, Id: string) {
    if (Id == undefined) {
      this.isUpdate = false;
      this.department = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-md',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.departmentservice.Get(Id).subscribe(result => {
        this.department = result;
        this.department.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-md',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
debugger;
    this.ShowLoader = true;
    this.departmentservice.GetList().subscribe(result => {
      this.departmentlist = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
      this.pager = this.pagerService.getPager(this.departmentlist.length, page);
 
    // get current page of items
    this.pagedItems = this.departmentlist.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveDepartmentMaster() {
    this.ShowLoader = true;
    
    this.departmentservice.CreateDepartmentMaster(this.department).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateDepartmentMaster() {
    this.ShowLoader = true;
    
    this.departmentservice.UpdateDepartmentMaster(this.department).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteDepartmentMaster(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.departmentservice.DeleteDepartmentMaster(ID).subscribe(result => {
          this.modalRef.hide(); 
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
}