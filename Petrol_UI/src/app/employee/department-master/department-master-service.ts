import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { APIConstant } from '../../Common/AppConstant';
import { Department } from './department-master';
@Injectable()
export class DepartmentMasterService {
   constructor(private httpclient: HttpClient) {
   }
   CreateDepartmentMaster(department: Department): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "DepartmentMaster", department);
   }
   UpdateDepartmentMaster(department: Department): Observable<boolean> {
       return this.httpclient.put<boolean>(APIConstant + "DepartmentMaster?id=" + department.ID, department);
   }
   DeleteDepartmentMaster(ID: string): Observable<Department> {
       return this.httpclient.delete<Department>(APIConstant + "DepartmentMaster?id=" + ID);
   }
   GetList(): Observable<Array<Department>> {
       return this.httpclient.get<Array<Department>>(APIConstant + "DepartmentMaster");
   }
   Get(ID: string): Observable<Department> {
       return this.httpclient.get<Department>(APIConstant + "DepartmentMaster?id=" + ID);
   }
}