import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { EmployeeService } from './employee-service';
import { EmployeeRoutingModule } from './employee-routing.module';
import { RegistrationComponent } from './registration/registration.component';
import { EmployeesalaryComponent } from './employeesalary/employeesalary.component';

import { SharedModuleModule } from '../shared-module/shared-module.module';
import { FilterRegEmployeePipe } from './registration/filter-reg-employee.pipe';
import { DepartmentMasterComponent } from './department-master/department-master.component';
import { DepartmentMasterService } from './department-master/department-master-service';
import { PagerService } from '../page/page.service';
import { EmployeeSalaryService } from './employeeSalary-service';
import { ShiftTypeMasterService } from '../shift/shift-type-master/shift-type-master-service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EmployeeRoutingModule,
    ModalModule,
    BsDatepickerModule,
    SharedModuleModule
  ],
  declarations: [RegistrationComponent, EmployeesalaryComponent, DepartmentMasterComponent],
  providers: [EmployeeService,EmployeeSalaryService,ShiftTypeMasterService,FilterRegEmployeePipe,DepartmentMasterService,PagerService,DepartmentMasterService]
})
export class EmployeeModule { }
