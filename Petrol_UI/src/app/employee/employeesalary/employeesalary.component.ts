import { Component, OnInit, TemplateRef } from '@angular/core';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { EmployeeSalaryService } from '../employeeSalary-service';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { EmployeeSalary } from '../employeeSalary';
import { DateTimeFormatter } from '../../Common/DateFormatter';
import { PagerService } from '../../page/page.service';
import { EmployeeService } from '../employee-service';
import { Employee } from '../Employee';
import { DepartmentMasterService } from '../department-master/department-master-service';
import { Department } from '../department-master/department-master';

import { ShiftTypeMasterService } from '../../shift/shift-type-master/shift-type-master-service';
import { ShiftType } from '../../shift/shift-type-master/shift-type-master';

@Component({
  selector: 'app-employeesalary',
  templateUrl: './employeesalary.component.html',
  styleUrls: ['./employeesalary.component.css']
})
export class EmployeesalaryComponent implements OnInit {
  page:number=1;
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  inputName : string = '';
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  employeeSalary: EmployeeSalary = new Object();
  empSalaryList: Array<EmployeeSalary>;
  errorMessage: string = "";
  showError: boolean = false;
  pager: any = {};
  pagedItems: any[];
  employeeList:Employee[];
  deptList:Department[];
  shiftmaster:ShiftType[];
  result: any;
  games: any[];
  selectedGame: Object = {};
  constructor(private employeeSalaryservice:EmployeeSalaryService,private Shiftservice: ShiftTypeMasterService,private deptservice:DepartmentMasterService,private empService: EmployeeService,private pagerService: PagerService ,private modalService: BsModalService, private dateFormatter: DateTimeFormatter) {
    // this.maxDate.setDate(this.maxDate.getDate() + 7);
    // this.bsRangeValue = [this.bsValue, this.maxDate]; 

    this.empService.GetList().subscribe(result => {
      this.employeeList = result;
    }, error => {
      this.employeeList=null;
    });

    this.deptservice.GetList().subscribe(result => {
      this.deptList = result;
    }, error => {
      this.deptList=null;
    });
    this.Shiftservice.GetList().subscribe(result => {
      this.shiftmaster = result;
    }, error => {
      this.shiftmaster=null;
    });


   }


  ngOnInit() {
  }
  pdffile()
 {
      window.open("http://localhost:60314/api/EmployeeSalary/GeneratePdf");
     
 }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id == undefined) {
      this.isUpdate = false;
      this.employeeSalary = {};
      this.modalRef = this.modalService.show(template,{class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true});
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.employeeSalaryservice.Get(Id).subscribe(result => {
        this.employeeSalary = result;
        this.employeeSalary.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    debugger;
    this.ShowLoader = true;
    this.employeeSalaryservice.GetList().subscribe(result => {
      this.empSalaryList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.empSalaryList.length, page);
 
    // get current page of items
    this.pagedItems = this.empSalaryList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveEmployeeSalary() {
    debugger;
    this.ShowLoader = true;
    // if (this.employeeSalary.Date instanceof Date) {
    //   this.employeeSalary.Date = this.dateFormatter.transformsDate(this.employeeSalary.Date);
    // }
   
    this.employeeSalaryservice.CreateEmployeeSalary(this.employeeSalary).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  Savepdf() {
    debugger;
    this.ShowLoader = true;   
    this.employeeSalaryservice.GetPDF().subscribe(result => {
      this.empSalaryList = result;
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }

  UpdateEmployeeSalary() {
    this.ShowLoader = true;
    // if (this.employeeSalary.Date instanceof Date) {
    //   this.employeeSalary.Date = this.dateFormatter.transformsDate(this.employeeSalary.Date);
    // }
    
    this.employeeSalaryservice.UpdateEmployeeSalary(this.employeeSalary).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteEmployeeSalary(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalService.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure You Want Delete this Record...!";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.employeeSalaryservice.DeleteEmployeeSalary(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }

  salarycal(){
    debugger;
    this.employeeSalary.Salary = this.employeeSalary.PaidDays * this.employeeSalary.Rate;
    
  }

  totalsal(){
    debugger;
  
    this.employeeSalary.Total = this.employeeSalary.Salary + this.employeeSalary.Bonus;

  }

}
