import { Component, OnInit, TemplateRef } from '@angular/core';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {NgForm} from '@angular/forms';
import { EmployeeService } from '../employee-service';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { Employee } from '../Employee';
import { DateTimeFormatter } from '../../Common/DateFormatter';
import { FilterRegEmployeePipe } from './filter-reg-employee.pipe';
import { Department } from '../department-master/department-master';
import { DepartmentMasterService } from '../department-master/department-master-service';
import { PagerService } from '../../page/page.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../../node_modules/rxjs/Observable';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [DateTimeFormatter]
})
export class RegistrationComponent implements OnInit {
  page:number=1;
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  inputName : string = '';
  Category : string =''; 
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  employee: Employee = new Object();
  empList: Array<Employee>;
  errorMessage: string = "";
  showError: boolean = false;
  defaultImage: string = "./assets/img/user.jpg";
  departmaster:Department[];
  pager: any = {};
 pagedItems: any[];
 public edited = false;
 private isUploadBtn: boolean = true;  
  
  constructor(private http: HttpClient,private empService: EmployeeService,private pagerService: PagerService,private departservice:DepartmentMasterService ,private modalService: BsModalService, private dateFormatter: DateTimeFormatter) 
  { this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsRangeValue = [this.bsValue, this.maxDate]; 

    this.departservice.GetList().subscribe(result => {
      this.departmaster = result;
    }, error => {
      this.departmaster=null;
    });
  }
  ngOnInit() {
    this.resetForm();
    this.resetFormall();
    
  }
  resetForm(form?:NgForm){
      
      form.reset();
      this.inputName='';
      this.Category='';
    
    } 

  resetFormall(form?:NgForm){
    form.reset();
    this.employee.EmployeeName='';
    this.employee.UserRole='';
    this.employee.Salary=0;
    this.employee.Gender='';
    this.employee.MobileNo ='';
    this.employee.Address='';

    
  }


  openModal(template: TemplateRef<any>, Id: string) {
    
    if (Id == undefined) {
      this.isUpdate = false;
      this.employee = {};
      this.modalRef = this.modalService.show(template,{class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true});
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.empService.Get(Id).subscribe(result => {
        this.employee = result;
        this.employee.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
  
    this.ShowLoader = true;
    this.empService.GetList().subscribe(result => {
      this.empList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.empList.length, page);
 
    // get current page of items
    this.pagedItems = this.empList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveEmployee() {
  
    this.ShowLoader = true;
    if (this.employee.DOB instanceof Date) {
      this.employee.DOB = this.dateFormatter.transformsDate(this.employee.DOB);
    }
    if (this.employee.JoiningDate instanceof Date) {
      this.employee.JoiningDate = this.dateFormatter.transformsDate(this.employee.JoiningDate);
    }
    this.empService.CreateEmployee(this.employee).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      
      this.GetList();
      this.saveTodos();
    },
     error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateEmployee() {
    this.ShowLoader = true;
    if (this.employee.DOB instanceof Date) {
      this.employee.DOB = this.dateFormatter.transformsDate(this.employee.DOB);
    }
    if (this.employee.JoiningDate instanceof Date) {
      this.employee.JoiningDate = this.dateFormatter.transformsDate(this.employee.JoiningDate);
    }
    this.empService.UpdateEmployee(this.employee).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteEmployee(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalService.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure You Want Delete this Record...!";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.empService.DeleteEmployee(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
  


  //file upload event 
  url = '';
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = reader.result;
      }
    }
  }

fileChange() {  
  
  debugger;  
  var fileList =(<HTMLInputElement>document.getElementById('inputGroupFile02')).files;
  if (fileList.length > 0) {  
  let file: File = fileList[0];  
  let formData: FormData = new FormData();  
  formData.append('uploadFile', file, file.name);  
  let apiUrl1 = "http://localhost:60314/api/Employee/UploadJsonFile";  
  this.http.post(apiUrl1, formData)  
  .subscribe(res =>  {
  data => console.log('success');
  error => console.log(error)  
  })  
  window.location.reload();  
  }
}  

  //show sucess message
 saveTodos(): void {
   debugger;
  //show box msg
  this.edited = true;
  //wait 3 Seconds and hide
  setTimeout(function() {
      this.edited = false;
      console.log(this.edited);
  }.bind(this), 3000);
 }
  
}
