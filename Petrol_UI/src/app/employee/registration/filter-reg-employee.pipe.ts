import { Pipe, PipeTransform } from '@angular/core';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Pipe({
  name: 'filterRegEmployee'
})
export class FilterRegEmployeePipe implements PipeTransform {

  transform(empList:any, bsRangeValue?: any): any {
    if (bsRangeValue) {
      const range = bsRangeValue.map(d => moment(d).format('MM/DD/YYYY'));
      return range[0] + ' - ' + range[1];
  } else {
      return null;
  }
   } 
  //  transform(empList:any, inputName?: string): any {
  //  if(inputName == empList.Name ) return empList;
  //      return empList.filter(function(empList){
  //        return empList.Name.toLowerCase().indexOf(inputName.toLowerCase()) > -1;
  //      } 
      
  //     )
  // } 

}
