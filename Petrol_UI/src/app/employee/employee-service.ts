import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';
import { Employee } from './Employee';
@Injectable()
export class EmployeeService {
    constructor(private httpclient: HttpClient) {
    }
    CreateEmployee(employee: Employee): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "Employee", employee);
    }
    UpdateEmployee(employee: Employee): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "Employee?id=" + employee.ID, employee);
    }
    DeleteEmployee(ID: string): Observable<Employee> {
        return this.httpclient.delete<Employee>(APIConstant + "Employee?id=" + ID);
    }
    GetList(): Observable<Array<Employee>> {
        return this.httpclient.get<Array<Employee>>(APIConstant + "Employee/List");
    }
    Get(ID: string): Observable<Employee> {
        return this.httpclient.get<Employee>(APIConstant + "Employee?id=" + ID);
    }
    GetUserRoleList(): Observable<Array<Employee>> {
        return this.httpclient.get<Array<Employee>>(APIConstant + "Employee/UserRole");
    }
    
}