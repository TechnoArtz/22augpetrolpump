export interface Employee {
    ID? : string,
    EmployeeName?: string,
    Address?: string,
    MobileNo?: string,
    Salary?:number,
    Gender?:string,
    UserRole?:string,
    UserRoleDes?:string,
    DOB?:any,
    JoiningDate?: any,
  
 
 }