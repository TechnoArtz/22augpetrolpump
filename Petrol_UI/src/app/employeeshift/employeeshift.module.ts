import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EmployeeshiftRoutingModule } from './employeeshift.routing';
import { EmployeeshiftComponent } from './employeeshift.component';

@NgModule({
    declarations: [
        EmployeeshiftComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        EmployeeshiftRoutingModule
    ],
})
export class EmployeeShiftModule { }

