import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-employeeshift',
  templateUrl: './employeeshift.component.html',
  styleUrls: ['./employeeshift.component.css']
})
export class EmployeeshiftComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  EditShift(){
    this.router.navigateByUrl("/layout/edit-shift");
  }
  CreateShift(){
    this.router.navigateByUrl("/layout/shift");
  }

}
