import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeshiftComponent } from './employeeshift.component';

describe('EmployeeshiftComponent', () => {
  let component: EmployeeshiftComponent;
  let fixture: ComponentFixture<EmployeeshiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeshiftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeshiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
