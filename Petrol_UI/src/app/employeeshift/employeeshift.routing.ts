import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeshiftComponent } from './employeeshift.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'layout' },
    {
        path: '', component: EmployeeshiftComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmployeeshiftRoutingModule { }
