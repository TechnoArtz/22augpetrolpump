export interface DipHSD{
    ID?:string;
    Date?:any;
    ItemType?:string;
    CurrentDip?:number;
    ActualDip?:number;
    Variation?:number;
 }