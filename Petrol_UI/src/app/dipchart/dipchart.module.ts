import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiphsdComponent } from './diphsd/diphsd.component';
import { DipmsComponent } from './dipms/dipms.component';
import {DipchartRoutingModule } from './dipchart-routing.module';
import { FormsModule } from '@angular/forms';
import { DipMSService } from './dipms-service';
import { DipHSDService } from './diphsd-service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { PagerService } from '../page/page.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DipchartRoutingModule,
    BsDatepickerModule
  ],
  declarations: [DiphsdComponent, DipmsComponent],
  providers:[DipMSService,DipHSDService,PagerService]
})
export class DipchartModule { }
