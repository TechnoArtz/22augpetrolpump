import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';
import {DipHSD} from './diphsd';
@Injectable()
export class DipHSDService {
   constructor(private httpclient: HttpClient) {
   }
   CreateDipHSD(diphsd: DipHSD): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "DipHSD", diphsd);
   }
   UpdateDipHSD(diphsd: DipHSD): Observable<boolean> {
       return this.httpclient.put<boolean>(APIConstant + "DipHSD?id=" + diphsd.ID, diphsd);
   }
   DeleteDipHSD(ID: string): Observable<DipHSD> {
       return this.httpclient.delete<DipHSD>(APIConstant + "DipHSD?id=" + ID);
   }
   GetList(): Observable<Array<DipHSD>> {
       return this.httpclient.get<Array<DipHSD>>(APIConstant + "DipHSD");
   }
   Get(ID: string): Observable<DipHSD> {
       return this.httpclient.get<DipHSD>(APIConstant + "DipHSD?id=" + ID);
   }
}