import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { DipMSService } from '../dipms-service';
import { DipMS } from '../dipms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DateTimeFormatter } from '../../Common/DateFormatter';
import { PagerService } from '../../page/page.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-dipms',
  templateUrl: './dipms.component.html',
  styleUrls: ['./dipms.component.css'],
  providers: [DateTimeFormatter]
})
export class DipmsComponent implements OnInit {
  edited: boolean;
  DateTo: string;
  DateFrom: string;
  Category: string;
  pager: any = {};
 pagedItems: any[];
 page:number=1;
 valuedate=new Date();
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  modalRef: BsModalRef;
  ShowLoader = false;
  isUpdate = false;
  dipms: DipMS = new Object();
  dipmsList: Array<DipMS>;
  errorMessage = '';
  showError = false;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  constructor(private pagerService: PagerService,private dipmsService: DipMSService, private modalService: BsModalService, private dateFormatter: DateTimeFormatter) 
  {
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsRangeValue = [this.bsValue, this.maxDate]; 
   }

   ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){
    
    form.reset();
    
    this.Category='';
    this.DateFrom='';
    this.DateTo='';
  } 
  saveTodos(): void {
    debugger;
   //show box msg
   this.edited = true;
   //wait 3 Seconds and hide
   setTimeout(function() {
       this.edited = false;
       console.log(this.edited);
   }.bind(this), 3000);
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id === undefined) {
      this.isUpdate = false;
      this.dipms = {};
      this.modalRef = this.modalService.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    } else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.dipmsService.Get(Id).subscribe(result => {
        this.dipms = result;
        this.dipms.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      });
    }
  }
  GetList() {
    debugger;
    this.ShowLoader = true;
    this.dipmsService.GetList().subscribe(result => {
      this.dipmsList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.dipmsList.length, page);
 
    // get current page of items
    this.pagedItems = this.dipmsList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveMS() {
    this.ShowLoader = true;
  
    this.dipmsService.CreateDipMS(this.dipms).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
      this.GetList();
      this.saveTodos();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateMS() {
    this.ShowLoader = true;
   
    this.dipmsService.UpdateDipMS(this.dipms).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteMS(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalService.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = 'Are you sure?';
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.dipmsService.DeleteDipMS(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = '';
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      } else {
        this.ShowLoader = false;
      }
    });
  }
  subtractActual(){
    this.dipms.Variation=this.dipms.CurrentDip-this.dipms.ActualDip;
  }

}
