import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DipmsComponent } from './dipms.component';

describe('DipmsComponent', () => {
  let component: DipmsComponent;
  let fixture: ComponentFixture<DipmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DipmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DipmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
