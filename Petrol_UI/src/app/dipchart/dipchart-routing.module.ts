import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DipmsComponent } from './dipms/dipms.component';
import { DiphsdComponent } from './diphsd/diphsd.component';

const routes: Routes = [
  {
    path: '', children: [
      {
        path: 'dip-hsd',
        component: DiphsdComponent
      },
      {
        path: 'dip-ms',
        component: DipmsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DipchartRoutingModule { }
