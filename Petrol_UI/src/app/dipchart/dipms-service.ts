import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';

import { DipMS } from './dipms';
@Injectable()
export class DipMSService {
   constructor(private httpclient: HttpClient) {
   }
   CreateDipMS(dipms: DipMS): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "DipMS", dipms);
   }
   UpdateDipMS(dipms: DipMS): Observable<boolean> {
       return this.httpclient.put<boolean>(APIConstant + "DipMS?id=" + dipms.ID, dipms);
   }
   DeleteDipMS(ID: string): Observable<DipMS> {
       return this.httpclient.delete<DipMS>(APIConstant + "DipMS?id=" + ID);
   }
   GetList(): Observable<Array<DipMS>> {
       return this.httpclient.get<Array<DipMS>>(APIConstant + "DipMS");
   }
   Get(ID: string): Observable<DipMS> {
       return this.httpclient.get<DipMS>(APIConstant + "DipMS?id=" + ID);
   }
}