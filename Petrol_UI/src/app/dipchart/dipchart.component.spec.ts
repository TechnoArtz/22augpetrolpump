import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DipchartComponent } from './dipchart.component';

describe('DipchartComponent', () => {
  let component: DipchartComponent;
  let fixture: ComponentFixture<DipchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DipchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DipchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
