export interface DipMS{
    ID?:string;
    Date?:any;
    ItemType?:string;
    CurrentDip?:number;
    ActualDip?:number;
    Variation?:number;
 }