
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DipchartComponent} from './dipchart.component';


const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'layout' },
    {
        path: '', component: DipchartComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DipchartRoutingModule { }
