import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiphsdComponent } from './diphsd.component';

describe('DiphsdComponent', () => {
  let component: DiphsdComponent;
  let fixture: ComponentFixture<DiphsdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiphsdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiphsdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
