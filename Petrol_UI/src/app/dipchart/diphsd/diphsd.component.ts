import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { DipHSDService } from '../diphsd-service';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { DateTimeFormatter } from '../../Common/DateFormatter';
import { DipHSD } from '../diphsd';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PagerService } from '../../page/page.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-diphsd',
  templateUrl: './diphsd.component.html',
  styleUrls: ['./diphsd.component.css'],
  providers: [DateTimeFormatter]
})
export class DiphsdComponent implements OnInit {
  edited: boolean;
  DateTo: string;
  DateFrom: string;
  Category: string;
  pager: any = {};
 pagedItems: any[];
 page:number=1;
 valuedate=new Date();
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  modalRef: BsModalRef;
  ShowLoader = false;
  isUpdate = false;
  diphsd: DipHSD = new Object();
  diphsdList: Array<DipHSD>=[{}];
  errorMessage = '';
  showError = false;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  CurrentDip:number;
  ActualDip: number;
  result: number;
    constructor(private pagerService: PagerService,private diphsdService: DipHSDService, private modalService: BsModalService, private dateFormatter: DateTimeFormatter) {
      this.maxDate.setDate(this.maxDate.getDate() + 7);
      this.bsRangeValue = [this.bsValue, this.maxDate]; 
     }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){
    
    form.reset();
    
    this.Category='';
    this.DateFrom='';
    this.DateTo='';
  } 
  saveTodos(): void {
    debugger;
   //show box msg
   this.edited = true;
   //wait 3 Seconds and hide
   setTimeout(function() {
       this.edited = false;
       console.log(this.edited);
   }.bind(this), 3000);
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id === undefined) {
      this.isUpdate = false;
      this.diphsd= {};
      this.modalRef = this.modalService.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    } else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.diphsdService.Get(Id).subscribe(result => {
        this.diphsd = result;
        this.diphsd.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      });
    }
  }
  GetList() {
    debugger;
    this.ShowLoader = true;
    this.diphsdService.GetList().subscribe(result => {
      this.diphsdList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.diphsdList.length, page);
 
    // get current page of items
    this.pagedItems = this.diphsdList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveHSD() {
    this.ShowLoader = true;
  
    this.diphsdService.CreateDipHSD(this.diphsd).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
      this.GetList();
      this.saveTodos();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateHSD() {
    this.ShowLoader = true;
   
    this.diphsdService.UpdateDipHSD(this.diphsd).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = '';
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteHSD(ID: string) {
    debugger;
    this.ShowLoader = true;
    this.modalRef = this.modalService.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = 'Are you sure?';
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.diphsdService.DeleteDipHSD(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = '';
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      } else {
        this.ShowLoader = false;
      }
    });
  }
  subtractActual(){
    this.diphsd.Variation=this.diphsd.CurrentDip-this.diphsd.ActualDip;
  }
}
