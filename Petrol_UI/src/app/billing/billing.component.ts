import { Component, OnInit, TemplateRef } from '@angular/core';
import { ROWSIZE, PAGESTOSHOW} from '../Common/Constant';
import {Router} from '@angular/router';

import { BillingService } from './billing-service';
import { Billing } from './billing';
import { CommonModalComponent } from '../common-modal/common-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Tank } from '../shift/TankMaster';
import { TankMasterService } from '../shift/TankMaster-service';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class BillingComponent implements OnInit {

  modalRef: BsModalRef;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  errorMessage: string = "";
  showError: boolean = false;
  billing: Billing = new Object();
  billingList: Array<Billing>;
  tanktype: Tank[]; 

  constructor(private router: Router,private billingservice: BillingService,private tankservice: TankMasterService,private modalService: BsModalService) 
  {
    this.tankservice.GetList().subscribe(result => {
      this.tanktype = result;
    }, error => {
      this.tanktype=null;
    });
  }
  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id === undefined) {
      this.isUpdate = false;
      this.billing= {};
      this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
    } else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.billingservice.Get(Id).subscribe(result => {
        this.billing = result;
        this.billing.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      });
    }
  }
  GetList() {
    
    this.ShowLoader = true;
    debugger;
    this.billingservice.GetList().subscribe(result => {
      this.billingList = result;
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  SaveBilling() {
    this.ShowLoader = true;
    
    this.billingservice.CreateBilling(this.billing).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateBilling() {
    this.ShowLoader = true;
    
    this.billingservice.UpdateBilling(this.billing).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteBilling(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalService.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.billingservice.DeleteBilling(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }

}
