import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';
import { Billing } from './billing';

@Injectable()
export class BillingService {
    constructor(private httpclient: HttpClient) {
    }
    CreateBilling(billing: Billing): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "Billing", billing);
    }
    UpdateBilling(billing: Billing): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "Billing?id=" + billing.ID, billing);
    }
    DeleteBilling(ID: string): Observable<Billing> {
        return this.httpclient.delete<Billing>(APIConstant + "Billing?id=" + ID);
    }
    GetList(): Observable<Array<Billing>> {
        return this.httpclient.get<Array<Billing>>(APIConstant + "Billing");
    }
    Get(ID: string): Observable<Billing> {
        return this.httpclient.get<Billing>(APIConstant + "Billing?id=" + ID);
    }
}