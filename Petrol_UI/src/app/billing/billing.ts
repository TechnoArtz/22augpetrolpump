export interface Billing
{
    ID?:string;
    BillNo?:number;
    Party?:string;
    VehicleNo?:string;
    ItemType?:string;
    ItemTypeDes?:string;
    Qty?:number;
    Rate?:number;
    Total?:number;
}