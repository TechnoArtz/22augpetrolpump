import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { BillingRoutingModule } from './billing.routing';
import {  BillingComponent } from './billing.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BillingService } from './billing-service';
import { TankMasterService } from '../shift/TankMaster-service';
import { PagerService } from '../page/page.service';



@NgModule({
    declarations: [
        BillingComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        BillingRoutingModule,
        ModalModule
    ],
    providers:[BillingService,TankMasterService,PagerService]
})
export class  BillingModule { }

