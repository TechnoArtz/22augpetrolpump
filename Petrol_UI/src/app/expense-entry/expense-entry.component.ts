import { Component, OnInit, TemplateRef } from '@angular/core';
import { ExpensesEntry } from './expense-entry';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CommonModalComponent } from '../common-modal/common-modal.component';
import { ExpensesEntryService } from './expense-entry-services';
import { DateTimeFormatter } from '../Common/DateFormatter';
import { PagerService } from '../page/page.service';

@Component({
  selector: 'app-expense-entry',
  templateUrl: './expense-entry.component.html',
  styleUrls: ['./expense-entry.component.css'],
  providers: [DateTimeFormatter]
})
export class ExpenseEntryComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  expenseentry: ExpensesEntry = new Object();
  expenseentryList: Array<ExpensesEntry>;
  errorMessage: string = "";
  showError: boolean = false;
  pager: any = {};
 pagedItems: any[];
 page:number=1;

  constructor(private pagerService: PagerService,private modalService: BsModalService, private expenseservice: ExpensesEntryService,private dateFormatter: DateTimeFormatter) 
  { }

  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id == undefined) {
      this.isUpdate = false;
      this.expenseentry = {};
      this.modalRef = this.modalService.show(template, { class: 'modal-lg',keyboard:false,
      backdrop: false,
      ignoreBackdropClick: true });
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.expenseservice.Get(Id).subscribe(result => {
        this.expenseentry = result;
        this.expenseentry.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg',keyboard:false,
        backdrop: false,
        ignoreBackdropClick: true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    debugger;
    this.ShowLoader = true;
    this.expenseservice.GetList().subscribe(result => {
      this.expenseentryList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.expenseentryList.length, page);
 
    // get current page of items
    this.pagedItems = this.expenseentryList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveExpenses() {
    this.ShowLoader = true;
    
    this.expenseservice.CreateExpenses(this.expenseentry).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateExpenses() {
    this.ShowLoader = true;
   
    this.expenseservice.UpdateExpenses(this.expenseentry).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteExpenses(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalService.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.expenseservice.DeleteExpenses(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }

}
