
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpenseEntryComponent } from './expense-entry.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'layout' },
    {
        path: '', component : ExpenseEntryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExpenseEntryRoutingModule { }
