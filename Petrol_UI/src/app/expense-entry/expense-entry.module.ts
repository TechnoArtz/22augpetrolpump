import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ExpenseEntryComponent } from './expense-entry.component';
import { ExpenseEntryRoutingModule } from './expense-entry.routing';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';

import { ExpensesEntryService } from './expense-entry-services';
import { PagerService } from '../page/page.service';
@NgModule({
    declarations: [
        ExpenseEntryComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ExpenseEntryRoutingModule,
        ModalModule,
        BsDatepickerModule


    ],
    providers:[ExpensesEntryService,PagerService]
})
export class ExpenseEntryModule { }

