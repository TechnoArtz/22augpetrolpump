import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';
import { ExpensesEntry } from './expense-entry';

@Injectable()
export class ExpensesEntryService {
    constructor(private httpclient: HttpClient) {
    }
    CreateExpenses(expenseentry: ExpensesEntry): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "Expenses", expenseentry);
    }
    UpdateExpenses(expenseentry: ExpensesEntry): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "Expenses?id=" + expenseentry.ID, expenseentry);
    }
    DeleteExpenses(ID: string): Observable<ExpensesEntry> {
        return this.httpclient.delete<ExpensesEntry>(APIConstant + "Expenses?id=" + ID);
    }
    GetList(): Observable<Array<ExpensesEntry>> {
        return this.httpclient.get<Array<ExpensesEntry>>(APIConstant + "Expenses");
    }
    Get(ID: string): Observable<ExpensesEntry> {
        return this.httpclient.get<ExpensesEntry>(APIConstant + "Expenses?id=" + ID);
    }
}