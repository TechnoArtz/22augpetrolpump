export interface ExpensesEntry
{
    ID?:string;
    Date?:any;
    Category?:string;
    PaymentMode?:string;
    BankName?:string;
    ChequeNo?:string;
    Amount?:number;
    Note?:string;

}