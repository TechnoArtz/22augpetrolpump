import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PumpMonthlyReportComponent } from './pump-monthly-report.component';

describe('PumpMonthlyReportComponent', () => {
  let component: PumpMonthlyReportComponent;
  let fixture: ComponentFixture<PumpMonthlyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PumpMonthlyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PumpMonthlyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
