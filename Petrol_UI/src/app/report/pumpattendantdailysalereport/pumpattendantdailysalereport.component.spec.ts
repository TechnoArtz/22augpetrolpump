import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PumpattendantdailysalereportComponent } from './pumpattendantdailysalereport.component';

describe('PumpattendantdailysalereportComponent', () => {
  let component: PumpattendantdailysalereportComponent;
  let fixture: ComponentFixture<PumpattendantdailysalereportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PumpattendantdailysalereportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PumpattendantdailysalereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
