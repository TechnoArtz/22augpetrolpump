import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftwisedailyComponent } from './shiftwisedaily.component';

describe('ShiftwisedailyComponent', () => {
  let component: ShiftwisedailyComponent;
  let fixture: ComponentFixture<ShiftwisedailyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftwisedailyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftwisedailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
