import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllCreditSalesReportComponent } from './all-credit-sales-report.component';

describe('AllCreditSalesReportComponent', () => {
  let component: AllCreditSalesReportComponent;
  let fixture: ComponentFixture<AllCreditSalesReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllCreditSalesReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllCreditSalesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
