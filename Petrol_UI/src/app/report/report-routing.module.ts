import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PumpMonthlyReportComponent } from './pump-monthly-report/pump-monthly-report.component';
import { AllCreditSalesReportComponent } from './all-credit-sales-report/all-credit-sales-report.component';
import { CreditbillbalancereportComponent } from './creditbillbalancereport/creditbillbalancereport.component';
import { CustomerwisecreditsalereportComponent } from './customerwisecreditsalereport/customerwisecreditsalereport.component';
import { ProductratehistoryComponent } from './productratehistory/productratehistory.component';
import { AllcreditbalancereportComponent } from './allcreditbalancereport/allcreditbalancereport.component';
import { AccountbalancesheetComponent } from './accountbalancesheet/accountbalancesheet.component';
import { ProductinventoryreportComponent } from './productinventoryreport/productinventoryreport.component';
import { PumpattendantdailysalereportComponent } from './pumpattendantdailysalereport/pumpattendantdailysalereport.component';
import { ShiftwisedailyComponent } from './shiftwisedaily/shiftwisedaily.component';
import { AttendanwisedailypumpreportComponent } from './attendanwisedailypumpreport/attendanwisedailypumpreport.component';
import { CashbookComponent } from './cashbook/cashbook.component';
import { BankbookComponent } from './bankbook/bankbook.component';
import { CustomerwisecreditbalancereportComponent } from './customerwisecreditbalancereport/customerwisecreditbalancereport.component';
import { ExpensereportComponent } from './expensereport/expensereport.component';


const routes: Routes = [
  {
    path: '', children: [
      {
        path: 'pump-month', component: PumpMonthlyReportComponent
      },
      {
        path: 'all-credit', component: AllCreditSalesReportComponent
      },
      {
        path: 'credit-bill-balance', component: CreditbillbalancereportComponent
      },
      {
        path: 'customer-wise-sale', component: CustomerwisecreditsalereportComponent
      },
      {
        path: 'all-credit-balance', component: AllcreditbalancereportComponent
      },
      {
        path: 'product-rate-history', component: ProductratehistoryComponent

      },
      {
        path: 'account-balance-sheet', component: AccountbalancesheetComponent

      },
      {
        path: 'product-inventroy-report', component: ProductinventoryreportComponent

      },
      {
        path: 'pump-attendent-daily-sale', component: PumpattendantdailysalereportComponent

      },
      {
        path: 'cash-book', component: CashbookComponent

      },
      {
        path: 'bank-book', component: BankbookComponent

      },
      {
        path: 'customer-wise-credit-balance', component: CustomerwisecreditbalancereportComponent

      },
      {
        path: 'shift-wise-daily', component: ShiftwisedailyComponent
      },
      {
        path: 'attendant-wise-daily-pump', component: AttendanwisedailypumpreportComponent

      },
      {
        path: 'expense-report', component: ExpensereportComponent

      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
