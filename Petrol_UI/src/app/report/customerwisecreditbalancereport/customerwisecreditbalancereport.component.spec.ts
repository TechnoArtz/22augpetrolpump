import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerwisecreditbalancereportComponent } from './customerwisecreditbalancereport.component';

describe('CustomerwisecreditbalancereportComponent', () => {
  let component: CustomerwisecreditbalancereportComponent;
  let fixture: ComponentFixture<CustomerwisecreditbalancereportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerwisecreditbalancereportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerwisecreditbalancereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
