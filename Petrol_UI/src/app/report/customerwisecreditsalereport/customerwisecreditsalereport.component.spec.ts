import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerwisecreditsalereportComponent } from './customerwisecreditsalereport.component';

describe('CustomerwisecreditsalereportComponent', () => {
  let component: CustomerwisecreditsalereportComponent;
  let fixture: ComponentFixture<CustomerwisecreditsalereportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerwisecreditsalereportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerwisecreditsalereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
