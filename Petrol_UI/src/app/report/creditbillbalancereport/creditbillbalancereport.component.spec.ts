import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditbillbalancereportComponent } from './creditbillbalancereport.component';

describe('CreditbillbalancereportComponent', () => {
  let component: CreditbillbalancereportComponent;
  let fixture: ComponentFixture<CreditbillbalancereportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditbillbalancereportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditbillbalancereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
