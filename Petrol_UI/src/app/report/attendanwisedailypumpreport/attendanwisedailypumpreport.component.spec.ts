import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanwisedailypumpreportComponent } from './attendanwisedailypumpreport.component';

describe('AttendanwisedailypumpreportComponent', () => {
  let component: AttendanwisedailypumpreportComponent;
  let fixture: ComponentFixture<AttendanwisedailypumpreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendanwisedailypumpreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanwisedailypumpreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
