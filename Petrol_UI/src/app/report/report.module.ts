import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ReportRoutingModule } from './report-routing.module';
import { PumpMonthlyReportComponent } from './pump-monthly-report/pump-monthly-report.component';
import { AllCreditSalesReportComponent } from './all-credit-sales-report/all-credit-sales-report.component';
import { CreditbillbalancereportComponent } from './creditbillbalancereport/creditbillbalancereport.component';
import { CustomerwisecreditsalereportComponent } from './customerwisecreditsalereport/customerwisecreditsalereport.component';
import { ProductratehistoryComponent } from './productratehistory/productratehistory.component';
import { AllcreditbalancereportComponent } from './allcreditbalancereport/allcreditbalancereport.component';
import { AccountbalancesheetComponent } from './accountbalancesheet/accountbalancesheet.component';
import { ProductinventoryreportComponent } from './productinventoryreport/productinventoryreport.component';
import { PumpattendantdailysalereportComponent } from './pumpattendantdailysalereport/pumpattendantdailysalereport.component';
import { CashbookComponent } from './cashbook/cashbook.component';
import { BankbookComponent } from './bankbook/bankbook.component';
import { ShiftwisedailyComponent } from './shiftwisedaily/shiftwisedaily.component';
import { AttendanwisedailypumpreportComponent } from './attendanwisedailypumpreport/attendanwisedailypumpreport.component';
import { CustomerwisecreditbalancereportComponent } from './customerwisecreditbalancereport/customerwisecreditbalancereport.component';
import { ExpensereportComponent } from './expensereport/expensereport.component';


@NgModule({
  imports: [
    CommonModule,
    ReportRoutingModule
  ],
  declarations: [
    PumpMonthlyReportComponent,
    AllCreditSalesReportComponent,
    CreditbillbalancereportComponent,
    CustomerwisecreditsalereportComponent,
    ProductratehistoryComponent,
    AllcreditbalancereportComponent,
    AccountbalancesheetComponent,
    ProductinventoryreportComponent,
    PumpattendantdailysalereportComponent,
    CashbookComponent,
    BankbookComponent,
    ShiftwisedailyComponent,
    AttendanwisedailypumpreportComponent,
    CustomerwisecreditbalancereportComponent,
    ExpensereportComponent
  ]
  
})
export class ReportModule { }
