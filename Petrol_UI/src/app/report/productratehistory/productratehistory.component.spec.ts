import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductratehistoryComponent } from './productratehistory.component';

describe('ProductratehistoryComponent', () => {
  let component: ProductratehistoryComponent;
  let fixture: ComponentFixture<ProductratehistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductratehistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductratehistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
