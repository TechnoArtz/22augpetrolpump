import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductinventoryreportComponent } from './productinventoryreport.component';

describe('ProductinventoryreportComponent', () => {
  let component: ProductinventoryreportComponent;
  let fixture: ComponentFixture<ProductinventoryreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductinventoryreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductinventoryreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
