import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllcreditbalancereportComponent } from './allcreditbalancereport.component';

describe('AllcreditbalancereportComponent', () => {
  let component: AllcreditbalancereportComponent;
  let fixture: ComponentFixture<AllcreditbalancereportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllcreditbalancereportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllcreditbalancereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
