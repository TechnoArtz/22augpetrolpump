import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';
//import { Employee } from './Employee';
import { Supplier } from './addsupplier';
@Injectable()
export class SupplierService {
    constructor(private httpclient: HttpClient) {
    }
    CreateSupplier(supplier: Supplier): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "SupplierMaster", supplier);
    }
    UpdateSupplier(supplier: Supplier): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "SupplierMaster?id=" + supplier.ID, supplier);
    }
    DeleteSupplier(ID: string): Observable<Supplier> {
        return this.httpclient.delete<Supplier>(APIConstant + "SupplierMaster?id=" + ID);
    }
    GetList(): Observable<Array<Supplier>> {
        return this.httpclient.get<Array<Supplier>>(APIConstant + "SupplierMaster");
    }
    Get(ID: string): Observable<Supplier> {
        return this.httpclient.get<Supplier>(APIConstant + "SupplierMaster?id=" + ID);
    }
}