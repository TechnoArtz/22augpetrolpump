import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddsupplierComponent } from './addsupplier/addsupplier.component';
import { AddcustomerComponent } from './addcustomer/addcustomer.component';

const routes: Routes = [
  {
    path: 'add-supplier', component: AddsupplierComponent
  },
  {
    path: 'add-customer', component: AddcustomerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrmRoutingModule { }
