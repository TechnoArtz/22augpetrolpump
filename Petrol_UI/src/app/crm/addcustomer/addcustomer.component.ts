import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonModalComponent } from '../../common-modal/common-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


import { Customer } from '../addCustomer';
import { CustomerService } from '../addCustomer-service';

import { DateTimeFormatter } from '../../Common/DateFormatter';
import { PagerService } from '../../page/page.service';

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.component.html',
  styleUrls: ['./addcustomer.component.css']
})
export class AddcustomerComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  customer: Customer = new Object();
  customerList: Array<Customer>;
  errorMessage: string = "";
  showError: boolean = false;
  pager: any = {};
  pagedItems: any[];
  page:number=1;
  constructor(private pagerService:PagerService,private dateFormatter: DateTimeFormatter,private customerservice:CustomerService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter) { }
  ngOnInit() {
  }
  
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id == undefined) {
      this.isUpdate = false;
      this.customer = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg',keyboard:false,
      backdrop: false,
      ignoreBackdropClick: true });
    }
    else {
    debugger;
      this.isUpdate = true;
      this.ShowLoader = true;
      this.customerservice.Get(Id).subscribe(result => {
        this.customer = result;
        this.customer.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg',keyboard:false,
        backdrop: false,
        ignoreBackdropClick: true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
     this.ShowLoader = true;
     debugger;
     this.customerservice.GetList().subscribe(result => {
       this.customerList = result;
       this.setPage(this.page);
       this.showError = false;
       this.errorMessage = "";
       this.ShowLoader = false;
     }, error => {
       this.ShowLoader = false;
       this.showError = true;
       this.errorMessage = error.Data.Message;
     });
   }
   SaveCust() {
    this.ShowLoader = true;
    debugger;
    this.customerservice.CreateCustomer(this.customer).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateCust() {
    this.ShowLoader = true;
    debugger;
    this.customerservice.UpdateCustomer(this.customer).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteCust(ID: string) {
    this.ShowLoader = true;
    debugger;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.customerservice.DeleteCustomer(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.customerList.length, page);
 
    // get current page of items
    this.pagedItems = this.customerList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
}
