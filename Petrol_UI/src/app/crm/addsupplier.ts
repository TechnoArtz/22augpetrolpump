export interface Supplier{
    ID? : string,
    Name?: string,
    CompanyName?: string,
    Email?: string,
    MobileNo1?: string,
    MobileNo2?: string,
    Address?: string,
    City?: string,
    ZipCode?:number;
    

}