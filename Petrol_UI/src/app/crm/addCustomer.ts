export interface Customer{
    ID? : string,
    CustomerName?: string,
    Email?:string,
    Address?: string,
    MobileNumber1?: string,
    MobileNumber2?: string,
    City?: string,
    ZipCode?:number;
 }