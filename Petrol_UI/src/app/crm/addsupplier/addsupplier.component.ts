import { Component, OnInit, TemplateRef } from '@angular/core';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


import { CommonModalComponent } from '../../common-modal/common-modal.component';

import { DateTimeFormatter } from '../../Common/DateFormatter';
import { SupplierService } from '../addsupplier-service';
import { Supplier } from '../addsupplier';
import { PagerService } from '../../page/page.service';
@Component({
  selector: 'app-addsupplier',
  templateUrl: './addsupplier.component.html',
  styleUrls: ['./addsupplier.component.css']
})
export class AddsupplierComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  supplier: Supplier = new Object();
  supplierList: Array<Supplier>;
  errorMessage: string = "";
  showError: boolean = false;
  defaultImage: string = "./assets/img/user.jpg";
  pager: any = {};
  pagedItems: any[];
  page:number=1;
  constructor(private pagerService: PagerService,private dateFormatter: DateTimeFormatter,private supplierservice:SupplierService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter ) { }


  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, Id: string) {
    if (Id == undefined) {
      this.isUpdate = false;
      this.supplier = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg',keyboard:false,
      backdrop: false,
      ignoreBackdropClick: true });
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.supplierservice.Get(Id).subscribe(result => {
        this.supplier = result;
        this.supplier.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg', keyboard:false,
        backdrop: false,
        ignoreBackdropClick: true  });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    debugger;
     this.ShowLoader = true;
     this.supplierservice.GetList().subscribe(result => {
       this.supplierList = result;
       this.setPage(this.page);
       this.showError = false;
       this.errorMessage = "";
       this.ShowLoader = false;
     }, error => {
       this.ShowLoader = false;
       this.showError = true;
       this.errorMessage = error.Data.Message;
     });
   }
   SaveSupplier() {
    this.ShowLoader = true;
    
    this.supplierservice.CreateSupplier(this.supplier).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateSupplier() {
    this.ShowLoader = true;
    
    this.supplierservice.UpdateSupplier(this.supplier).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteSupplier(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.supplierservice.DeleteSupplier(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.supplierList.length, page);
 
    // get current page of items
    this.pagedItems = this.supplierList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
}
