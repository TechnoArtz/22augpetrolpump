import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';

import { Customer } from './addCustomer';
@Injectable()
export class CustomerService {
   constructor(private httpclient: HttpClient) {
   }
   CreateCustomer(customer: Customer): Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "CustomerMaster", customer);
   }
   UpdateCustomer(customer: Customer): Observable<boolean> {
       debugger;
       return this.httpclient.put<boolean>(APIConstant + "CustomerMaster?id=" + customer.ID, customer);
   }
   DeleteCustomer(ID: string): Observable<Customer> {
       return this.httpclient.delete<Customer>(APIConstant + "CustomerMaster?id=" + ID);
   }
   GetList(): Observable<Array<Customer>> {
       return this.httpclient.get<Array<Customer>>(APIConstant + "CustomerMaster");
   }
   Get(ID: string): Observable<Customer> {
       debugger;
       return this.httpclient.get<Customer>(APIConstant + "CustomerMaster?id=" + ID);
   }
}