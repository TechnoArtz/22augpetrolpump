import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrmRoutingModule } from './crm-routing.module';
import { AddsupplierComponent } from './addsupplier/addsupplier.component';
import { AddcustomerComponent } from './addcustomer/addcustomer.component';
import { SupplierService } from './addsupplier-service';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CustomerService } from './addCustomer-service';
import { PagerService } from '../page/page.service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    CrmRoutingModule,
    FormsModule,
    ModalModule,
    BsDatepickerModule
  ],
  declarations: [AddsupplierComponent, AddcustomerComponent],
  providers:[SupplierService,CustomerService,PagerService]
})
export class CrmModule { }
