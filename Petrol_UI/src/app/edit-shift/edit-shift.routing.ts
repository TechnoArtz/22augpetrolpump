
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { EditShiftComponent } from './edit-shift.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'layout' },
    {
        path: '', component: EditShiftComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EditShiftRoutingModule { }
