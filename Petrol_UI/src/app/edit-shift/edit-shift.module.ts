import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EditShiftRoutingModule } from './edit-shift.routing';
import { EditShiftComponent } from './edit-shift.component';

@NgModule({
    declarations: [
        EditShiftComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        EditShiftRoutingModule
    ],
})
export class EditShiftModule { }

