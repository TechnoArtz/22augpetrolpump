export interface Search
{
    category?:string,
    Role?:string,
    StartDate?:Date,
    EndDate?:Date,
    EmployeeName?:string
}