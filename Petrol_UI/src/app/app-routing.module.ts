import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { LoginregistrationComponent } from './loginregistration/loginregistration.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'login', component: LoginComponent },
  { path: 'login-registration', component: LoginregistrationComponent },
  {
    path: 'layout', component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'home' },
      { path: 'home', loadChildren: './home/home.module#HomeModule' },
      { path: 'userlist', loadChildren: './user/user.module#UserModule' },
      { path: 'employee', loadChildren: './employee/employee.module#EmployeeModule' },
      { path: 'customer', loadChildren: './customer/customer.module#CustomerModule'},
      { path: 'edit-shift', loadChildren: './edit-shift/edit-shift.module#EditShiftModule'},
      { path: 'shift', loadChildren: './shift/shift.module#ShiftModule'},
      { path: 'shiftmanagement', loadChildren: './shiftmanagement/shiftmanagement.module#ShiftmanagementModule'},
      { path: 'billing', loadChildren: './billing/billing.module#BillingModule'},
      { path: 'employeeshift', loadChildren: './employeeshift/employeeshift.module#EmployeeShiftModule'},
      { path: 'inventory', loadChildren: './inventory/inventory.module#InventoryModule'},
      { path: 'creditor', loadChildren: './creditor/creditor.module#CreditorModule'},
      { path: 'accounting', loadChildren: './accounting/accounting.module#AccountingModule'},
      { path: 'report', loadChildren: './report/report.module#ReportModule'},
      { path: 'expense-entry', loadChildren: './expense-entry/expense-entry.module#ExpenseEntryModule'},
      { path: 'dipchart', loadChildren: './dipchart/dipchart.module#DipchartModule'},
      { path: 'productsale', loadChildren: './productsale/productsale.module#ProductsaleModule'},
      { path: 'purchase', loadChildren: './purchase/purchase.module#PurchaseModule'},
      { path: 'crm', loadChildren: './crm/crm.module#CrmModule'},
      { path: 'receiveitem', loadChildren: './receiveitem/receiveitem.module#ReceiveItemModule'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
