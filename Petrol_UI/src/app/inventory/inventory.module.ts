import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageproductComponent } from './manageproduct/manageproduct.component';
import { InventoryRoutingModule } from './inventory-routing.module';
import { AddorderComponent } from './addorder/addorder.component';
import { SupplierComponent } from './supplier/supplier.component';
import { DailysalereportComponent } from './dailysalereport/dailysalereport.component';
import { PurchaseproductComponent } from './purchaseproduct/purchaseproduct.component';
import { ProductentryComponent } from './productentry/productentry.component';
import { StockreportComponent } from './stockreport/stockreport.component';
import { PurchasereturnComponent } from './purchasereturn/purchasereturn.component';

@NgModule({
  imports: [
    CommonModule,
    InventoryRoutingModule
  ],
  declarations: [ManageproductComponent, AddorderComponent, SupplierComponent, DailysalereportComponent, PurchaseproductComponent, ProductentryComponent, StockreportComponent, PurchasereturnComponent]
})
export class InventoryModule { }
