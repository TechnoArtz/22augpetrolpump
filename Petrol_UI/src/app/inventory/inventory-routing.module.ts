import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageproductComponent } from './manageproduct/manageproduct.component';
import { AddorderComponent } from './addorder/addorder.component';
import { SupplierComponent } from './supplier/supplier.component';
import { DailysalereportComponent } from './dailysalereport/dailysalereport.component';
import { PurchaseproductComponent } from './purchaseproduct/purchaseproduct.component';
import { ProductentryComponent } from './productentry/productentry.component';
import { StockreportComponent } from './stockreport/stockreport.component';
import { PurchasereturnComponent } from './purchasereturn/purchasereturn.component';

const routes: Routes = [
  {
    path: '', children: [
      {
        path: 'manage-product', component: ManageproductComponent
      },
      {
        path: 'add-order', component: AddorderComponent
      },
      {
        path: 'supplier', component: SupplierComponent
      },
      {
        path: 'daily-sale-report', component: DailysalereportComponent
      },
      {
        path: 'purchase-product', component: PurchaseproductComponent
      },
      {
        path: 'product-entry', component: ProductentryComponent
      },
      {
        path: 'stock-report', component: StockreportComponent
      },
      {
        path: 'purchase-return', component: PurchasereturnComponent
      }
    ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }
