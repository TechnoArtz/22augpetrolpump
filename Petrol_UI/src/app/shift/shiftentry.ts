export interface ShiftEntry{
    EncShiftType?:string,
    ID?:string,
    ShiftType?:string;
    ShiftTypeDec?:string;
    EmployeeName?:string;
    EmployeeNameDes?:string;
    StartTime?:string;
    EndTime?:string;
    CreatedDate?:any;

}
