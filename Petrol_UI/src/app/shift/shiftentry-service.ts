import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';




import { ShiftEntry } from './shiftentry';
@Injectable()
export class ShiftEntryService {
    constructor(private httpclient: HttpClient) {
    }
    CreateShiftEntry(shiftentry: ShiftEntry): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "ShiftEntry", shiftentry);
    }
    UpdateShiftEntry(shiftentry: ShiftEntry): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "ShiftEntry?id=" + shiftentry.ID, shiftentry);
    }
    DeleteShiftEntry(ID: string): Observable<ShiftEntry> {
        return this.httpclient.delete<ShiftEntry>(APIConstant + "ShiftEntry?id=" + ID);
    }
    GetList(): Observable<Array<ShiftEntry>> {
        return this.httpclient.get<Array<ShiftEntry>>(APIConstant + "ShiftEntry");
    }
    Get(ID: string): Observable<ShiftEntry> {
        debugger;
        return this.httpclient.get<ShiftEntry>(APIConstant + "ShiftEntry?id=" + ID);
    }
}