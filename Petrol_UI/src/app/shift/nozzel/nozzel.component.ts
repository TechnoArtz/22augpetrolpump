import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


import { CommonModalComponent } from '../../common-modal/common-modal.component';

import { DateTimeFormatter } from '../../Common/DateFormatter';


import { NozzelMasterService } from '../NozzelMaster-service';
import { Nozzel } from '../NozzelMaster';

import { Product } from './Product';
import { Type } from '@angular/compiler/src/output/output_ast';
import { FilterPipe } from './filter.pipe';
import { PagerService } from '../../page/page.service';
import { NgForm } from '@angular/forms';



@Component({
  selector: 'app-nozzel',
  templateUrl: './nozzel.component.html',
  styleUrls: ['./nozzel.component.css']
})
export class NozzelComponent implements OnInit {

  edited: boolean;
  Category: string;
  NozzelMasterForm: any;
  filteredItems: any[];
  productList: Product[] = [
    {"id": 1, "name": "juice 1", "description": "description 1"},
    {"id": 2, "name": "juice 2", "description": "description 2"},
    {"id": 3, "name": "juice 3", "description": "description 3"},
    {"id": 4, "name": "juice 4", "description": "description 4"},
    {"id": 5, "name": "juice 5", "description": "description 5"},
    {"id": 6, "name": "juice 6", "description": "description 6"},
    {"id": 7, "name": "juice 7", "description": "description 7"},
    {"id": 8, "name": "juice 8", "description": "description 8"},
    {"id": 9, "name": "juice 9", "description": "description 9"},
    {"id": 10, "name": "orange 1", "description": "description 1"},
    {"id": 11, "name": "orange 2", "description": "description 2"},
    {"id": 12, "name": "orange 3", "description": "description 3"},
    {"id": 13, "name": "orange 4", "description": "description 4"},
    {"id": 14, "name": "orange 5", "description": "description 5"},
    {"id": 15, "name": "orange 6", "description": "description 6"},
    {"id": 16, "name": "orange 7", "description": "description 7"},
    {"id": 17, "name": "orange 8", "description": "description 8"},
    {"id": 18, "name": "orange 9", "description": "description 9"},
    {"id": 19, "name": "orange 10", "description": "description 10"},
    {"id": 20, "name": "orange 11", "description": "description 11"},
    {"id": 21, "name": "orange 12", "description": "description 12"},
    {"id": 22, "name": "orange 13", "description": "description 13"},
    {"id": 23, "name": "orange 14", "description": "description 14"},
    {"id": 24, "name": "orange 15", "description": "description 15"},
    {"id": 25, "name": "orange 16", "description": "description 16"},
    {"id": 26, "name": "orange 17", "description": "description 17"},
  ]
 // filteredItems : Nozzel[];
  items: Nozzel[];
  inputName : string = '';

  employees: any[];
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
 //teacher1: Teacher= new Object(0);
  nozzel: Nozzel = new Object();
  searchText = '';
  page:number=1;
  pager: any = {};
 pagedItems: any[];
 
  nozzelList: Array<Nozzel>;
  errorMessage: string = "";
  showError: boolean = false;
  defaultImage: string = "./assets/img/user.jpg";
  constructor(private pagerService: PagerService,private nozzelservice:NozzelMasterService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter ) 
  {
this.employees = [{ ID:0, Type:'n1'}];
//this.filteredItems= nozzelList;
this.filteredItems = this.productList;

   }
  
getemployee(): void{
  this.employees = [{ ID:0, Type:'n1'}];

}

  ngOnInit() {
 this.resetForm();
  }
  saveTodos(): void {
    debugger;
   //show box msg
   this.edited = true;
   //wait 3 Seconds and hide
   setTimeout(function() {
       this.edited = false;
       console.log(this.edited);
   }.bind(this), 3000);
  }

  resetForm(form?:NgForm){

    form.reset();
    this.inputName='';
    this.Category='';
  }
  FilterByName1(){
    debugger;
    this.filteredItems = [];
    if(this.inputName != ""){
          this.productList.forEach(element => {
              if(element.name.toUpperCase().indexOf(this.inputName.toUpperCase())>=0){
                this.filteredItems.push(element);
             }
          });
    }else{
       this.filteredItems = this.productList;
    }
    console.log(this.filteredItems);
    //this.init();
 }
  FilterByName(){
    debugger;
   this.filteredItems = [];
    //this.GetList();
    if(this.inputName != ""){
      this.nozzelList.forEach(element => {
        if(element.Type.toLowerCase().indexOf(this.inputName.toLowerCase())>=0){
           this.filteredItems.push(element);
      }
     });
}else{
this.filteredItems = this.nozzelList;
}
console.log(this.filteredItems);
this.GetList();
      // this.ShowLoader = true;
      // this.nozzelservice.GetType(ID)
      // .subscribe(resp => this.NozzelMasterForm.setValue(resp)
      //          , error => this.errorMessage = error);      
    // this.nozzelservice.Get(ID).subscribe(result => {
    //   this.nozzel = result;
    //     this.nozzel.ID = ID;
    //     this.ShowLoader = false;
    //   this.showError = false;
    //   this.errorMessage = "";
    //   this.ShowLoader = false;
    //   this.GetList();
    // }, error => {
    //   this.showError = true;
    //   this.errorMessage = error.Data.Message;
    //   this.ShowLoader = false;
    // });
      // this.ShowLoader = true;
      // this.nozzelservice.Get(Id).subscribe(result => {
      //   this.nozzel = result;
      //   this.nozzel.ID = Id;
      //   this.ShowLoader = false;
      //   this.modalRef = this.modalservice.show(template, { class: 'modal-lg' });
      // }, error => {
      //   this.showError = true;
      //   this.errorMessage = error.Data.Message;
      //   this.ShowLoader = false;
      // })
          
    
  //  this.init();
 }
  openModal(template: TemplateRef<any>, Id: string) {
    if (Id == undefined) {
      this.isUpdate = false;
      this.nozzel = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-sm',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.nozzelservice.Get(Id).subscribe(result => {
        this.nozzel = result;
        this.nozzel.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-sm',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    debugger;
    this.ShowLoader = true;
    this.nozzelservice.GetList().subscribe(result => {
      this.nozzelList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.nozzelList.length, page);
 
    // get current page of items
    this.pagedItems = this.nozzelList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveNozzelMaster() {
    this.ShowLoader = true;
    
    this.nozzelservice.CreateNozzelMaster(this.nozzel).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
      this.saveTodos();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateNozzelMaster() {
    this.ShowLoader = true;
    
    this.nozzelservice.UpdateNozzelMaster(this.nozzel).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteNozzelMaster(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.nozzelservice.DeleteNozzelMaster(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
}
