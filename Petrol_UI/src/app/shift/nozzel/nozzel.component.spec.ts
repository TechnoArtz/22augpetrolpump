import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NozzelComponent } from './nozzel.component';

describe('NozzelComponent', () => {
  let component: NozzelComponent;
  let fixture: ComponentFixture<NozzelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NozzelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NozzelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
