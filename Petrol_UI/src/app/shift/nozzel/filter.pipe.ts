import { Pipe, PipeTransform } from '@angular/core';
//import { Nozzel } from '../NozzelMaster';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform 
{

  transform(nozzelList:any, inputName?: string): any {

    
    // return nozzels.filter( nozzels.Type.indexOf(searchText) !== -1);
   if(inputName == nozzelList.Type ) return nozzelList;
       return nozzelList.filter(function(nozzelList){
         return nozzelList.Type.toLowerCase().indexOf(inputName.toLowerCase()) > -1;
       } 
      
      )
  }
  
 

}

