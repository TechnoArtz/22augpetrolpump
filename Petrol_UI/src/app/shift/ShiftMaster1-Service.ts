import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';



import { ShiftMaster1 } from './ShiftMaster1';
@Injectable()
export class ShiftMaster1Service {
    constructor(private httpclient: HttpClient) {
    }
    CreateShiftMaster(shiftmaster1: ShiftMaster1): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "ShiftMaster/CreateShiftMaster",shiftmaster1 );
    }
    UpdateShiftMaster(shiftmaster1: ShiftMaster1): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "ShiftMaster?id=" + shiftmaster1.ID, shiftmaster1);
    }
    DeleteShiftMaster(ID: string): Observable<ShiftMaster1> {
        return this.httpclient.delete<ShiftMaster1>(APIConstant + "ShiftMaster?id=" + ID);
    }
    GetList(): Observable<Array<ShiftMaster1>> {
        return this.httpclient.get<Array<ShiftMaster1>>(APIConstant + "ShiftMaster");
    }
    Get(ID: string): Observable<ShiftMaster1> {
        return this.httpclient.get<ShiftMaster1>(APIConstant + "ShiftMaster?id=" + ID);
    }
}