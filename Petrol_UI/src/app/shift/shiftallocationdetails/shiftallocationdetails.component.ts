import { Component, OnInit } from '@angular/core';
import { ShiftAllocationService } from '../shiftallocation-service';
import { Nozzel } from '../NozzelMaster';
import { Tank } from '../TankMaster';
import { Employee } from '../../employee/Employee';
import { ShiftType } from '../shift-type-master/shift-type-master';
import { ShiftAllocationDetails, ShiftAllocation } from '../shiftallocation';
import { Router } from '@angular/router';
import { NozzelMasterService } from '../NozzelMaster-service';
import { TankMasterService } from '../TankMaster-service';
import { EmployeeService } from '../../employee/employee-service';
import { ShiftTypeMasterService } from '../shift-type-master/shift-type-master-service';


@Component({
  selector: 'app-shiftallocationdetails',
  templateUrl: './shiftallocationdetails.component.html',
  styleUrls: ['./shiftallocationdetails.component.css']
})
export class ShiftallocationdetailsComponent implements OnInit {
  valuedate=new Date();
  shiftdetails: ShiftAllocationDetails = new Object();

  shiftallocation: ShiftAllocation = new Object();
  //search: Search= new Object();
  // shiftallocation:Observer<ShiftAllocationDetails[]>;
  ShiftTypeList: Array<ShiftAllocation> = [{}];
  shiftallocationList: Array<ShiftAllocationDetails> = [{}];
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  Nozzel: string;
  Opening: any = 0;
  ClosingInput: any = 0;
  RateInput: any = 0;
  CashInput: any = 0;
  totalamount;
  totalsale;
  shiftMaster: ShiftType[];
  EmployeeNameMaster: Employee[];
  tankmaster: Tank[];
  nozzelmaster: Nozzel[];
  result: number;
  resultrate: number;
  resulttotalamtcash: number;
  errorMessage: string = "";
  showError: boolean = false;
  constructor(private shiftallocationservice: ShiftAllocationService,private employeeRegServive:EmployeeService,private shiftTypeService:ShiftTypeMasterService, private router: Router, private nozzelservice:NozzelMasterService,private tankservice:TankMasterService) {
    
    if (this.shiftallocationservice.ShiftAllocationID != null && this.shiftallocationservice.ShiftAllocationID != undefined && this.shiftallocationservice.ShiftAllocationID != "0") {
      this.isUpdate = true;
      this.shiftallocationservice.Get(this.shiftallocationservice.ShiftAllocationID).subscribe(result => {
        this.shiftallocationList = result.lstAllotcation;
        this.shiftallocation = result;
      })
    }
    this.employeeRegServive.GetList().subscribe(result => {
      this.EmployeeNameMaster = result;
    }, error => {
      this.EmployeeNameMaster=null;
    });

    this.shiftTypeService.GetList().subscribe(result => {
      this.shiftMaster = result;
    }, error => {
      this.shiftMaster=null;
    });

    this.nozzelservice.GetList().subscribe(result => {
      this.nozzelmaster = result;
    }, error => {
      this.nozzelmaster=null;
    });

    this.tankservice.GetList().subscribe(result => {
      this.tankmaster = result;
    }, error => {
      this.tankmaster=null;
    });
  }
  ngOnInit() {
  }

  //   GetList() {
  //     debugger;
  //      this.ShowLoader = true;
  //      this.shiftallocationservice.GetList().subscribe(result => {
  //        this.shiftallocationList = result;
  //        this.showError = false;
  //        this.errorMessage = "";
  //        this.ShowLoader = false;
  //      }, error => {
  //        this.ShowLoader = false;
  //        this.showError = true;
  //        this.errorMessage = error.Data.Message;
  //      });
  //    }
  //   //  SaveShiftAllocation(){
  //   //   this.shiftallocationservice.shiftdetails = {};
  //   //   this.shiftallocationservice.ShiftAllocationID= "-1";
  //   //   this.router.navigateByUrl("/layout/shift-allocation-details");
  //   // }
  //   // UpdateShiftAllocation() {
  //   //   this.shiftallocationservice.shiftdetails = this.ShiftAllocationID;
  //   //   this.shiftallocationservice.ShiftAllocationID = shiftdetails.;
  //   //   this.router.navigateByUrl("/layout/shift-allocation-details");
  //   // }
  SaveShiftAllocationdetails() {
    debugger
    this.ShowLoader = true;
    this.shiftallocation.lstAllotcation = this.shiftallocationList;
    this.shiftallocationservice.CreateShiftAllocation(this.shiftallocation).subscribe(result => {

      // this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.router.navigateByUrl("/layout/shift/shift-allocation");
      //this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateShiftAllocationdetails() {
    this.ShowLoader = true;
    debugger;
    this.shiftallocation.lstAllotcation = this.shiftallocationList;
    this.shiftallocationservice.UpdateShiftAllocation(this.shiftallocation, this.shiftallocation.ID).subscribe(result => {
      //this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.router.navigateByUrl("/layout/shift/shift-allocation");
      // this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }

  addrow() {
    this.shiftallocationList.push({});
  }
  SubtractionTotalSale(item, i) {
    debugger;
    this.shiftallocationList[i].TotalSale = item.Opening - item.Closing;
    this.result = this.shiftallocationList[i].TotalSale;
    this.getTotalSale();
  }
  MultiplicationTotalAmt(item, i) {
    debugger;
    this.shiftallocationList[i].TotalAmt = this.shiftallocationList[i].TotalSale * item.Rate;
    this.resultrate = this.shiftallocationList[i].TotalAmt;
    this.getTotalAmt();
  }
  SubstractionCreditCard(item, i) {
    this.shiftallocationList[i].CreditCardCash = this.shiftallocationList[i].TotalAmt - item.ByCash;
    this.resulttotalamtcash = this.shiftallocationList[i].CreditCardCash;
  }

  getTotalAmt() {
    let total = 0;
    for (let i = 0; i < this.shiftallocationList.length; i++) {
      if (this.shiftallocationList[i].TotalAmt) {
        total += this.shiftallocationList[i].TotalAmt;
        this.totalamount = total;
      }
    }
    return this.totalamount;
  }
  getTotalSale() {
    let total = 0;
    for (let i = 0; i < this.shiftallocationList.length; i++) {
      if (this.shiftallocationList[i].TotalSale) {
        total += this.shiftallocationList[i].TotalSale;
        this.totalsale = total;
      }
    }
    return this.totalsale;
  }


}
