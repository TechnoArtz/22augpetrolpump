import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftallocationdetailsComponent } from './shiftallocationdetails.component';

describe('ShiftallocationdetailsComponent', () => {
  let component: ShiftallocationdetailsComponent;
  let fixture: ComponentFixture<ShiftallocationdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftallocationdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftallocationdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
