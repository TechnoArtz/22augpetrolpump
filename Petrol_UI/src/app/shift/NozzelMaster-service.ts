import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';


import { Nozzel } from './NozzelMaster';
@Injectable()
export class NozzelMasterService {
    constructor(private httpclient: HttpClient) {
    }
    CreateNozzelMaster(nozzel: Nozzel): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "NozzleMaster", nozzel);
    }
    UpdateNozzelMaster(nozzel: Nozzel): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "NozzleMaster?id=" + nozzel.ID, nozzel);
    }
    DeleteNozzelMaster(ID: string): Observable<Nozzel> {
        return this.httpclient.delete<Nozzel>(APIConstant + "NozzleMaster?id=" + ID);
    }
    GetList(): Observable<Array<Nozzel>> {
        return this.httpclient.get<Array<Nozzel>>(APIConstant + "NozzleMaster");
    }
    Get(ID: string): Observable<Nozzel> {
        return this.httpclient.get<Nozzel>(APIConstant + "NozzleMaster?id=" + ID);
    }
    GetType(ID: string): Observable<Nozzel> {
        return this.httpclient.get<Nozzel>(APIConstant + "NozzleMaster/GetType?Type=" + ID);
    }

    // getCustomerById(id){
    //     return this.httpclient.get(APIConstant +"NozzleMaster/GetType?Type="+ id);
                
    //    }
}