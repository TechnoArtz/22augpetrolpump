import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ShiftRoutingModule } from './shift-routing.module';
import { ShiftentryComponent } from './shiftentry/shiftentry.component';
import { ShiftallocationComponent } from './shiftallocation/shiftallocation.component';
import { ShiftmasterComponent } from './shiftmaster/shiftmaster.component';
import { ShiftdetailsComponent } from './shiftdetails/shiftdetails.component';
import { UnitService } from './ShiftMaster-Service';
import { NozzelComponent } from './nozzel/nozzel.component';
import { NozzelMasterService } from './NozzelMaster-service';
import { ShiftMaster1Component } from './shift-master1/shift-master1.component';
import { ShiftMaster1Service } from './ShiftMaster1-Service';
import { TankMasterComponent } from './tank-master/tank-master.component';
import { TankMasterService } from './TankMaster-service';
import { SearchByNamePipe } from './nozzel/search-by-nozzeltype.pipe';
import { ShiftTypeMasterComponent } from './shift-type-master/shift-type-master.component';
import { ShiftTypeMasterService } from './shift-type-master/shift-type-master-service';

import { SharedModuleModule } from '../shared-module/shared-module.module';
import { ShiftAllocationService } from './shiftallocation-service';

import { EmployeeService } from '../employee/employee-service';
import { ShiftEntryService } from './shiftentry-service';
import { ShiftallocationdetailsComponent } from './shiftallocationdetails/shiftallocationdetails.component';
import { PagerService } from '../page/page.service';
//import { ExcelService } from './shiftallocation/excel.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ShiftRoutingModule,
    ModalModule,
    BsDatepickerModule,
    SharedModuleModule
  ],
  declarations: [ShiftentryComponent, ShiftallocationComponent, ShiftmasterComponent, ShiftdetailsComponent, NozzelComponent, ShiftMaster1Component, TankMasterComponent,SearchByNamePipe, ShiftTypeMasterComponent, ShiftallocationdetailsComponent],
  providers: [UnitService,NozzelMasterService,PagerService,ShiftMaster1Service,ShiftEntryService,TankMasterService,ShiftTypeMasterService,ShiftAllocationService,EmployeeService]
})
export class ShiftModule { }
