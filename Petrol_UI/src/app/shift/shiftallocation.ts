export interface ShiftAllocation {
    ID?:string,
    Date?: any,
    ShiftType?: string,
    ShiftDes?:string,
    Empoyee_ID?: string,
    EmployeeName?:string,
    EmployeeNameDes?:string,
    Note?:string,
    lstAllotcation?: Array<ShiftAllocationDetails>;
  
}
export interface ShiftAllocationDetails {
    ID?:string,
    ShiftAllocationMasterID?:string,
    Empoyee_ID?: string,
    EmployeeName?:string,
    EmployeeNameDes?:string,
    TankDes?:string,
    NozzelDes?:string,
    Date?: any,
    shift?: string,
    Note?:string,
    Tank?:string,
    Nozzel?:string,
    Opening?:number,
    Closing?:number,
    TotalSale?:number,
    Rate?:number,
    TotalAmt?:number,
    ByCash?:number,
    CreditCardCash?:number,
    DebitCardCash?:number,
    GrandTotalSale?:number,
    GrandTotalAmount?:number,
    PumpTesting?:number,
}

