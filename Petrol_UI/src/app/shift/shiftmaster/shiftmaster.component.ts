import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


import { CommonModalComponent } from '../../common-modal/common-modal.component';

import { DateTimeFormatter } from '../../Common/DateFormatter';
import { UnitService } from '../ShiftMaster-Service';
import { Unit } from '../ShiftMaster';
import { PagerService } from '../../page/page.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-shiftmaster',
  templateUrl: './shiftmaster.component.html',
  styleUrls: ['./shiftmaster.component.css']
})
export class ShiftmasterComponent implements OnInit {
  Category: string;
  edited: boolean;
  inputName: string;
  page:number=1;
  pager: any = {};
 pagedItems: any[];
 
  modalRef: BsModalRef;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  unit: Unit = new Object();
  unitList: Array<Unit>;
  errorMessage: string = "";
  showError: boolean = false;
  defaultImage: string = "./assets/img/user.jpg";
  constructor(private pagerService: PagerService,private unitservice:UnitService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter) {}



  ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){

    this.inputName='';
    this.Category='';
    
  }
  saveTodos(): void {
    debugger;
   //show box msg
   this.edited = true;
   //wait 3 Seconds and hide
   setTimeout(function() {
       this.edited = false;
       console.log(this.edited);
   }.bind(this), 3000);
  }
  openModal(template: TemplateRef<any>, Id: string) {
    if (Id == undefined) {
      this.isUpdate = false;
      this.unit = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-sm',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.unitservice.Get(Id).subscribe(result => {
        this.unit = result;
        this.unit.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-sm',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    this.ShowLoader = true;
    this.unitservice.GetList().subscribe(result => {
      this.unitList = result;
      this.setPage(this.page);

      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.unitList.length, page);
 
    // get current page of items
    this.pagedItems = this.unitList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveUnitMaster() {
    this.ShowLoader = true;
    
    this.unitservice.CreateUnitMaster(this.unit).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.saveTodos();
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateUnitMaster() {
    this.ShowLoader = true;
    debugger;
    this.unitservice.UpdateUnitMaster(this.unit).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteUnitMaster(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.unitservice.DeleteUnitMaster(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
}
