import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftallocationComponent } from './shiftallocation.component';

describe('ShiftallocationComponent', () => {
  let component: ShiftallocationComponent;
  let fixture: ComponentFixture<ShiftallocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftallocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftallocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
