import { Component, OnInit, TemplateRef } from '@angular/core';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
declare var $: any;

import { DateTimeFormatter } from '../../Common/DateFormatter';
import { ShiftAllocationDetails, ShiftAllocation} from '../shiftallocation';
import { ShiftAllocationService } from '../shiftallocation-service';
//import {ExcelService} from './excel.service';

import { Search } from '../../Common/Search';
import { ShiftTypeMasterService } from '../shift-type-master/shift-type-master-service';
import { ShiftType } from '../shift-type-master/shift-type-master';
import { EmployeeService } from '../../employee/employee-service';
import { Employee } from '../../employee/Employee';
import { Tank } from '../TankMaster';
import { Nozzel } from '../NozzelMaster';
import { TankMasterService } from '../TankMaster-service';
import { NozzelMasterService } from '../NozzelMaster-service';
import { Router } from '@angular/router';
import { PagerService } from '../../page/page.service';
@Component({
  selector: 'app-shiftallocation',
  templateUrl: './shiftallocation.component.html',
  styleUrls: ['./shiftallocation.component.css']
})
export class ShiftallocationComponent implements OnInit {
  pager: any = {};
  pagedItems: any[];
  page:number=1;
  resulttotalamtcash: any;
  resultrate: any;
  result;
 //temp: Array<tempShiftDetail> =[{}];
 shiftAllocation: any ={};
  modalRef: BsModalRef;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  shiftdetails: ShiftAllocationDetails = new Object();
  shiftallocation: ShiftAllocation = new Object();
  search: Search= new Object();
 // shiftallocation:Observer<ShiftAllocationDetails[]>;
 ShiftTypeList:Array<ShiftAllocation>=[{}];
  shiftallocationList: Array<ShiftAllocationDetails>=[{}] ;
 // shiftallocationList: Array<ShiftAllocationDetails> =[{}];
 // shiftallocationList1: Array<ShiftAllocationDetails> =[{}];
  errorMessage: string = "";
  showError: boolean = false;
  defaultImage: string = "./assets/img/user.jpg";
  Nozzel:string;
  Opening: any = 0;
  ClosingInput: any = 0;
  RateInput: any = 0;
  CashInput: any = 0;
  totalamount;
  totalsale;
  shiftMaster: ShiftType[];
  EmployeeNameMaster: Employee[];
  tankmaster:Tank[];
  nozzelmaster:Nozzel[];
  constructor(private pagerService: PagerService,private shiftallocationservice:ShiftAllocationService,private employeeRegServive:EmployeeService,private shiftTypeService:ShiftTypeMasterService,private tankservice:TankMasterService,private nozzelservice:NozzelMasterService,
    private modalservice:BsModalService,private dateformatter:DateTimeFormatter,private router:Router)   
  {  
    //this.excelService = excelService;
    this.shiftTypeService.GetList().subscribe(result =>{
       this.shiftMaster=result;

      });
      this.employeeRegServive.GetList().subscribe(result =>{
        this.EmployeeNameMaster=result;
 
       });
       this.tankservice.GetList().subscribe(result =>{
        this.tankmaster=result;
 
       });
       this.nozzelservice.GetList().subscribe(result =>{
        this.nozzelmaster=result;
 
       });
   }

  ngOnInit() {
  }
  // Getshifttype(){
  //   this.ShowLoader = true;
  //   this.shiftallocationservice.GetShiftMaster().subscribe(result => {
  //     this.shiftallocation= result;
  //     this.showError = false;
  //     this.errorMessage = "";
  //     this.ShowLoader = false;
  //   }, error => {
  //     this.ShowLoader = false;
  //     this.showError = true;
  //     this.errorMessage = error.Data.Message;
  //   });

  // }
  // GetEmpNames(){
  //   this.ShowLoader = true;
  //   this.shiftallocationservice.GetEmployeeNameMaster().subscribe(result => {
  //     this.shiftallocation= result;
  //     this.showError = false;
  //     this.errorMessage = "";
  //     this.ShowLoader = false;
  //   }, error => {
  //     this.ShowLoader = false;
  //     this.showError = true;
  //     this.errorMessage = error.Data.Message;
  //   });

  // }


  // exportToExcel(event) {
  //   debugger;
  //  this.excelService.exportAsExcelFile(this.shiftallocationList, 'shiftallocationList1');
  // }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id == undefined) {
      this.isUpdate = false;
      this.shiftallocation = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg' });
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      debugger;
      this.shiftallocationservice.Get(Id).subscribe(result => {
       
        this.shiftallocation = result;
       // this.shiftallocationList=this.shiftallocation.lstAllotcation;
        this.shiftdetails.ID= Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg' });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }

  GetList() {
    debugger;
     this.ShowLoader = true;
     this.shiftallocationservice.GetList().subscribe(result => {
       this.shiftallocationList = result;
       this.setPage(this.page);
       this.showError = false;
       this.errorMessage = "";
       this.ShowLoader = false;
     }, error => {
       this.ShowLoader = false;
       this.showError = true;
       this.errorMessage = error.Data.Message;
     });
   }
   setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.shiftallocationList.length, page);
 
    // get current page of items
    this.pagedItems = this.shiftallocationList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }

   GetFilterdData() {
    debugger;
    if(this.search.category!="")
   {
     this.ShowLoader = true;
     this.shiftallocationservice.GetFilterdData(this.search).subscribe(result => {
       this.shiftallocationList = result;
       this.showError = false;
       this.errorMessage = "";
       this.ShowLoader = false;
     }, error => {
       this.ShowLoader = false;
       this.showError = true;
       this.errorMessage = error.Data.Message;
     })};
   }
   SaveShiftAllocation()
   {
     this.ShowLoader=true;
     this.shiftallocationservice.ShiftAllocationID="0";
     this.router.navigateByUrl("/layout/shift/shift-allocation-details");
   }
   UpdateShiftAllocation(id:string){
    this.ShowLoader=true;
    this.shiftallocationservice.ShiftAllocationID=id;
    this.router.navigateByUrl("/layout/shift/shift-allocation-details");
   }
  //  SaveShiftAllocation() {
  //   debugger
  //   this.ShowLoader = true;
  //   this.shiftallocation.lstAllotcation=this.shiftallocationList;
  //   this.shiftallocationservice.CreateShiftAllocation(this.shiftallocation).subscribe(result => {
     
  //     this.modalRef.hide();
  //     this.showError = false;
  //     this.errorMessage = "";
  //     this.ShowLoader = false;
  //     this.GetList();
  //   }, error => {
  //     this.showError = true;
  //     this.errorMessage = error.Data.Message;
  //     this.ShowLoader = false;
  //   });
  // }
  // UpdateShiftAllocation() {
  //   this.ShowLoader = true;
  //   debugger;
  //   this.shiftallocationservice.UpdateShiftAllocation(this.shiftdetails,this.shiftdetails.ID).subscribe(result => {
  //     this.modalRef.hide();
  //     this.showError = false;
  //     this.errorMessage = "";
  //     this.ShowLoader = false;
  //     this.GetList();
  //   }, error => {
  //     this.ShowLoader = false;
  //     this.showError = true;
  //     this.errorMessage = error.Data.Message;
  //   });
  // }
   addrow(){
   this.shiftallocationList.push({});
   this.getTotalAmt();
   this.getTotalSale();
  }
  SubtractionTotalSale(item, i) {
    debugger;
    this.shiftallocationList[i].TotalSale = item.Opening - item.Closing;
    this.result = this.shiftallocationList[i].TotalSale;
  }
  MultiplicationTotalAmt(item, i) {
    debugger;
    this.shiftallocationList[i].TotalAmt = this.shiftallocationList[i].TotalSale * item.Rate;
    this.resultrate = this.shiftallocationList[i].TotalAmt;
  }
  SubstractionCreditCard(item, i) {
    debugger;
    this.shiftallocationList[i].CreditCardCash = this.shiftallocationList[i].TotalAmt - item.ByCash;
    this.resulttotalamtcash = this.shiftallocationList[i].CreditCardCash;
  }
 
  getTotalAmt() {
    debugger;
    let total = 0;
    for (let i = 0; i < this.shiftallocationList.length; i++) {
      if (this.shiftallocationList[i].TotalAmt) {
        total += this.shiftallocationList[i].TotalAmt;
        this.totalamount = total;
      }
    }
    return this.totalamount;
  }
  getTotalSale() {
    debugger;
    let total = 0;
    for (let i = 0; i < this.shiftallocationList.length; i++) {
      if (this.shiftallocationList[i].TotalSale) {
        total += this.shiftallocationList[i].TotalSale;
        this.totalsale = total;
      }
    }
    return this.totalsale;
  }

}