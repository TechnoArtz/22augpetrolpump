import { Component, OnInit, TemplateRef } from '@angular/core';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


import { CommonModalComponent } from '../../common-modal/common-modal.component';

import { DateTimeFormatter } from '../../Common/DateFormatter';
import { Tank } from '../TankMaster';
import { TankMasterService } from '../TankMaster-service';
import { PagerService } from '../../page/page.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-tank-master',
  templateUrl: './tank-master.component.html',
  styleUrls: ['./tank-master.component.css']
})
export class TankMasterComponent implements OnInit {
  Category: string;
  inputName: string;
  edited: boolean;
  pager: any = {};
 pagedItems: any[];
 page:number=1;
  modalRef: BsModalRef;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  tank: Tank = new Object();
  tankList: Array<Tank>;
  errorMessage: string = "";
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  showError: boolean = false;
  //defaultImage: string = "./assets/img/user.jpg";
  constructor(private pagerService: PagerService,private tankservice:TankMasterService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter ) { }

  ngOnInit() {
    this.resetForm();
     }
     saveTodos(): void {
       debugger;
      //show box msg
      this.edited = true;
      //wait 3 Seconds and hide
      setTimeout(function() {
          this.edited = false;
          console.log(this.edited);
      }.bind(this), 3000);
     }
   
     resetForm(form?:NgForm){
   
       form.reset();
       this.inputName='';
       this.Category='';
     }
  openModal(template: TemplateRef<any>, Id: string) {
    if (Id == undefined) {
      this.isUpdate = false;
      this.tank = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-sm',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.tankservice.Get(Id).subscribe(result => {
        this.tank = result;
        this.tank.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-sm',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
   debugger;
    this.ShowLoader = true;
    this.tankservice.GetList().subscribe(result => {
      this.tankList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.tankList.length, page);
 
    // get current page of items
    this.pagedItems = this.tankList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveTankMaster() {
    this.ShowLoader = true;
    
    this.tankservice.CreateTankMaster(this.tank).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
      this.saveTodos();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateTankMaster() {
    this.ShowLoader = true;
    
    this.tankservice.UpdateTankMaster(this.tank).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteTankMaster(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.tankservice.DeleteTankMaster(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
}
