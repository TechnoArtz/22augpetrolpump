import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';



//import { Tank } from './TankMaster';
import { ShiftAllocation, ShiftAllocationDetails } from './shiftallocation';
import { Search } from '../Common/Search';
@Injectable()
export class ShiftAllocationService {
   // shiftdetails: ShiftAllocationDetails={};
    ShiftAllocationID:string;
    // shiftallocation: ShiftAllocation={};
    constructor(private httpclient: HttpClient) {
    }
    CreateShiftAllocation(shiftallocation: ShiftAllocation): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "ShiftAllocation/Create", shiftallocation);
    }
    // AddShiftAllocation(shiftallocationdetails: ShiftAllocationDetails): Observable<boolean> {
    //     return this.httpclient.post<boolean>(APIConstant + "ShiftAllocation", shiftallocationdetails);
    // }
    UpdateShiftAllocation(shiftallocation: ShiftAllocationDetails,ShiftAllocationID): Observable<boolean> {
        debugger;
        return this.httpclient.put<boolean>(APIConstant + "ShiftAllocation?id=" + ShiftAllocationID, shiftallocation);
    }

    GetList(): Observable<Array<ShiftAllocationDetails>> {
        return this.httpclient.get<Array<ShiftAllocationDetails>>(APIConstant + "ShiftAllocation/List");
    }
    GetFilterdData(search: Search): Observable<Array<ShiftAllocationDetails>> {
        return this.httpclient.post<Array<ShiftAllocationDetails>>(APIConstant + "ShiftAllocation/FilterdData", search);
    }
    Get(ShiftAllocationID: string): Observable<ShiftAllocation> {
        debugger;
        return this.httpclient.get<ShiftAllocation>(APIConstant + "ShiftAllocation?id=" + ShiftAllocationID);
    }

    GetShiftMaster(): Observable<ShiftAllocation> {
        return this.httpclient.get<ShiftAllocation>(APIConstant + "ShiftAllocation/ShiftMaster" );
    }

    GetEmployeeNameMaster(): Observable<ShiftAllocation> {
        return this.httpclient.get<ShiftAllocation>(APIConstant + "ShiftAllocation/EmployeeRegMaster" );
    }
    GetTankNameMaster(): Observable<ShiftAllocationDetails> {
        return this.httpclient.get<ShiftAllocationDetails>(APIConstant + "ShiftAllocation/TankMaster" );
    }
    GetNozzelNameMaster(): Observable<ShiftAllocationDetails> {
        return this.httpclient.get<ShiftAllocationDetails>(APIConstant + "ShiftAllocation/NozzelMaster" );
    }
}