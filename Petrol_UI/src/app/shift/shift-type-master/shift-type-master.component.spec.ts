import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftTypeMasterComponent } from './shift-type-master.component';

describe('ShiftTypeMasterComponent', () => {
  let component: ShiftTypeMasterComponent;
  let fixture: ComponentFixture<ShiftTypeMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftTypeMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftTypeMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
