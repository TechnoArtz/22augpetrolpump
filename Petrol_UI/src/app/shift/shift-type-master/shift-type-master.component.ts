import { Component, OnInit, TemplateRef } from '@angular/core';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


import { CommonModalComponent } from '../../common-modal/common-modal.component';

import { DateTimeFormatter } from '../../Common/DateFormatter';
import { ShiftType } from './shift-type-master';
import { ShiftTypeMasterService } from './shift-type-master-service';
import { PagerService } from '../../page/page.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-shift-type-master',
  templateUrl: './shift-type-master.component.html',
  styleUrls: ['./shift-type-master.component.css']
})
export class ShiftTypeMasterComponent implements OnInit {
  edited: boolean;
  Category: string;
  inputName: string;
  pager: any = {};
  pagedItems: any[];
  page:number=1;
  modalRef: BsModalRef;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  shifttype: ShiftType = new Object();
  shifttypelist: Array<ShiftType>;
  errorMessage: string = "";
  showError: boolean = false;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  //defaultImage: string = "./assets/img/user.jpg";

  constructor(private pagerService: PagerService,private shiftservice:ShiftTypeMasterService,private modalservice:BsModalService,private dateformatter:DateTimeFormatter) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){

    form.reset();
    this.inputName='';
    this.Category='';
  }
  saveTodos(): void {
    debugger;
   //show box msg
   this.edited = true;
   //wait 3 Seconds and hide
   setTimeout(function() {
       this.edited = false;
       console.log(this.edited);
   }.bind(this), 3000);
  }
  openModal(template: TemplateRef<any>, Id: string) {
    debugger;
    if (Id == undefined) {
      this.isUpdate = false;
      this.shifttype = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.shiftservice.Get(Id).subscribe(result => {
        this.shifttype = result;
        this.shifttype.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    debugger;
    this.ShowLoader = true;
    this.shiftservice.GetList().subscribe(result => {
      this.shifttypelist = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  pdffile()
 {
      window.open("http://localhost:60314/api/ShiftMaster/GeneratePdf");
     
 }





  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.shifttypelist.length, page);
 
    // get current page of items
    this.pagedItems = this.shifttypelist.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveShiftMaster() {
    this.ShowLoader = true;
    debugger;
    this.shiftservice.CreateShiftMaster(this.shifttype).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
      this.saveTodos();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateShiftMaster() {
    this.ShowLoader = true;
    debugger;
    this.shiftservice.UpdateShiftMaster(this.shifttype).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteShiftMaster(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.shiftservice.DeleteShiftMaster(ID).subscribe(result => {
          this.modalRef.hide(); 
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }


}
