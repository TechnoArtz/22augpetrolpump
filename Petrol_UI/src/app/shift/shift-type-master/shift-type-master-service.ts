import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

//import { APIConstant } from './../Common/AppConstant';


//import { Nozzel } from './NozzelMaster';
import { ShiftType } from './shift-type-master';
import { APIConstant } from '../../Common/AppConstant';
@Injectable()
export class ShiftTypeMasterService {
    constructor(private httpclient: HttpClient) {
    }
    CreateShiftMaster(shifttype: ShiftType): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "ShiftMaster/CreateShiftMaster", shifttype);
    }
    UpdateShiftMaster(shifttype: ShiftType): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "ShiftMaster?id=" + shifttype.ID, shifttype);
    }
    DeleteShiftMaster(ID: string): Observable<ShiftType> {
        return this.httpclient.delete<ShiftType>(APIConstant + "ShiftMaster?id=" + ID);
    }
    GetList(): Observable<Array<ShiftType>> {
        return this.httpclient.get<Array<ShiftType>>(APIConstant + "ShiftMaster/List");
    }
    Get(ID: string): Observable<ShiftType> {
        return this.httpclient.get<ShiftType>(APIConstant + "ShiftMaster?id=" + ID);
    }
}