export interface ShiftType{
    ID?:string,
    Type?:string,
    StartTime?:string,
    EndTime?:string,
}