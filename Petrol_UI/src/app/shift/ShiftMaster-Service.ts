import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';

import { Unit } from './ShiftMaster';
@Injectable()
export class UnitService {
    constructor(private httpclient: HttpClient) {
    }
    CreateUnitMaster(unit: Unit): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "UnitMaster", unit);
    }
    UpdateUnitMaster(unit: Unit): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "UnitMaster?id=" + unit.ID, unit);
    }
    DeleteUnitMaster(ID: string): Observable<Unit> {
        return this.httpclient.delete<Unit>(APIConstant + "UnitMaster?id=" + ID);
    }
    GetList(): Observable<Array<Unit>> {
        return this.httpclient.get<Array<Unit>>(APIConstant + "UnitMaster");
    }
    Get(ID: string): Observable<Unit> {
        return this.httpclient.get<Unit>(APIConstant + "UnitMaster?id=" + ID);
    }
}