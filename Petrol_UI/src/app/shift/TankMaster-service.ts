import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';



import { Tank } from './TankMaster';
@Injectable()
export class TankMasterService {
    constructor(private httpclient: HttpClient) {
    }
    CreateTankMaster(tank: Tank): Observable<boolean> {
        return this.httpclient.post<boolean>(APIConstant + "TankMaster", tank);
    }
    UpdateTankMaster(tank: Tank): Observable<boolean> {
        return this.httpclient.put<boolean>(APIConstant + "TankMaster?id=" + tank.ID, tank);
    }
    DeleteTankMaster(ID: string): Observable<Tank> {
        return this.httpclient.delete<Tank>(APIConstant + "TankMaster?id=" + ID);
    }
    GetList(): Observable<Array<Tank>> {
        return this.httpclient.get<Array<Tank>>(APIConstant + "TankMaster");
    }
    Get(ID: string): Observable<Tank> {
        return this.httpclient.get<Tank>(APIConstant + "TankMaster?id=" + ID);
    }
}