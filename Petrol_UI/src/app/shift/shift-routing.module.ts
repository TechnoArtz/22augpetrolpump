import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShiftentryComponent } from './shiftentry/shiftentry.component';
import { ShiftallocationComponent } from './shiftallocation/shiftallocation.component';
import { ShiftmasterComponent } from './shiftmaster/shiftmaster.component';
import { ShiftdetailsComponent } from './shiftdetails/shiftdetails.component';
import { NozzelComponent } from './nozzel/nozzel.component';
import { ShiftMaster1Component } from './shift-master1/shift-master1.component';
import { TankMasterComponent } from './tank-master/tank-master.component';
import { ShiftTypeMasterComponent } from './shift-type-master/shift-type-master.component';
import { ShiftallocationdetailsComponent } from './shiftallocationdetails/shiftallocationdetails.component';

const routes: Routes = [
  {
    path: '', children: [
      {
        path: 'shift-entry', component: ShiftentryComponent
      },
      {
        path: 'shift-allocation', component: ShiftallocationComponent
      },
      {
        path:'shift-allocation-details',component:ShiftallocationdetailsComponent
      },
      {
        path: 'Unit-master', component: ShiftmasterComponent
      },
      {
        path: 'shift-details', component: ShiftdetailsComponent
      },
      {
        path: 'shift-nozzel', component: NozzelComponent
      },
      {
        path: 'shift-master1', component: ShiftMaster1Component
      },
      {
        path: 'tank-master', component: TankMasterComponent
      },
      {
        path: 'shifttype-master', component: ShiftTypeMasterComponent
      }
    ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShiftRoutingModule { }
