import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CommonModalComponent } from '../../common-modal/common-modal.component';

import { ShiftEntryService } from '../shiftentry-service';
import { DateTimeFormatter } from '../../Common/DateFormatter';
import { ShiftEntry} from '../shiftentry';
import { ShiftMaster1Service } from '../ShiftMaster1-Service';

import { EmployeeService } from '../../employee/employee-service';
import { Employee} from '../../employee/Employee';
import { error } from 'util';
import { ShiftType } from '../shift-type-master/shift-type-master';
import { ShiftMaster1 } from '../ShiftMaster1';
import { PagerService } from '../../page/page.service';
import { ShiftTypeMasterService } from '../shift-type-master/shift-type-master-service';

@Component({
  selector: 'app-shiftentry',
  templateUrl: './shiftentry.component.html',
  styleUrls: ['./shiftentry.component.css']
})
export class ShiftentryComponent implements OnInit {
  page:number=1;
  pager: any = {};
 pagedItems: any[];
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  modalRef: BsModalRef;
  config = {
    keyboard:true,
    backdrop: true,
    ignoreBackdropClick: false
  };
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  shiftentry: ShiftEntry = new Object();
  shiftentryList: Array<ShiftEntry>;
  errorMessage: string = "";
  showError: boolean = false;
  shiftmaster: ShiftType[];
  employee: Employee[];
  valuedate=new Date();
 
  constructor(private pagerService: PagerService,private shiftentryservice: ShiftEntryService,private dateFormatter: DateTimeFormatter,private employeeservice:EmployeeService ,private shiftMasterService: ShiftTypeMasterService, private modalservice: BsModalService, private dateformatter: DateTimeFormatter) {
    
    this.maxDate.setDate(this.maxDate.getDate() + 7);

    this.shiftMasterService.GetList().subscribe(result => {
      this.shiftmaster = result;
    }, error => {
      this.shiftmaster=null;
    });

    this.employeeservice.GetList().subscribe(result=>{
    
      this.employee=result;
    }, error=>{
      this.employee=null;
    });
    

    
  }
  ngOnInit() {
  }

  //    empName: Employeelist[] = [
  //       {ID:1, EmpName: "Amit"},
  //       {ID:2, EmpName: "Amol"},
  //       {ID:3, EmpName: "Amit"}
  // ];
  openModal(template: TemplateRef<any>, Id: string) {
    if (Id === undefined) {
      this.isUpdate = false;
      this.shiftentry = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
    } else {
      this.isUpdate = true;
      this.ShowLoader = true;
      debugger;
      this.shiftentryservice.Get(Id).subscribe(result => {
        this.shiftentry = result;
        this.shiftentry.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg',backdrop:false, keyboard:false,ignoreBackdropClick:true });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      });
    }
  }

  GetList() {

    this.ShowLoader = true;
    this.shiftentryservice.GetList().subscribe(result => {
      
      this.shiftentryList = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.shiftentryList.length, page);
 
    // get current page of items
    this.pagedItems = this.shiftentryList.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveShiftEntry() {
    this.ShowLoader = true;
  debugger;
    if (this.shiftentry.CreatedDate instanceof Date) {
      this.shiftentry.CreatedDate = this.dateFormatter.transformsDate(this.shiftentry.CreatedDate);
    }


    this.shiftentryservice.CreateShiftEntry(this.shiftentry).subscribe(result => {
      debugger;
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateShiftEntry() {
    this.ShowLoader = true;

    this.shiftentryservice.UpdateShiftEntry(this.shiftentry).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteShiftEntry(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.shiftentryservice.DeleteShiftEntry(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
}
