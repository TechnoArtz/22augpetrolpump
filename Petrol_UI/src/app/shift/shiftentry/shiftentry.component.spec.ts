import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftentryComponent } from './shiftentry.component';

describe('ShiftentryComponent', () => {
  let component: ShiftentryComponent;
  let fixture: ComponentFixture<ShiftentryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftentryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftentryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
