import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftMaster1Component } from './shift-master1.component';

describe('ShiftMaster1Component', () => {
  let component: ShiftMaster1Component;
  let fixture: ComponentFixture<ShiftMaster1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftMaster1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftMaster1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
