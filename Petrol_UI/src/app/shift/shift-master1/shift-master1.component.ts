import { Component, OnInit, TemplateRef } from '@angular/core';
import { Url } from 'url';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


import { CommonModalComponent } from '../../common-modal/common-modal.component';

import { DateTimeFormatter } from '../../Common/DateFormatter';
import { ShiftMaster1 } from '../ShiftMaster1';
import { ShiftMaster1Service } from '../ShiftMaster1-Service';
import { PagerService } from '../../page/page.service';


@Component({
  selector: 'app-shift-master1',
  templateUrl: './shift-master1.component.html',
  styleUrls: ['./shift-master1.component.css']
})
export class ShiftMaster1Component implements OnInit {
  page:number=1;
  pager: any = {};
 pagedItems: any[];

  modalRef: BsModalRef;
  ShowLoader: boolean = false;
  isUpdate: boolean = false;
  shiftmaster1: ShiftMaster1 = new Object();
  shiftmaster1List: Array<ShiftMaster1>;
  errorMessage: string = "";
  showError: boolean = false;
  defaultImage: string = "./assets/img/user.jpg";
  constructor(private pagerService: PagerService,private shiftmaster1service:ShiftMaster1Service,private modalservice:BsModalService,private dateformatter:DateTimeFormatter ) { }

  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, Id: string) {
    if (Id == undefined) {
      this.isUpdate = false;
      this.shiftmaster1 = {};
      this.modalRef = this.modalservice.show(template, { class: 'modal-lg' });
    }
    else {
      this.isUpdate = true;
      this.ShowLoader = true;
      this.shiftmaster1service.Get(Id).subscribe(result => {
        this.shiftmaster1 = result;
        this.shiftmaster1.ID = Id;
        this.ShowLoader = false;
        this.modalRef = this.modalservice.show(template, { class: 'modal-lg' });
      }, error => {
        this.showError = true;
        this.errorMessage = error.Data.Message;
        this.ShowLoader = false;
      })
    }
  }
  GetList() {
    debugger;
    this.ShowLoader = true;
    this.shiftmaster1service.GetList().subscribe(result => {
      this.shiftmaster1List = result;
      this.setPage(this.page);
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  setPage(page) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.shiftmaster1List.length, page);
 
    // get current page of items
    this.pagedItems = this.shiftmaster1List.slice(this.pager.startIndex, this.pager.endIndex + 1);
 }
  SaveShiftMaster() {
    this.ShowLoader = true;
    
    this.shiftmaster1service.CreateShiftMaster(this.shiftmaster1).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.showError = true;
      this.errorMessage = error.Data.Message;
      this.ShowLoader = false;
    });
  }
  UpdateShiftMaster() {
    this.ShowLoader = true;
    
    this.shiftmaster1service.UpdateShiftMaster(this.shiftmaster1).subscribe(result => {
      this.modalRef.hide();
      this.showError = false;
      this.errorMessage = "";
      this.ShowLoader = false;
      this.GetList();
    }, error => {
      this.ShowLoader = false;
      this.showError = true;
      this.errorMessage = error.Data.Message;
    });
  }
  DeleteShiftMaster(ID: string) {
    this.ShowLoader = true;
    this.modalRef = this.modalservice.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
    this.modalRef.content.message = "Are you sure?";
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.shiftmaster1service.DeleteShiftMaster(ID).subscribe(result => {
          this.modalRef.hide();
          this.showError = false;
          this.errorMessage = "";
          this.ShowLoader = false;
          this.GetList();
        }, error => {
          this.showError = true;
          this.errorMessage = error.Data.Message;
          this.ShowLoader = false;
        });
      }
      else
        this.ShowLoader = false;
    })
  }
}
