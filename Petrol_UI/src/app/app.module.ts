import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DatePipe } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { AuthInterceptor } from './AuthInterceptor';
import { DateTimeFormatter } from './Common/DateFormatter';
import { CommonModalComponent } from './common-modal/common-modal.component';
import { LoginregistrationComponent } from './loginregistration/loginregistration.component';




@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    CommonModalComponent,
    LoginregistrationComponent,
    
 
    
    
  ],
  entryComponents: [CommonModalComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    CommonModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [DatePipe, DateTimeFormatter, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
