export interface ReceiveItem
{
   ID?:string;
   Date?:string;
   TankType?:string;
   TankTypeDes?:string;
   Qty?:number;

}