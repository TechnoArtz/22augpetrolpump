import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { APIConstant } from '../Common/AppConstant';
import { ReceiveItem } from './receiveitem';


@Injectable()
export class ReceiveItemService {
   constructor(private httpclient: HttpClient) {
   }
   CreateReceiveItemMaster(receiveitem: ReceiveItem) : Observable<boolean> {
       return this.httpclient.post<boolean>(APIConstant + "ReceiveItemMaster", receiveitem);
   }
   UpdateReceiveItemMaster(receiveitem: ReceiveItem): Observable<boolean> {
       return this.httpclient.put<boolean>(APIConstant + "ReceiveItemMaster?id=" + receiveitem.ID, receiveitem);
   }
   DeleteReceiveItemMaster(ID: string): Observable<ReceiveItem> {
       return this.httpclient.delete<ReceiveItem>(APIConstant + "ReceiveItemMaster?id=" + ID);
   }
   GetList(): Observable<Array<ReceiveItem>> {
       return this.httpclient.get<Array<ReceiveItem>>(APIConstant + "ReceiveItemMaster");
   }
   Get(ID: string): Observable<ReceiveItem> {
       return this.httpclient.get<ReceiveItem>(APIConstant + "ReceiveItemMaster?id=" + ID);
   }
}