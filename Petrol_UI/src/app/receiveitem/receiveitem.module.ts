import { NgModule } from '@angular/core';


import { EmployeeService } from '../employee/employee-service';
import { ShiftTypeMasterService } from '../shift/shift-type-master/shift-type-master-service';
import { NozzelMasterService } from '../shift/NozzelMaster-service';
import { TankMasterService } from '../shift/TankMaster-service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReceiveitemComponent } from './receiveitem.component';
import { ReceiveItemRoutingModule } from './receiveitem.routing';
import { ReceiveItemService } from './receiveitem-service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { PagerService } from '../page/page.service';

// import { ShiftmanagementComponent } from './shiftmanagement.component';
// import { ShiftmanagementRoutingModule } from './shiftmanagement.routing';


@NgModule({
    declarations: [
        ReceiveitemComponent
    ],
    imports: [
ReceiveItemRoutingModule,CommonModule,FormsModule,BsDatepickerModule
    ],
    providers:[ReceiveItemService,EmployeeService,ShiftTypeMasterService,PagerService,NozzelMasterService,TankMasterService]
})
export class ReceiveItemModule { }
