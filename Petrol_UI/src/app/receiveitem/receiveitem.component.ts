import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { DateTimeFormatter } from '../Common/DateFormatter';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ReceiveItem } from './receiveitem';
import { ReceiveItemService } from './receiveitem-service';
import { CommonModalComponent } from '../common-modal/common-modal.component';
import { TankMasterService } from '../shift/TankMaster-service';
import { Tank } from '../shift/TankMaster';
import { PagerService } from '../page/page.service';

@Component({
  selector: 'app-receiveitem',
  templateUrl: './receiveitem.component.html',
  styleUrls: ['./receiveitem.component.css'],
  providers: [DateTimeFormatter]
})
export class ReceiveitemComponent implements OnInit {
  valuedate=new Date();
 modalRef: BsModalRef;
 config = {
  keyboard:true,
  backdrop: true,
  ignoreBackdropClick: false
};
 ShowLoader = false;
 isUpdate = false;
 receiveitem: ReceiveItem=new Object();
 receiveitemList:Array<ReceiveItem>;
 errorMessage = '';
 showError = false;
 tankmaster:Tank[];
 pager: any = {};
 pagedItems: any[];
 page:number=1;
 constructor(private pagerService: PagerService,private modalService: BsModalService,private tankservice:TankMasterService, private dateFormatter: DateTimeFormatter,private receiveitemservice: ReceiveItemService)
  {
    this.tankservice.GetList().subscribe( result=>{
      this.tankmaster=result;
    },error=>{
      this.tankmaster=null;
    });
   }
 ngOnInit() {
 }
 openModal(template: TemplateRef<any>, Id: string) {
   debugger;
   if (Id === undefined) {
     this.isUpdate = false;
     this.receiveitem= {};
     this.modalRef = this.modalService.show(template, { class: 'modal-sm',backdrop:false, keyboard:false,ignoreBackdropClick:true });
   } else {
     this.isUpdate = true;
     this.ShowLoader = true;
     this.receiveitemservice.Get(Id).subscribe(result => {
       this.receiveitem = result;
       this.receiveitem.ID = Id;
       this.ShowLoader = false;
       this.modalRef = this.modalService.show(template, { class: 'modal-sm',backdrop:false, keyboard:false,ignoreBackdropClick:true });
     }, error => {
       this.showError = true;
       this.errorMessage = error.Data.Message;
       this.ShowLoader = false;
     });
   }
 }
 GetList() {
   this.ShowLoader = true;
   this.receiveitemservice.GetList().subscribe(result => {
     
     this.receiveitemList = result;
     this.setPage(this.page);
     this.showError = false;
     this.errorMessage = "";
     this.ShowLoader = false;
   }, error => {
     this.ShowLoader = false;
     this.showError = true;
     this.errorMessage = error.Data.Message;
   });
 }
 setPage(page) {
  // get pager object from service
  this.pager = this.pagerService.getPager(this.receiveitemList.length, page);

  // get current page of items
  this.pagedItems = this.receiveitemList.slice(this.pager.startIndex, this.pager.endIndex + 1);
}
 SaveReceiveEntry() {
  debugger;
   this.ShowLoader = true;
   this.receiveitemservice.CreateReceiveItemMaster(this.receiveitem).subscribe(result => {
     debugger;
     this.modalRef.hide();
     this.showError = false;
     this.errorMessage = "";
     this.ShowLoader = false;
     this.GetList();
   }, error => {
     this.showError = true;
     this.errorMessage = error.Data.Message;
     this.ShowLoader = false;
   });
 }
 UpdateReceiveEntry() {
   this.ShowLoader = true;
   this.receiveitemservice.UpdateReceiveItemMaster(this.receiveitem).subscribe(result => {
     this.modalRef.hide();
     this.showError = false;
     this.errorMessage = "";
     this.ShowLoader = false;
     this.GetList();
   }, error => {
     this.ShowLoader = false;
     this.showError = true;
     this.errorMessage = error.Data.Message;
   });
 }
 DeleteReceiveEntry(ID: string) {
   this.ShowLoader = true;
   this.modalRef = this.modalService.show(CommonModalComponent, {backdrop:false, keyboard:false,ignoreBackdropClick:true});
   this.modalRef.content.message = "Are you sure?";
   this.modalRef.content.onClose.subscribe(result => {
     if (result) {
       this.receiveitemservice.DeleteReceiveItemMaster(ID).subscribe(result => {
         this.modalRef.hide();
         this.showError = false;
         this.errorMessage = "";
         this.ShowLoader = false;
         this.GetList();
       }, error => {
         this.showError = true;
         this.errorMessage = error.Data.Message;
         this.ShowLoader = false;
       });
     }
     else
       this.ShowLoader = false;
   })
 }

}
