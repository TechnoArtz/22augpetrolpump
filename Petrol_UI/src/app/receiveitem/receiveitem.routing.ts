import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceiveitemComponent } from './receiveitem.component';

const routes: Routes = [
  {
   path:'', pathMatch:'full',redirectTo:'layout'
  },
  {
      path:'', component:ReceiveitemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiveItemRoutingModule { }
