import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveitemComponent } from './receiveitem.component';

describe('ReceiveitemComponent', () => {
  let component: ReceiveitemComponent;
  let fixture: ComponentFixture<ReceiveitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiveitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
