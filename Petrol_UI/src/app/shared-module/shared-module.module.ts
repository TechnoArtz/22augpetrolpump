import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterPipe } from '../shift/nozzel/filter.pipe';
import { FilterRegEmployeePipe } from '../employee/registration/filter-reg-employee.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FilterPipe,FilterRegEmployeePipe],
  exports: [ FilterPipe ,FilterRegEmployeePipe]
})
export class SharedModuleModule { }
